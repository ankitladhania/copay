const formModal = require("../../models/InsuranceFormsFilled");
const insuranceModal = require("../../models/insuranceProvider");

class ServiceService {
  constructor() {}

  async getformData(query, programId) {
    try {
      const result = await formModal
        .find(
          {
            "data.NAME": { $regex: `${query}`, $options: "$i" },
            programId: programId
          },
          { "data.NAME": 1 }
        )
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getAllInsuranceProvider(query) {
    try {
      const result = await insuranceModal
        .find({ name: { $regex: `${query}`, $options: "$i" } },{name: 1})
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new ServiceService();
