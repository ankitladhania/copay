import React, { Component, Fragment } from "react";
import {
  Form,
  Select,
  Input,
  Radio,
  DatePicker,
  Button,
  Icon,
  Upload
} from "antd";
import forEachRight from "lodash-es/forEachRight";
import isEmpty from "lodash-es/isEmpty";
import { injectIntl } from "react-intl";
import moment from "moment";
import messages from "./message";
import { PATIENT_ATTRIBUTE_FIELDS } from "../../constant";
import { getUploadDocsURL } from "../../helper/urls/user";

const { Item: FormItem } = Form;
const { Option } = Select;
const { TextArea } = Input;
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
const {
  PROGRMAID,
  REFEREDBY,
  PRODUCT,
  PATIENTTYPE,
  EMIRATE,
  FORM_REFERAL_DATE,
  IQVIA_REFERAL_DATE,
  CONSENT_SIGNED,
  CONSENT_REASON,
  PATIENT_CONTACT_DATE,
  DOSSIER_PREPARATION_DATE,
  GENERAL_COMMENT,
  CONSENT_FORM
} = PATIENT_ATTRIBUTE_FIELDS;

const DONE = "done";
const ERROR = "error";
const CONCENT = "concent";

class EditPatientAttribute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consentFormSigned: true,
      rejection: {},
      selectedCharity: [],
      removeUpload: false,
      concentForm: {}
    };
  }

  componentDidMount() {
    const { formId, attribute, insurance = {} } = this.props;
    const { data = {} } = attribute[formId] || {};
    const { charity = {}, consentForm, concentReason } = data;
    if (consentForm) {
      if (!isEmpty(consentForm)) {
        this.setState({
          concentForm: consentForm,
          removeUpload: true,
          consentFormSigned: "Yes"
        });
      } else {
        this.setState({
          consentFormSigned: "Yes"
        });
      }
    }
    if (concentReason) {
      this.setState({ consentFormSigned: "No" });
    }
    const insuranceIds = Object.keys(insurance);
    const charityIds = Object.keys(charity);
    const { rejection } = this.state;
    insuranceIds.forEach(id => {
      const { rejection: rejectionDate } = charity[id] || {};
      const { name } = insurance[id] || {};
      if (rejectionDate) {
        if (rejectionDate !== null) {
          rejection[name] = true;
        } else {
          rejection[name] = false;
        }
      } else {
        rejection[name] = false;
      }
      this.setState({ rejection: rejection, selectedCharity: charityIds });
    });
  }

  handleconsentsigned = e => {
    const { target: { value } = {} } = e;
    this.setState({ consentFormSigned: value });
  };

  handleRejectionDate = (e, name) => {
    const { rejection } = this.state;
    if (e !== null) {
      rejection[name] = true;
    } else {
      rejection[name] = false;
    }
    this.setState({ rejection: rejection });
  };

  getProgramOptions = () => {
    const { program = {} } = this.props;
    const programIds = Object.keys(program);
    const options = programIds.map(progrmaId => {
      const { name } = program[progrmaId] || {};
      return (
        <Option key={progrmaId} value={progrmaId}>
          {name}
        </Option>
      );
    });
    return options;
  };

  handleConsentChange = info => {
    const { fileList, concentForm } = this.state;
    const myfileList = {};
    myfileList[CONCENT] = info.fileList;
    this.setState({ fileList: { ...fileList, ...myfileList } });
    const { file: { status, response = {}, name = "" } = {} } = info;
    const { payload: { data: { files = [] } = {} } = {} } = response;
    if (status === DONE) {
      const data = {};
      data[CONCENT] = {};
      data[CONCENT].url = files[0];
      data[CONCENT].name = name;
      this.setState({
        removeUpload: true,
        concentForm: { ...concentForm, ...data }
      });
      const { handleSavingConcentForm } = this.props;
      handleSavingConcentForm(data);
    } else if (status === ERROR) {
      this.props.handleUploadError();
    }
  };

  handleDelete = () => {
    const { concentForm } = this.state;
    delete concentForm[CONCENT];
    this.setState({ concentForm: concentForm, removeUpload: false });
    const { handleDeletingConcentForm } = this.props;
    handleDeletingConcentForm();
  };
  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  addMoreCharity = () => {
    let { selectedCharity } = this.state;
    selectedCharity.push("");
    this.setState(
      {
        selectedCharity: selectedCharity
      },
      () => {}
    );
  };

  changeCharity = (e, index) => {
    const { selectedCharity } = this.state;
    let charity = selectedCharity;
    charity[index] = e;

    this.setState({
      selectedCharity: charity,
      index: index
    });
  };

  deleteCharity = element => {
    let { selectedCharity } = this.state;

    let index = selectedCharity.indexOf(element);
    selectedCharity.splice(index, 1);

    this.setState(
      {
        selectedCharity: selectedCharity
      },
      () => {}
    );
  };

  getCharityOptions = () => {
    const { insurance } = this.props;
    const { selectedCharity } = this.state;
    const insuranceIds = Object.keys(insurance);
    const Options = insuranceIds.map(id => {
      const { name } = insurance[id];
      return (
        <Option key={id} value={id} disabled={selectedCharity.includes(id)}>
          {name}
        </Option>
      );
    });
    return Options;
  };

  getCharityElement = () => {
    const {
      formId,
      insurance,
      attribute,
      intl: { formatMessage }
    } = this.props;
    const { rejection, selectedCharity = [] } = this.state;
    const { getFieldDecorator } = this.props.form;
    const { data: { charity = {} } = {} } = attribute[formId] || {};
    const charityOption = this.getCharityOptions();

    let charities = [];
    forEachRight(selectedCharity, (element, index) => {
      const { name } = insurance[element] || {};

      charities.unshift(
        <div className="flex" key={index}>
          <div key={index} className="mb20 ">
            <div>
              <Select
                className={
                  "iqvia-style-select charity fontsize14 full-width mb10"
                }
                value={element}
                onChange={e => {
                  this.changeCharity(e, index);
                }}
                filterOption={(inputValue, option) => {
                  console.log("inputValue", option, inputValue);
                }}
              >
                {charityOption}
              </Select>
            </div>
            <div>
              <div className="flex column align-item-center" key={element}>
                <div className="flex align-item-center ">
                  <div className="wp50 mr32">
                    <FormItem label={formatMessage(messages.dateOfSubmission)}>
                      {getFieldDecorator(`charity.${element}.submission`, {
                        initialValue:
                          charity[element] &&
                          charity[element].submission !== null
                            ? moment(charity[element].submission)
                            : null
                      })(<DatePicker />)}
                    </FormItem>
                  </div>
                  <FormItem label={formatMessage(messages.dateofRejection)}>
                    {getFieldDecorator(`charity.${element}.rejection`, {
                      initialValue:
                        charity[element] && charity[element].rejection !== null
                          ? moment(charity[element].rejection)
                          : null
                    })(
                      <DatePicker
                        onChange={e => {
                          this.handleRejectionDate(e, name);
                        }}
                      />
                    )}
                  </FormItem>
                </div>
                {rejection[name] && rejection[name] !== null && (
                  <FormItem label={formatMessage(messages.reasonOfRejection)}>
                    {getFieldDecorator(`charity.${element}.reason`, {
                      initialValue:
                        charity[element] && charity[element].reason !== null
                          ? charity[element].reason
                          : null
                    })(<TextArea className="textarea" />)}
                  </FormItem>
                )}
              </div>
            </div>
          </div>
          <div
            className="ml20 deleteIcon clickable flex align-items-center justify-content-center"
            onClick={e => {
              this.deleteCharity(element);
            }}
          >
            <Icon type="delete" />
          </div>
        </div>
      );
    });
    return charities;
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { consentFormSigned } = this.state;
    const programOption = this.getProgramOptions();

    const {
      formId,
      attribute,
      formFilled,
      intl: { formatMessage },
      userId
    } = this.props;
    const { data = {} } = attribute[formId] || {};
    const {
      referedBy,
      Product,
      PatientType,
      Emirate,
      formReferalDate,
      formReferalIqvia,
      consentSigned,
      consentForm,
      concentReason,
      patientContactDate,
      dossierPreparationDate,
      generalComment
    } = data;

    const { programId } = formFilled[formId] || {};

    const attachedDocs = this.getCharityElement();

    const { removeUpload: displayButton, concentForm } = this.state;

    return (
      <Fragment>
        <Form>
          <div className="edit-patient-attribute flex column">
            <FormItem label="Patient ProgramId">
              {getFieldDecorator(PROGRMAID, {
                initialValue: programId
              })(
                <Select className="attribute-input" disabled>
                  {programOption}
                </Select>
              )}
            </FormItem>
            <FormItem label="Refered By">
              {getFieldDecorator(REFEREDBY, {
                initialValue: referedBy
              })(<Input className="attribute-input" />)}
            </FormItem>
            <FormItem label="Product">
              {getFieldDecorator(PRODUCT, {
                initialValue: Product
              })(<Input className="attribute-input" />)}
            </FormItem>
            <FormItem label="Patient Type">
              {getFieldDecorator(PATIENTTYPE, {
                initialValue: PatientType
              })(<Input className="attribute-input" />)}
            </FormItem>
            <FormItem label="Emirate">
              {getFieldDecorator(EMIRATE, {
                initialValue: Emirate
              })(<Input className="attribute-input" />)}
            </FormItem>
            <FormItem label="Date of referal on formId:">
              {getFieldDecorator(FORM_REFERAL_DATE, {
                initialValue: formReferalDate ? moment(formReferalDate) : null
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Date of referal to IQVIA:">
              {getFieldDecorator(IQVIA_REFERAL_DATE, {
                initialValue: formReferalIqvia ? moment(formReferalIqvia) : null
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Consent Form Signed:">
              {getFieldDecorator(CONSENT_SIGNED, {
                initialValue: consentSigned
              })(
                <RadioGroup
                  className="radio-group-tab flex align-items-center mt8"
                  buttonStyle="solid"
                  onChange={this.handleconsentsigned}
                >
                  <RadioButton key={"Yes"} value={"Yes"}>
                    {formatMessage(messages.yes)}
                  </RadioButton>

                  <RadioButton key={"No"} value={"No"}>
                    {formatMessage(messages.no)}
                  </RadioButton>
                </RadioGroup>
              )}
            </FormItem>
            {consentFormSigned === "Yes" && (
              <FormItem label="If Yes, Upload">
                {getFieldDecorator(CONSENT_FORM, {
                  initialValue: concentForm
                })(
                  <div>
                    <div
                      className={`image-name flex align-items-center ${
                        concentForm[CONCENT] ? undefined : "donot-display"
                      }`}
                    >
                      <div>
                        {concentForm[CONCENT] &&
                          this.getfileName(concentForm[CONCENT].name)}
                      </div>
                      <div className="ml8">
                        <Icon
                          type="close"
                          onClick={e => {
                            this.handleDelete();
                          }}
                          className="ml5"
                        />
                      </div>
                    </div>

                    <Upload
                      action={getUploadDocsURL(userId)}
                      onChange={info => {
                        this.handleConsentChange(info);
                      }}
                      name="files"
                      className={`clickable ${concentForm[CONCENT] &&
                        "donot-display"}`}
                    >
                      {!displayButton && (
                        <Button type="default">
                          <Icon type="upload" /> Click to Upload
                        </Button>
                      )}
                    </Upload>
                  </div>
                )}
              </FormItem>
            )}
            {consentFormSigned === "No" && (
              <FormItem label="If No, Reason">
                {getFieldDecorator(CONSENT_REASON, {
                  initialValue: concentReason
                })(<TextArea className="textarea" />)}
              </FormItem>
            )}
            <FormItem label="Date of 1st contact with patient:">
              {getFieldDecorator(PATIENT_CONTACT_DATE, {
                initialValue: patientContactDate
                  ? moment(patientContactDate)
                  : null
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="Date of patient dossier started:">
              {getFieldDecorator(DOSSIER_PREPARATION_DATE, {
                initialValue: dossierPreparationDate
                  ? moment(dossierPreparationDate)
                  : null
              })(<DatePicker />)}
            </FormItem>
            <FormItem label="General Comments:">
              {getFieldDecorator(GENERAL_COMMENT, {
                initialValue: generalComment
              })(<TextArea className="textarea" />)}
            </FormItem>
            <div className="pb30">
              <div className="fontsize18 dark bold mb10">
                {formatMessage(messages.charityProvider)}
              </div>
              <div>{attachedDocs}</div>
              <Button
                className="border-none"
                onClick={e => {
                  e.preventDefault();
                  this.addMoreCharity();
                }}
                icon="plus"
              >
                {formatMessage(messages.add)}
              </Button>
            </div>
          </div>
        </Form>
      </Fragment>
    );
  }
}

const EditForm = Form.create()(EditPatientAttribute);
export default injectIntl(EditForm);
