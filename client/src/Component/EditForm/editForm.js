import React, { Component, Fragment } from "react";
import {
  Input,
  Select,
  DatePicker,
  Button,
  Upload,
  Form,
  Radio,
  Checkbox,
  Icon
} from "antd";
import forEachRight from "lodash-es/forEachRight";
import { QUESTION_TYPE } from "../../constant";
import { getUploadDocsURL } from "../../helper/urls/user";
import moment from "moment";

const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
const { Item: FormItem } = Form;
const CheckboxGroup = Checkbox.Group;
const DONE = "done";
const ERROR = "error";
const NAME = "NAME";
const {
  INPUT,
  TEXT,
  CHECKBOX,
  RADIO,
  DROPDOWN,
  ATTACHMENT,
  ATTACHMENT_PHOTO,
  DOC_TYPE,
  DATE,
  CONDITIONAL,
  EMAIL,
  NATIONALITY,
  DEPENDENT
} = QUESTION_TYPE;

const OTHER = "Others";
const NATIONALITY_OTHER = "NATIONALITY_OTHER";
const DEPENDENT_FIELD = "NO_OF_DEPENDENT";
const DEPENDENT_MALE = "NO_OF_DEPENDENT_MALE";
const DEPENDENT_FEMALE = "NO_OF_DEPENDENT_FEMALE";

const ImageHost = process.env.REACT_APP_IMAGE_HOST;

class EditFormElement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileList: {},
      docs: [],
      attachments: [],
      selectedDocs: [],
      removeUpload: false,
      allAttachments: {},
      alreadyAppliedToCharity: "",
      uploadingDocs: false
    };
  }
  componentDidMount() {
    const { formId } = this.props.match.params;
    const { filledForm } = this.props;

    const { data = {} } = filledForm[formId] || {};
    const { ATTACHMENTS = {}, PHOTO } = data;
    const selectedDocs = Object.keys(ATTACHMENTS);
    this.setState({
      selectedDocs: selectedDocs,
      attachments: [...selectedDocs],
      allAttachments: { ...ATTACHMENTS },
      imgUrl: PHOTO,
      nationality: "",
      dependents: ""
    });
  }

  // componentDidUpdate = (prevProps, prevState) => {
  //   if (this.props.filledForm !== prevProps.filledForm) {
  //     const { formId } = this.props.match.params;
  //     const { filledForm } = this.props;

  //     const { data = {} } = filledForm[formId] || {};
  //     const { ATTACHMENTS = {}, PHOTO } = data;
  //     const selectedDocs = Object.keys(ATTACHMENTS);
  //     this.setState({
  //       selectedDocs: selectedDocs,
  //       attachments: [...selectedDocs],
  //       allAttachments: { ...ATTACHMENTS },
  //       imgUrl: PHOTO
  //     });
  //   }
  // };

  addMoreAttachment = () => {
    let { attachments } = this.state;
    attachments.push(null);
    //status : 0 for new, 1 for old
    this.setState(
      {
        attachments: attachments
      },
      () => {}
    );
  };

  deleteAttachment = element => {
    const { handleDeletingAttachments } = this.props;
    handleDeletingAttachments(element);
    let { attachments, selectedDocs } = this.state;
    let index = attachments.indexOf(element);
    attachments.splice(index, 1);
    let indexinselectedDocs = selectedDocs.indexOf(element);
    selectedDocs.splice(indexinselectedDocs);

    this.setState(
      {
        attachments: attachments,
        selectedDocs: selectedDocs
      },
      () => {}
    );
  };

  handleChange = info => {
    const fileList = info.fileList;
    this.setState({ fileList: fileList });
  };

  handleAttachmentChange = (info, element) => {
    const { fileList, allAttachments } = this.state;
    const myfileList = {};
    myfileList[element] = info.fileList;
    this.setState({
      fileList: { ...fileList, ...myfileList },
      uploadingDocs: true
    });
    const { file: { status, response = {}, name = null } = {} } = info;
    const { payload: { data: { files = [] } = {} } = {} } = response;
    if (status === DONE) {
      const data = {};
      data[element] = {};
      data[element].url = files[0];
      data[element].name = name;
      this.setState({
        removeUpload: true,
        allAttachments: { ...allAttachments, ...data },
        uploadingDocs: false
      });
      const { handleSavingAttachments } = this.props;
      handleSavingAttachments(data);
    } else if (status === ERROR) {
      this.props.handleUploadError();
    }
  };

  handlePhotoChange = element => {
    const { file } = element;
    const { response: { payload, status } = {}, status: photoStatus } = file;
    if (photoStatus === DONE) {
      if (status) {
        const {
          data: { files = [] }
        } = payload;
        this.setState({ imgUrl: files[0] });
        this.props.handleImageChange(files[0]);
      }
    } else if (status === ERROR) {
      this.props.handleUploadError();
    }
  };

  handlePhotoDelete = element => {
    this.setState({ imgUrl: null });
    const { handleImageDelete } = this.props;
    handleImageDelete();
  };

  handleDelete = element => {
    const { allAttachments } = this.state;
    delete allAttachments[element];
    this.setState({ allAttachments: allAttachments });
    const { handleDeletingAttachments } = this.props;
    handleDeletingAttachments(element);
  };

  changeAttachmentType = (e, index) => {
    const { selectedDocs, attachments } = this.state;
    let attachment = attachments;
    attachment[index] = e;

    this.setState({
      attachments: attachment,
      index: index,
      selectedDocs: [...selectedDocs, e]
    });
  };

  handleAlreadyAppliedCharity = e => {
    const { value } = e.target;
    this.setState({ alreadyAppliedToCharity: value });
  };

  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  onSelectChange = key => {
    this.setState({ nationality: key });
  };

  onDependentChange = e => {
    const { setFieldsValue } = this.props.form;
    this.setState({ dependents: e.target.value });

    setFieldsValue({ [DEPENDENT_MALE]: undefined });
    setFieldsValue({ [DEPENDENT_FEMALE]: undefined });
  };

  dependentsValidator = FieldId => (rule, value, callback) => {
    let otherField =
      FieldId === DEPENDENT_MALE ? DEPENDENT_FEMALE : DEPENDENT_MALE;

    const { setFieldsValue, getFieldsValue } = this.props.form;
    const { dependents } = this.state;
    if (value && value.length > 0 && value.match("^[0-9]+$") !== null) {
      const dependentField = getFieldsValue([DEPENDENT_FIELD]);
      const totalDependents =
        dependents === ""
          ? parseInt(dependentField[DEPENDENT_FIELD])
          : parseInt(dependents);
      const fieldValue = parseInt(value);

      if (fieldValue < 0) {
        callback("Value cannot be less than 0(zero)");
      } else if (fieldValue > totalDependents) {
        callback("Value cannot be greater that Total dependents");
      } else {
        const remaining = totalDependents - fieldValue;

        setFieldsValue({ [otherField]: `${remaining}` });
        callback();
      }
    } else if (!value) {
      callback();
    } else {
      callback("Enter valid number");
    }
  };

  getFieldInputs = (question, response) => {
    const {
      QuestionId,
      Question,
      Type,
      Option: Options = [],
      FieldId
    } = question;
    const { userId } = this.props;
    const {
      getFieldDecorator,
      isFieldTouched,
      getFieldError
    } = this.props.form;

    const { nationality, dependents } = this.state;

    const nameError = isFieldTouched(NAME) && getFieldError(NAME);

    switch (Type) {
      case INPUT:
        return (
          <FormItem
            label={Question && Question}
            key={QuestionId}
            validateStatus={nameError ? "error" : ""}
            help={nameError || ""}
          >
            {getFieldDecorator(FieldId, {
              rules:
                FieldId === NAME
                  ? [
                      {
                        required: true,
                        message: "Enter a Name"
                      }
                    ]
                  : [],
              initialValue: response[FieldId] || null
            })(<Input className="mt8" />)}
          </FormItem>
        );

      case TEXT:
        return (
          <FormItem label={Question && Question} key={QuestionId}>
            {getFieldDecorator(FieldId, {
              initialValue: response[FieldId] || null
            })(<TextArea className="mt8" />)}
          </FormItem>
        );

      case EMAIL:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {
              rules: [
                {
                  type: "email",
                  message: "Enter a valid email"
                }
              ],
              initialValue: response[FieldId] || null
            })(<Input className="mt8" />)}
          </FormItem>
        );

      case CONDITIONAL:
        const { Condition = {} } = question;
        console.log("Condition--------------->", Condition);
        const { ifValue, Fields } = Condition;
        const { alreadyAppliedToCharity } = this.state;
        const fieldValue =
          alreadyAppliedToCharity === ""
            ? response[FieldId]
            : alreadyAppliedToCharity;
        return (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {
                initialValue: response[FieldId] || null
              })(
                <RadioGroup
                  className="radio-group-tab flex align-items-center mt8"
                  buttonStyle="solid"
                  onChange={this.handleAlreadyAppliedCharity}
                >
                  {Options &&
                    Options.map(singleOption => {
                      const { label, value } = singleOption;
                      return (
                        <RadioButton key={value} value={value}>
                          {label}
                        </RadioButton>
                      );
                    })}
                </RadioGroup>
              )}
            </FormItem>
            {fieldValue === ifValue &&
              Fields.map(field => {
                return this.getFieldInputs(field, response);
              })}
          </Fragment>
        );

      case DATE:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {
              initialValue: response[FieldId] ? moment(response[FieldId]) : null
            })(<DatePicker className="mt8" />)}
          </FormItem>
        );

      case RADIO:
        const radioOption = (
          <Fragment>
            <FormItem label={Question && Question} key={QuestionId}>
              {getFieldDecorator(FieldId, {
                initialValue: response[FieldId] || undefined
              })(
                <RadioGroup
                  className="radio-group-tab flex align-items-center mt8"
                  buttonStyle="solid"
                >
                  {Options &&
                    Options.map(singleOption => {
                      const { label, value } = singleOption;
                      return (
                        <RadioButton key={value} value={value}>
                          {label}
                        </RadioButton>
                      );
                    })}
                </RadioGroup>
              )}
            </FormItem>
          </Fragment>
        );
        return radioOption;

      case CHECKBOX:
        return (
          <Fragment>
            <FormItem label={Question && Question} key={QuestionId}>
              {getFieldDecorator(FieldId, {
                initialValue: response[FieldId] || undefined
              })(<CheckboxGroup options={Options} className="mt8" />)}
            </FormItem>
          </Fragment>
        );

      case DROPDOWN:
        const options = Options.map(singleoption => {
          const { label, value } = singleoption;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });
        return (
          <FormItem label={Question && Question} key={QuestionId}>
            {getFieldDecorator(FieldId, {
              initialValue: response[FieldId] || undefined
            })(<Select className="mt8">{options}</Select>)}
          </FormItem>
        );

      case ATTACHMENT_PHOTO:
        const uploadButton = (
          <div className="mt8">
            <Icon type={this.state.loading ? "loading" : "plus"} />
            <div className="ant-upload-text">Upload</div>
          </div>
        );
        return (
          <FormItem label={Question !== null && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <div className="photo-wrapper">
                <Upload
                  action={getUploadDocsURL(userId)}
                  accept="image/jpeg"
                  showUploadList={false}
                  onChange={this.handlePhotoChange}
                  name="files"
                  listType="picture-card"
                >
                  {this.state.imgUrl ? (
                    <img
                      alt=""
                      src={`${ImageHost}${this.state.imgUrl}`}
                      className="userDp"
                    />
                  ) : (
                    uploadButton
                  )}
                </Upload>
                <div className="close-icon">
                  {this.state.imgUrl && (
                    <Icon
                      type="close-circle"
                      theme="filled"
                      onClick={this.handlePhotoDelete}
                    />
                  )}
                </div>
              </div>
            )}
          </FormItem>
        );

      case ATTACHMENT:
        return (
          <FormItem label={Question && Question} key={QuestionId}>
            {getFieldDecorator(FieldId, {
              initialValue: response[FieldId] || undefined
            })(
              <Upload
                action={getUploadDocsURL(userId)}
                accept="image/jpeg,application/pdf"
                onChange={this.handleChange}
                name="files"
              >
                <Button className="mt8">
                  <Icon type="upload" /> Click to Upload
                </Button>
              </Upload>
            )}
          </FormItem>
        );

      case DOC_TYPE:
        let attachedDocs = [];
        const {
          selectedDocs,
          attachments,
          removeUpload,
          allAttachments
        } = this.state;

        const docOptions = Options.filter(singleoption => {
          const { value } = singleoption;
          if (!selectedDocs.includes(value)) return true;

          return false;
        }).map(singleoption => {
          const { label, value } = singleoption;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });
        forEachRight(attachments, (element, index) => {
          const dispalybuton =
            removeUpload &&
            (allAttachments[element] && allAttachments[element].length > 0);
          attachedDocs.unshift(
            <div className="flex " key={index}>
              <div className="mb20 flex align-items-center justify-content-space-between flex-grow-1">
                <Select
                  className={"iqvia-style-select attachment fontsize14"}
                  value={element}
                  onChange={e => {
                    this.changeAttachmentType(e, index);
                  }}
                >
                  {docOptions}
                </Select>
                <div className="attachment-name">
                  <div
                    className={`image-name flex align-items-center ${
                      allAttachments[element] ? undefined : "donot-display"
                    }`}
                  >
                    <div>
                      {allAttachments[element] &&
                        this.getfileName(allAttachments[element].name)}
                    </div>
                    <div className="ml8">
                      <Icon
                        type="close"
                        onClick={e => {
                          this.handleDelete(element);
                        }}
                        className="ml5"
                      />
                    </div>
                  </div>
                  <Upload
                    action={getUploadDocsURL(userId)}
                    // accept="image/jpeg"
                    onChange={info => {
                      this.handleAttachmentChange(info, element);
                    }}
                    disabled={element === null || element === ""}
                    name="files"
                    className={`clickable ${allAttachments[element] &&
                      "donot-display"}`}
                  >
                    {!dispalybuton && (
                      <Button className="uploading-docs-btn" disabled={element === null || element === ""}>
                        {this.state.uploadingDocs ? (
                          <Icon type="loading" />
                        ) : (
                          <div>
                            <Icon type="upload" /> Click to Upload{" "}
                          </div>
                        )}
                      </Button>
                    )}
                  </Upload>
                </div>
              </div>
              <div
                className="ml20 deleteIcon clickable flex align-items-center justify-content-center"
                onClick={e => {
                  this.deleteAttachment(element);
                }}
              >
                <Icon type="delete" />
              </div>
            </div>
          );
        });
        return (
          <FormItem label={Question !== null && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <div>
                <div>{attachedDocs}</div>
                <Button
                  className="border-none mt8"
                  onClick={e => {
                    e.preventDefault();
                    this.addMoreAttachment();
                  }}
                  icon="plus"
                >
                  Add
                </Button>
              </div>
            )}
          </FormItem>
        );

      case NATIONALITY:
        let showOtherBox = false;
        if (nationality === "" && response[FieldId] === OTHER) {
          showOtherBox = true;
        } else if (nationality !== "" && nationality === OTHER) {
          showOtherBox = true;
        }
        const nationalityoptions = Options.map(single_option => {
          const { label, value } = single_option;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });
        return (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {
                initialValue: response[FieldId] || undefined
              })(
                <Select className="mt8" onChange={this.onSelectChange}>
                  {nationalityoptions}
                </Select>
              )}
            </FormItem>
            {showOtherBox && (
              <FormItem>
                {getFieldDecorator(NATIONALITY_OTHER, {
                  initialValue: response[NATIONALITY_OTHER] || undefined
                })(<Input placeholder="Please Enter the Nationality" />)}
              </FormItem>
            )}
          </Fragment>
        );

      case DEPENDENT:
        const {
          FieldId: dependentsFieldId,
          Question: dependentsQuestion,
          Dependents
        } = question;
        return (
          <Fragment>
            <FormItem label={dependentsQuestion !== "" && dependentsQuestion}>
              {getFieldDecorator(dependentsFieldId, {
                rules: [],
                initialValue: response[FieldId] || undefined
              })(<Input type="number" onChange={this.onDependentChange} />)}
            </FormItem>
            {Dependents.map(dependent => {
              const {
                FieldId: dependentFieldId,
                Question: dependentQuestion
              } = dependent;
              let disable = true;
              if (dependents === "" && response[FieldId]) {
                disable = false;
              } else if (dependent !== "") {
                disable = false;
              }
              return (
                <FormItem label={dependentQuestion} key={dependentQuestion}>
                  {getFieldDecorator(dependentFieldId, {
                    rules: [
                      {
                        validator: this.dependentsValidator(dependentFieldId)
                      }
                    ],
                    initialValue: response[dependentFieldId] || undefined
                  })(<Input type="number" disabled={disable} />)}
                </FormItem>
              );
            })}
          </Fragment>
        );

      default:
        break;
    }
  };
  render() {
    const { getFieldInputs } = this;
    const { questions, data } = this.props;
    const getField = questions.map((question, index) => (
      <Fragment key={index}>{getFieldInputs(question, data)}</Fragment>
    ));

    return (
      <Fragment>
        <div className="flex column ">
          <div>
            <div>{getField}</div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default EditFormElement;
