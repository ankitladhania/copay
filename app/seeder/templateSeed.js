const template = require("../models/insuranceTemplate");
const Mongo = require("../../libs/mongo");
var ObjectId = require("mongodb").ObjectID;

(async () => {
  try {
    const mongo = new Mongo("charity-mongodb://charity-mongodb:27017/rpm");

    var conn = (async function() {
      try {
        const connection = await mongo.getConnection();
      } catch (err) {}
    })();

    const options = {
      formTemplate: [
        {
          type: "Section",
          component: [
            {
              QuestionId: "1",
              Question: "Applicant Photo ",
              Type: "ATTACHMENT_PHOTO",
              FieldId: "PHOTO"
            },
            {
              QuestionId: "2",
              Question: "Name",
              Type: "INPUT",
              FieldId: "NAME"
            },
            {
              QuestionId: "3",
              Question: "Date Of Birth",
              Type: "DATE",
              FieldId: "DOB"
            },
            {
              QuestionId: "4",
              Question: "Nationality",
              Type: "NATIONALITY",
              Option: [
                { label: "Emirate", value: "Emirate" },
                { label: "Indian", value: "Indian" },
                { label: "Pakistani", value: "Pakistani" },
                { label: "Egyptian", value: "Egyptian" },
                { label: "Filipino", value: "Filipino" },
                { label: "Others", value: "Others" }
              ],
              FieldId: "NATIONALITY"
            },
            {
              QuestionId: "5",
              Question: "Marital Status ",
              Type: "RADIO",
              Option: [
                { label: "Married", value: "Married" },
                { label: "Unmarried", value: "Unmarried" }
              ],
              FieldId: "MARITAL_STATUS"
            },
            {
              QuestionId: "6",
              Question: "No. Of Dependent",
              Type: "DEPENDENT",
              FieldId: "NO_OF_DEPENDENT",
              Dependents: [
                {
                  Question: "Number Of Male Dependent",
                  Type: "INPUT",
                  FieldId: "NO_OF_DEPENDENT_MALE"
                },
                {
                  Question: "Number Of FeMale Dependent",
                  Type: "INPUT",
                  FieldId: "NO_OF_DEPENDENT_FEMALE"
                }
              ]
            },
            {
              QuestionId: "7",
              Question: "Dependent Total Salary, if Working",
              Type: "INPUT",
              FieldId: "NO_OF_DEPENDENT_SALARY"
            },
            {
              QuestionId: "8",
              Question: "Qualifications ",
              Type: "DROPDOWN",
              Option: [
                { label: "Higher Secondary", value: "Higher Secondary" },
                { label: "Diploma", value: "Diploma" },
                { label: "Graduation", value: "Graduation" },
                { label: "Post-Graduation", value: "Post-Graduation" }
              ],
              FieldId: "QUALIFICATION"
            },
            {
              QuestionId: "9",
              Question: "Occupation",
              Type: "INPUT",
              FieldId: "OCCUPATION"
            },
            {
              QuestionId: "10",
              Question: "Monthly Salary in AED",
              Type: "INPUT",
              FieldId: "SALARY"
            },
            {
              QuestionId: "11",
              Question: "Religion",
              Type: "INPUT",
              FieldId: "RELIGION"
            },
            {
              QuestionId: "12",
              Question: "Mobile Number",
              Type: "INPUT",
              FieldId: "MOBILE_NUMBER"
            },
            {
              QuestionId: "13",
              Question: "Email",
              Type: "EMAIL",
              FieldId: "EMAIL"
            }
          ],
          name: "Personal Info",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "14",
              Question: "Emirates",
              Type: "DROPDOWN",
              Option: [
                { label: "Dubai", value: "Dubai" },
                { label: "Abu Dhabi", value: "Abu Dhabi" },
                { label: "Sharjah", value: "Sharjah" },
                { label: "Ajman", value: "Ajman" },
                { label: "Ras al-Khaimah", value: "Ras al-Khaimah" },
                { label: "Fujairah", value: "Fujairah" },
                { label: "Umm al-Quwain", value: "Umm al-Quwain" }
              ],
              FieldId: "EMIRATES"
            },
            {
              QuestionId: "15",
              Question: "Area",
              Type: "INPUT",
              FieldId: "AREA"
            },
            {
              QuestionId: "16",
              Question: "Type Of Accommodation",
              Type: "DROPDOWN",
              Option: [
                { label: "Apartment", value: "Apartment" },
                { label: "Villas", value: "Villas" }
              ],
              FieldId: "ACCOMODATION"
            },
            {
              QuestionId: "17",
              Question: "Building No.",
              Type: "INPUT",
              FieldId: "BUILDING_NO"
            },
            {
              QuestionId: "18",
              Question: "Apartment/Unit No.",
              Type: "INPUT",
              FieldId: "APARTMENT_NO"
            },
            {
              QuestionId: "19",
              Question: "Room No.",
              Type: "INPUT",
              FieldId: "ROOM_NO"
            },
            {
              QuestionId: "20",
              Question: "Yearly Rent in AED",
              Type: "INPUT",
              FieldId: "RENT_PER_YEAR"
            },
            {
              QuestionId: "21",
              Question: "Work Landline No.",
              Type: "INPUT",
              FieldId: "WORK_NUMBER"
            },
            {
              QuestionId: "22",
              Question: "Residence Landline Telephone No.",
              Type: "INPUT",
              FieldId: "RESIDENCE_NUMBER"
            }
          ],
          name: "Address Details",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "23",
              Question: "Type Of Diseases ",
              Type: "INPUT",
              FieldId: "DISEASE_TYPE"
            },
            {
              QuestionId: "24",
              Question: "Date of Diagnosis",
              Type: "DATE",
              FieldId: "DIAGNOSIS_DATE"
            },
            {
              QuestionId: "25",
              Question: "Type Of Treatment",
              Type: "INPUT",
              FieldId: "TREATMENT_TYPE"
            },
            {
              QuestionId: "26",
              Question: "Place Of Treatement",
              Type: "INPUT",
              FieldId: "PLACE_TREATMENT"
            },
            {
              QuestionId: "27",
              Question: "Indicated Treatement",
              Type: "INPUT",
              FieldId: "INDICATED_TREATMENT"
            },
            {
              QuestionId: "28",
              Question: "How do you reach the Hospital",
              Type: "INPUT",
              FieldId: "HOW_YOU_REACH_HOSPITAL"
            }
          ],
          name: "Health Details",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "29",
              Question: "Do you have Health Insurance ",
              Type: "RADIO",
              Option: [
                { label: "Yes", value: "Yes" },
                { label: "No", value: "No" }
              ],
              FieldId: "HEALTH_INSURANCE"
            },
            {
              QuestionId: "30",
              Question: "Health Card Type",
              Type: "INPUT",
              FieldId: "HEALTH_CARD_TYPE"
            },
            {
              QuestionId: "31",
              Question: "Heath Card Expiries Date",
              Type: "DATE",
              FieldId: "HEALTH_CARD_EXPIRE_DATE"
            },
            {
              QuestionId: "32",
              Question: "Name of treating Doctor",
              Type: "INPUT",
              FieldId: "TREATING_DOCTOR"
            },
            {
              QuestionId: "33",
              Question: "Treating Doctor Number",
              Type: "INPUT",
              FieldId: "TREATING_DOCTOR_NUMBER"
            }
          ],
          name: "Health Insurance",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "34",
              Question: "Years of Residing in UAE",
              Type: "INPUT",
              FieldId: "YEAR_OF_RESIDING_UAE"
            },
            {
              QuestionId: "35",
              Question: "Residency Number",
              Type: "INPUT",
              FieldId: "RESIDENCY_NUMBER"
            },
            {
              QuestionId: "36",
              Question: "Sponser Name",
              Type: "INPUT",
              FieldId: "SPONSER_NAME"
            },
            {
              QuestionId: "37",
              Question: "Sponser Phone Number",
              Type: "INPUT",
              FieldId: "SPONSER_PHONE_NUMBER"
            },
            {
              QuestionId: "38",
              Question: "Residency Expire Date",
              Type: "DATE",
              FieldId: "RESIDENCY_EXPIRE_DATE"
            },
            {
              QuestionId: "39",
              Question: "National ID number",
              Type: "INPUT",
              FieldId: "NATIONAL_ID_NUMBER"
            },
            {
              QuestionId: "40",
              Question: "Did you Apply for any other charity  ",
              Type: "CONDITIONAL",
              Option: [
                { label: "Yes", value: "Yes" },
                { label: "No", value: "No" }
              ],
              Condition: {
                ifValue: "Yes",
                Fields: [
                  {
                    Question: "If Yes, Name of Charity",
                    Type: "INPUT",
                    FieldId: "OTHER_CHARITY_NAME"
                  },
                  {
                    Question: "If Yes, Date of Charity Submission",
                    Type: "DATE",
                    FieldId: "OTHER_CHARITY_SUBMISSION_DATE"
                  }
                ]
              },
              FieldId: "OTHER_CHARITY_APPLIED"
            },
            {
              QuestionId: "41",
              Question: "How did you hear about our charity",
              Type: "INPUT",
              FieldId: "REFFERED_BY_CHARITY"
            },
            {
              QuestionId: "42",
              Question: "Contact person in case of Emergency",
              Type: "",
              FieldId: ""
            },
            {
              QuestionId: "43",
              Question: "Relation",
              Type: "INPUT",
              FieldId: "EMERGENCY_CONTACT_RELATION"
            },
            {
              QuestionId: "44",
              Question: "Contact Number",
              Type: "INPUT",
              FieldId: "EMERGENCY_CONTACT_NUMBER"
            }
          ],
          name: "UAE Residency For Non-Locals",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "45",
              Question: "Describe the case",
              Type: "TEXT",
              FieldId: "CASE_DESCRIPTION"
            }
          ],
          name: "Case Description",
          showHeader: true
        },
        {
          type: "Section",
          component: [
            {
              QuestionId: "46",
              Question: "Select attachment from following ",
              Type: "DOC_TYPE",
              Option: [
                { label: "Personal Photo", value: "Personal Photo" },
                {
                  label: "Residency Page",
                  value: "Residency Page"
                },
                { label: "Nation ID", value: "Nation ID" },
                { label: "Passport copy", value: "Passport copy" },
                { label: "Salary Certificate", value: "Salary Certificate" },
                { label: "Health Card Copy", value: "Health Card Copy" },
                {
                  label: "Medical Report (Not older than 3 months)",
                  value: "Medical Report (Not older than 3 months)"
                },
                {
                  label: "Prescription (Not older than 3 months)",
                  value: "Prescription (Not older than 3 months)"
                },
                {
                  label: "Letter explaining the case",
                  value: "Letter explaining the case"
                },
                { label: "Rent Contract", value: "Rent Contract" },
                {
                  label: "3 Month recent bank statement",
                  value: "3 Month recent bank statement"
                },
                {
                  label: "Father/Husband Passport Copy",
                  value: "Father/Husband Passport Copy"
                },
                { label: "Treatment Quotation", value: "Treatment Quotation" }
              ],
              FieldId: "ATTACHMENTS"
            }
          ],
          name: "Attachments",
          showHeader: true
        },
        {
          type: "component",
          component: [
            {
              QuestionId: "47",
              Type: "CHECKBOX",
              Option: [
                {
                  label:
                    "In case of any change in any previous data you have submitted, you are obliged to inform the charity about these changes",
                  value: "TermAccepted"
                }
              ],
              FieldId: "TERM"
            }
          ],
          name: "Term",
          showHeader: false
        },
        {
          type: "component",
          component: [
            {
              QuestionId: "48",
              Type: "CHECKBOX",
              Option: [
                {
                  label:
                    "I Pledge that all data submitted by myself is correct and I take full responsibility of The consequences if not correct.",
                  value: "EndorsementAccepted"
                }
              ],
              FieldId: "ENDORSEMENT"
            }
          ],
          name: "Endorsement",
          showHeader: false
        }
      ]
    };

    await template.remove({});
    let status;
    status = await template.create(options);
    console.log("status", status);
    mongo.disconnectConnection();
  } catch (err) {
    console.log("err", err);
  }
})();
