const express = require("express");
const router = express.Router();
const insuranceController = require("../../../app/controller/insurance/insurance.controller");

router.get("/allProvider", insuranceController.getAllInsuranceProvider);


module.exports = router;