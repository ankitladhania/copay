import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { IntlProvider, addLocaleData } from "react-intl";
import arLocaleData from "react-intl/locale-data/ar";
import esLocaleData from "react-intl/locale-data/es";
import hiLocalData from "react-intl/locale-data/hi";
import { composeWithDevTools } from "redux-devtools-extension";
import AppWrapper from "./AppWrapper";
import translations from "./i18n/locales";
import * as serviceWorker from "./serviceWorker";

import "./styles/index.less";
// import "antd/dist/antd.less";

import allReducers from "./Reducers";

let store;
if (process.env.NODE_ENV === "development") {
  store = createStore(allReducers, composeWithDevTools(applyMiddleware(thunk)));
} else {
  store = createStore(allReducers, applyMiddleware(thunk));
}

addLocaleData(arLocaleData);
addLocaleData(esLocaleData);
addLocaleData(hiLocalData);
//fecth locale
const locale = window.location.search.replace("?locale=", "") || "en";
const messages = translations[locale];
//

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={locale} key={locale} messages={messages}>
      <AppWrapper />
    </IntlProvider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
