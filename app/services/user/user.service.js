const userModal = require("../../models/users");
const insuranceFormFilledModal = require("../../models/InsuranceFormsFilled");
class UserService {
  constructor() {}

  async addPatientCoordinator(data) {
    try {
      const user = await userModal.create(data);
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getUserById(id) {
    try {
      const user = await userModal.find({ _id: id });
      return user;
    } catch (err) {
      throw err;
    }
  }

  async editPatientCoordinator(id, data) {
    try {
      const user = await userModal
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              name: data.name,
              email: data.email,
              programId: data.programId,
              password: data.password
            }
          },
          { new: true }
        )
        .lean();
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getAllUser() {
    try {
      const user = await userModal.find({}).lean();
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getPatientCoordinatorFormData(formIds, programId) {
    try {
      const formData = await insuranceFormFilledModal
        .find({ _id: { $in: formIds }, programId: programId })
        .lean();
      return formData;
    } catch (err) {
      throw err;
    }
  }

  async getFormDataForAllFormInProgram(programId) {
    try {
      const formData = await insuranceFormFilledModal
        .find({ programId: programId })
        .lean();
      return formData;
    } catch (err) {
      throw err;
    }
  }

  async getActiveUser() {
    try {
      const user = await userModal
        .find({
          status: "ACTIVE"
        })
        .lean();
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getUser(data) {
    try {
      const user = await userModal.findOne(data).lean();
      return user;
    } catch (err) {
      throw err;
    }
  }

  async getBasicInfo(id, fields = ["_id", "name", "category", "programId"]) {
    try {
      if (id === undefined || id == null) return;
      let user = await this.getUser({ _id: id });
      let basicInfo = {};

      for (let field in fields) {
        basicInfo = Object.assign(basicInfo, {
          [fields[field]]: user[fields[field]]
        });
      }

      return basicInfo;
    } catch (err) {
      throw err;
    }
  }

  async getPatientCoordinatorFormFilledIds(userId) {
    try {
      const user = await userModal
        .find({ _id: userId }, { formFilled: 1 })
        .lean();
      return user;
    } catch (err) {
      throw err;
    }
  }
  async addProgramId(programId, userIds) {
    try {
      const result = await userModal
        .update(
          { _id: { $in: userIds } },
          {
            $push: { programId: programId }
          },
          { new: true, multi: true }
        )
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async addForm(id, data, programId) {
    try {
      const formFilled = await insuranceFormFilledModal.create({
        data: data,
        programId: programId
      });
      const { _id: formId } = formFilled;
      const result = await userModal
        .findOneAndUpdate(
          { _id: id },
          {
            $push: { formFilled: formId }
          },
          { new: true }
        )
        .lean();
      return { user: result, formId, formFilled };
    } catch (error) {
      console.log("error", error);
    }
  }

  async UpdateForm(formId, data) {
    try {
      const result = await insuranceFormFilledModal
        .findOneAndUpdate(
          { _id: formId },
          {
            $set: { data: data }
          },
          { new: true }
        )
        .lean();

      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async deleteUser(userId) {
    try {
      const result = await userModal
        .findOneAndUpdate(
          { _id: userId },
          {
            $set: { status: "DELETED" }
          },
          { new: true }
        )
        .lean();

      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new UserService();
