import { connect } from "react-redux";
import AddProgram from "../../Component/Modal/addProgram/addProgram";
import { close } from "../../modules/modal";
import { addProgram } from "../../modules/program";

const mapStateToProps = state => {
  const { modal } = state;
  return {
    show: modal.show && modal.modalType === "ADD_PROGRAM"
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close()),
    addProgram: data => dispatch(addProgram(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProgram);
