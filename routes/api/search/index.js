const express = require("express");
const router = express.Router();
const searchController = require("../../../app/controller/search/search.controller");

router.get("/search/:programId", searchController.doSearch);


module.exports = router;