const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String
    },
    email: {
      type: String,
      required: true,
      unique: true,
      index: true,
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Please fill a valid email address"
      ]
    },
    password: {
      type: String
    },
    programId: [{ type: mongoose.Schema.Types.ObjectId, ref: "program" }],
    category: {
      required: true,
      type: String,
      enum: ["patientCoordinator", "superAdmin"],
      index: true
    },
    status: {
      required: true,
      type: String,
      enum: ["ACTIVE", "DELETED"],
      index: true
    },
    formFilled: [
      { type: mongoose.Schema.Types.ObjectId, ref: "insuranceFormFilled" }
    ]
  },
  {
    collection: "users",
    timestamps: true
  }
);

module.exports = mongoose.model("user", userSchema);
