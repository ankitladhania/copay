import React, { Fragment, Component } from "react";
import {
  Page,
  Text,
  View,
  Image,
  Document,
  StyleSheet
} from "@react-pdf/renderer";
import isEmpty from "lodash-es/isEmpty";
import Available from "../../Assets/images/available.png";
import { QUESTION_TYPE } from "../../constant";
import moment from "moment";

// Font.register(
//     `http://localhost3000/static/media/AvenirNext-Regular.1f1c6c50.ttf`,
//     { family: "AvenirNext-Regular" }
//   );

const styles = StyleSheet.create({
  page: { backgroundColor: "white", padding: "16pt" },
  header: {
    width: "100%",
    // fontFamily: "AvenirNext-Regular",
    fontSize: "16pt"
  },
  heading: {
    // fontFamily: "AvenirNext-Regular",
    fontSize: "16pt"
  },
  content: {
    // fontFamily: "AvenirNext-Regular",
    fontSize: "12pt",
    marginTop: "16pt"
  },
  section_header: {
    textAlign: "center",
    backgroundColor: "#7a67ae",
    color: "white",
    padding: "4pt"
  },
  image: {
    width: "120pt",
    height: "50pt"
  },
  response: {
    padding: "6pt",
    fontSize: "12pt",
    minHeight: "24pt"
  },
  question: {
    padding: "4pt",
    fontSize: "12pt",
    backgroundColor: "#d5c9e3"
  },
  Div: {
    width: "100%",
    borderRight: "1pt solid #b3b1b2"
  },
  input: {
    width: "50%",
    borderRight: "1pt solid #b3b1b2"
  }
});

let LOADING = true;
const ATTACHMENT_FIELD = "Attachments";
const OTHER = "Others";
const NATIONALITY_OTHER = "NATIONALITY_OTHER";
const ImageHost = process.env.REACT_APP_IMAGE_HOST;
const {
  INPUT,
  TEXT,
  CHECKBOX,
  RADIO,
  DROPDOWN,
  ATTACHMENT,
  DOC_TYPE,
  DATE,
  CONDITIONAL,
  EMAIL,
  NATIONALITY,
  DEPENDENT
} = QUESTION_TYPE;

class Doc extends Component {
  getQuestionsResponse = (questions, formFilled) => {
    let result = [];
    questions.forEach((question, index) => {
      const { Type, QuestionId: qid, Question: statement, FieldId } = question;
      if (Type === TEXT) {
        result.push(
          <View style={styles.Div} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text wrap style={styles.response}>
              {`    ${formFilled[FieldId] ? formFilled[FieldId] : ""}`}
            </Text>
          </View>
        );
      } else if (Type === INPUT) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
      } else if (Type === EMAIL) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
      } else if (Type === DATE) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`${
              formFilled[FieldId]
                ? moment(formFilled[FieldId]).format("DD-MMM-YYYY")
                : ""
            }`}</Text>
          </View>
        );
      } else if (Type === RADIO) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
      } else if (Type === DROPDOWN) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`    ${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
      } else if (Type === NATIONALITY) {
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`    ${
              formFilled[FieldId]
                ? formFilled[FieldId] === OTHER
                  ? `${OTHER}(${formFilled[NATIONALITY_OTHER]})`
                  : formFilled[FieldId]
                : ""
            }`}</Text>
          </View>
        );
      } else if (Type === DEPENDENT) {
        const { Dependents } = question;
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`   ${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
        result.push(
          Dependents.map(dependent => {
            const {
              FieldId: dependentFieldId,
              Question: dependentQuestion
            } = dependent;
            return (
              <View style={styles.input} key={`${qid}-${index}`}>
                <Text style={styles.question}>{dependentQuestion}:</Text>
                <Text style={styles.response}>{`   ${formFilled[
                  dependentFieldId
                ] || ""}`}</Text>
              </View>
            );
          })
        );
      } else if (Type === ATTACHMENT) {
        if (formFilled[FieldId]) {
          result.push(
            <View key={`${qid}-${index}`}>
              <Text style={styles.question}>{statement}</Text>
              <Image
                src={`${ImageHost}${formFilled[FieldId]}`}
                style={styles.image}
              />
            </View>
          );
        } else {
          result.push(
            <View style={styles.input} key={`${qid}-${index}`}>
              <Text style={styles.question}>{statement}</Text>
              <Text style={styles.response} />
            </View>
          );
        }
      } else if (Type === CHECKBOX) {
        let checkbox = [];
        if (formFilled[FieldId]) {
          const checkboxResponse = formFilled[FieldId] || [];

          checkboxResponse.forEach((response, index) => {
            checkbox.push(
              <Text key={index}>{`${formFilled[FieldId][response]},`} </Text>
            );
          });
        } else {
          checkbox.push(<Text key={`${qid}-${index}`}>{``} </Text>);
        }
        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}: </Text>
            <Text style={styles.response}>{`${checkbox}`}</Text>
          </View>
        );
      } else if (Type === DOC_TYPE) {
        let attachment = [];
        const { Option: attachments } = question || [];

        for (const response in attachments) {
          const { label } = attachments[response];
          const attachedDocuments = formFilled[FieldId] || {};
          const attachmentAvailable = Object.keys(attachedDocuments).includes(
            label
          );
          const isAvailable = attachmentAvailable ? Available : "";
          attachment.push(
            <View
              key={label}
              style={{
                flexDirection: "row",
                borderBottom: "1pt solid #b3b1b2",
                borderRight: "1pt solid #b3b1b2",
                marginRight: "12pt"
              }}
            >
              <View
                style={{
                  Width: "50%",
                  paddingTop: "4pt",
                  paddingBottom: "4pt",
                  minWidth: "50%",
                  borderRight: "1pt solid #b3b1b2"
                }}
              >
                <Text
                  style={{
                    marginLeft: "12pt"
                  }}
                >
                  {label}
                </Text>
              </View>
              <View
                style={{
                  maxWidth: "50%",
                  minWidth: "50%",
                  marginLeft: "12pt"
                }}
              >
                {isAvailable !== "" ? (
                  <Image
                    key={"availabilty"}
                    src={`${isAvailable}`}
                    style={{
                      height: "10pt",
                      width: "15pt",
                      marginTop: "5pt"
                    }}
                  />
                ) : (
                  <Text
                    style={{
                      marginLeft: "12pt"
                    }}
                  />
                )}
              </View>
            </View>
          );
        }

        result.push(
          <View key={`${qid}-${index}`}>
            {attachment.map(attachment => (
              <View key={attachment}>
                <View>{attachment}</View>
              </View>
            ))}
          </View>
        );
      } else if (Type === CONDITIONAL) {
        const { Condition = {} } = question;
        const { ifValue, Fields } = Condition;

        result.push(
          <View style={styles.input} key={`${qid}-${index}`}>
            <Text style={styles.question}>{statement}:</Text>
            <Text style={styles.response}>{`${formFilled[FieldId] ||
              ""}`}</Text>
          </View>
        );
        if (formFilled[FieldId] === ifValue) {
          result.push(
            Fields.map(field => {
              const {
                Question: conditionQuestion,
                FieldId: conditionFieldId,
                Type: conditionFieldType
              } = field;
              return (
                <View
                  style={styles.input}
                  key={`${qid}-${index}-${conditionQuestion}`}
                >
                  <Text style={styles.question}>{conditionQuestion}:</Text>
                  <Text style={styles.response}>{`${
                    formFilled[conditionFieldId]
                      ? conditionFieldType === DATE
                        ? moment(formFilled[conditionFieldId]).format(
                            "DD-MMM-YYYY"
                          )
                        : formFilled[conditionFieldId]
                      : ""
                  }`}</Text>
                </View>
              );
            })
          );
        }
      }
    });
    if (result.length > 1 && result.length % 2 !== 0) {
      result.push(
        <View style={styles.input} key={`extra`}>
          <Text style={styles.response}>{``}</Text>
        </View>
      );
    }
    return result;
  };

  generateResponsePdf = () => {
    const { template, formFilled: formData } = this.props;
    const templateId = Object.keys(template);
    const { formTemplate } = template[templateId] || {};

    const { formId } = this.props.match.params;
    const { data: formFilled = {} } = formData[formId] || {};
    const formResponse = formTemplate.map((template, index) => {
      const { component, name, showHeader } = template;
      if (name === "Term" || name === "Endorsement") {
        const { formFilled: formData } = this.props;
        const { formId } = this.props.match.params;
        const { data = {} } = formData[formId] || {};
        if (data["TERM"] && name === "Term") {
          const { Option = {} } = component[0] || {};
          const { label } = Option[0] || {};

          return (
            <View key={name}>
              <View
                style={[
                  styles.section_header,
                  {
                    borderTop: "1pt solid #b3b1b2",
                    borderRight: "1pt solid #b3b1b2",
                    borderLeft: "1pt solid #b3b1b2"
                  }
                ]}
              >
                <Text style={{ fontSize: "16pt" }}>{name}</Text>
              </View>
              <View
                style={{
                  borderRight: "1pt solid #b3b1b2",
                  borderLeft: "1pt solid #b3b1b2"
                }}
              >
                <Text
                  style={{
                    marginLeft: "5pt",
                    marginRight: "5pt"
                  }}
                >
                  {label}
                </Text>
              </View>
            </View>
          );
        } else if (data["ENDORSEMENT"] && name === "Endorsement") {
          const { Option = {} } = component[0] || {};
          const { label } = Option[0] || {};
          return (
            <View
              key={name}
              style={{
                borderBottom: "1pt solid #b3b1b2",
                borderRight: "1pt solid #b3b1b2",
                borderLeft: "1pt solid #b3b1b2"
              }}
            >
              <View style={{ flexDirection: "column" }}>
                <View>
                  <Text style={{ marginLeft: "5pt", marginRight: "5pt" }}>
                    {label}
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      marginLeft: "5pt",
                      marginRight: "5pt",
                      marginTop: "5pt"
                    }}
                  >
                    Date :
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      marginLeft: "5pt",
                      marginRight: "5pt",
                      marginTop: "5pt",
                      marginBottom: "5pt"
                    }}
                  >
                    Signature :
                  </Text>
                </View>
              </View>
            </View>
          );
        } else {
          return null;
        }
      } else if (showHeader) {
        return (
          <View
            key={`${name}-${index}`}
            break={name === ATTACHMENT_FIELD}
            style={{ width: "100%", margin: "8pt 0" }}
          >
            <View style={styles.section_header}>
              <Text style={{ fontSize: "14pt" }}>{name}</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                flexWrap: true,
                borderLeft: "1pt solid #b3b1b2",
                borderBottom: "1pt solid #b3b1b2"
              }}
              wrap
            >
              {this.getQuestionsResponse(component, formFilled)}
            </View>
          </View>
        );
      } else {
        return (
          <View key={name}>
            {this.getQuestionsResponse(component, formFilled)}
          </View>
        );
      }
    });

    // let trial = [];
    // trial.push(<Fragment>{result});
    const { logo = "", name = "" } = this.props;
    const photo = formFilled["PHOTO"] || {};
    return (
      <Document>
        <Page style={styles.page} size="A4">
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View>
              <Image
                key={"name"}
                src={`${ImageHost}charity-provider/${logo}`}
                style={styles.image}
              />
            </View>
            <View style={{ marginLeft: "2cm" }}>
              <Text style={{ fontSize: "12pt" }}>{name}</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
            {isEmpty(photo) ? (
              <View
                style={{
                  height: "130pt",
                  width: "120pt",
                  border: "1pt solid black",
                  flexDirection: "row",
                  alignItems: "flex-end"
                }}
              >
                <Text style={{ padding: "10pt" }}>No Photo Available</Text>
              </View>
            ) : (
              <Image
                key={"name"}
                src={`${ImageHost}${photo}`}
                style={{
                  height: "100pt",
                  width: "110pt",
                  flexDirection: "row",
                  alignItems: "flex-end"
                }}
              />
            )}
          </View>
          <View style={styles.content}>{formResponse}</View>
        </Page>
      </Document>
    );
  };

  //   createDocument = () => (
  //     <Document
  //       onRender={() => {
  //         //  LOADING = false;
  //       }}
  //     >
  //       <Page size="A4" style={styles.page}>
  //         <View style={styles.section}>
  //           <Text>Section #1</Text>
  //         </View>
  //         <View style={styles.section}>
  //           <Text>Section #2</Text>
  //         </View>
  //       </Page>
  //     </Document>
  //   );

  componentDidMount() {}

  componentWillMount() {
    //
  }

  componentDidUpdate() {
    //
  }

  componentWillReceiveProps() {
    //
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    //
    return !LOADING;
  }

  render() {
    return <Fragment>{this.generateResponsePdf()}</Fragment>;
  }
}

export default Doc;
