import React, { Component, Fragment } from "react";
import { Form, Button, Icon } from "antd";
import { Element, scroller } from "react-scroll";
import CommonError from "../CommonError";
import Menubar from "./menubar";
import FormCreation from "../FormCreation";
import "./style.less";

const ATTACHMENTS = "ATTACHMENTS";
const PHOTO = "PHOTO";
const PERSONAL_INFO = "PersonalInfo";
const DONE = "done";
const ERROR = "error";
const NAME = "NAME";

class InsuranceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attachments: {},
      imgUrl: "",
      active: PERSONAL_INFO,
      requesting: false,
      errorMsg: "",
      uploadError: false
    };
  }

  componentDidMount() {
    this.props.form.validateFields();
  }

  scrollToElement = name => {
    scroller.scrollTo(name, {
      duration: 1000,
      delay: 0,
      smooth: true,
      offset: -180
    });
  };

  setActive = val => {
    this.setState({ active: val });
  };

  handleSavingAttachments = attachment => {
    const { attachments } = this.state;
    this.setState({ attachments: { ...attachments, ...attachment } }, () => {});
  };

  handleDeletingAttachments = attachment => {
    const { attachments } = this.state;
    delete attachments[attachment];
    this.setState({ attachments: { ...attachments } }, () => {});
  };

  handleUploadError = () => {
    this.setState({
      uploadError: true,
      errorMsg: "Something went wrong. Please try again."
    });
  };

  handlePhotoChange = element => {
    const { file } = element;
    const { response: { payload, status } = {}, status: photoStatus } = file;
    if (photoStatus === DONE) {
      if (status) {
        const {
          data: { files = [] }
        } = payload;
        this.setState({ imgUrl: files[0] });
      }
    } else if (photoStatus === ERROR) {
      this.setState({
        uploadError: true,
        errorMsg: "Something went wrong. Please try again."
      });
    }
  };

  handlePhotoDelete = element => {
    this.setState({ imgUrl: undefined });
  };

  hasErrors = () => {
    const { getFieldsValue } = this.props.form;
    const value = getFieldsValue([NAME]);
    if (
      value[NAME] === undefined
    ) {
      return true;
    }
    return false;
  };
  handleSubmit = e => {
    e.preventDefault();
    const { addFormData } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values[ATTACHMENTS] = this.state.attachments;
        if (this.state.imgUrl !== "" && this.state.imgUrl !== null) {
          values[PHOTO] = this.state.imgUrl;
        }
        const { programId } = this.props.match.params;
        this.setState({ requesting: true });
        addFormData(values, programId).then(response => {
          const { status, payload } = response;
          if (status) {
            const {
              data: { formData }
            } = payload;
            this.setState({ requesting: false });

            const formId = Object.keys(formData);
            this.props.history.push({
              pathname: `/form/${formId}`,
              state: { selectedTab: "MasterForm" }
            });
          } else {
            this.setState({ errorMsg: payload.error });
          }
        });
      }
    });
  };

  resetMsg = () => {
    this.setState({ errorMsg: "" });
  };

  render() {
    const { template } = this.props;
    const templateId = Object.keys(template);
    const { formTemplate = [] } = template[templateId] || {};
    const disabled = this.hasErrors();

    const formElement = formTemplate.map((template, index) => {
      const { component, name, showHeader } = template;
      if (showHeader) {
        return (
          <Fragment key={index}>
            <Element name={name} className="mt32">
              <div className="fontsize18 dark mb16 bold">{name}</div>
              <FormCreation
                questions={component}
                {...this.props}
                imgUrl={this.state.imgUrl}
                handleSavingAttachments={this.handleSavingAttachments}
                handleDeletingAttachments={this.handleDeletingAttachments}
                handlePhotoChange={this.handlePhotoChange}
                handlePhotoDelete={this.handlePhotoDelete}
                handleUploadError={this.handleUploadError}
              />
            </Element>
          </Fragment>
        );
      } else {
        return (
          <Fragment key={index}>
            <div className="mt16">
              <FormCreation
                questions={component}
                {...this.props}
                handleSavingAttachments={this.handleSavingAttachments}
              />
            </div>
          </Fragment>
        );
      }
    });

    if (formElement.length <= 0) {
      return (
        <Fragment>
          <Icon type="loading" className="loading-icon"/>
        </Fragment>
      );
    }
    const { requesting, errorMsg, uploadError } = this.state;
    return (
      <Fragment>
        <div className="insurance-master-form">
          <div className="menubar mt10">
            <Menubar
              scrollToElement={this.scrollToElement}
              setActive={this.setActive}
              active={this.state.active}
              template={this.props.template}
            />
          </div>
          <div className="insurance-form ">
            <Form>
              {formElement}
              <Button
                className="iqvia mt10"
                type="primary"
                loading={requesting}
                disabled={disabled}
                onClick={this.handleSubmit}
              >
                Submit
              </Button>
            </Form>
          </div>
          {(uploadError || errorMsg.length > 0) && (
            <CommonError
              msg={errorMsg}
              close={this.resetMsg}
              className="insurance-form-error"
            />
          )}
        </div>
      </Fragment>
    );
  }
}

export default Form.create()(InsuranceForm);
