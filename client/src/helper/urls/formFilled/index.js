// import changePassword from "../../../Containers/page/changePassword";

export const getFormFilledForProgramURL = (programId = "") => {
  return `/getAllFormFilled/${programId}`;
};

export const getFormByIdURL = formId => {
  return `/${formId}`;
};

export const getFormFilledForCharityURL = (providerId = "") => {
  return `/getcharityForms/${providerId}`;
};
