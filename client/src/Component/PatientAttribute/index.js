import React, { Component, Fragment } from "react";
import ReadMode from "./readmode";
import EditForm from "./edit";
import "./style.less";
import { MODE } from "../../constant";

const { EDIT } = MODE;

class PatientAttribute extends Component {
  componentDidMount() {
    const { formId } = this.props.match.params;
    const { fetchPatientAttributeById } = this.props;
    fetchPatientAttributeById(formId);
  }
  render() {
    const { mode, setAttributeRef } = this.props;
    const { formId } = this.props.match.params;
    return (
      <Fragment>
        {mode === EDIT ? (
          <EditForm
            wrappedComponentRef={setAttributeRef}
            {...this.props}
            formId={formId}
          />
        ) : (
          <ReadMode {...this.props} formId={formId} />
        )}
      </Fragment>
    );
  }
}

export default PatientAttribute;
