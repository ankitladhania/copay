import React, { Component } from "react";
import FormPdf from "../../Container/FormPdf";
import EditForm from "../EditForm";
import { MODE } from "../../constant";

const { READ } = MODE;

class MasterForm extends Component {
  render() {
    const {
      mode,
      setMasterFormRef,
      handleSavingAttachments,
      handleDeletingAttachments
    } = this.props;
    if (mode === READ) {
      return <FormPdf {...this.props} />;
    }
    return (
      <EditForm
        {...this.props}
        wrappedComponentRef={setMasterFormRef}
        handleDeletingAttachments={handleDeletingAttachments}
        handleSavingAttachments={handleSavingAttachments}
      />
    );
  }
}

export default MasterForm;
