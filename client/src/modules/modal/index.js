const OPEN_MODAL = "OPEN_MODAL";
const CLOSE_MODAL = "CLOSE_MODAL";

const initialState = {
  show: false
};

export const open = (modalType, entityId, purpose, data) => {
  return dispatch => {
    dispatch({
      type: OPEN_MODAL,
      payload: { modalType, entityId: entityId, purpose, data }
    });
  };
};

export const close = () => {
  return dispatch => {
    dispatch({ type: CLOSE_MODAL });
  };
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case OPEN_MODAL:
      return {
        show: true,
        modalType: payload.modalType,
        entityId: payload.entityId,
        purpose: payload.purpose,
        data: payload.data,
        subentity: payload.data
      };

    case CLOSE_MODAL:
      return {
        show: false
      };
    default:
      return state;
  }
};
