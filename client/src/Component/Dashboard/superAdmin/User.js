import React, { Component, Fragment } from "react";
import { Form, Button, Table } from "antd";
import { USER_STATUS } from "../../../constant";

class AddUser extends Component {
  componentDidMount() {
    const { getPatientCoordinator } = this.props;
    getPatientCoordinator();
  }

  handleEdit = key => {
    const { openEditUser } = this.props;
    openEditUser(key.id);
  };

  handleOnClick = key => {
    const { id } = key;
    this.props.history.push(`/patientCoordinator/${id}`);
  };

  render() {
    const columns = [
      {
        title: "Name",
        dataIndex: "name",
        render: (text, record) => {
          return (
            <div
            // className="clickable"
            // onClick={e => this.handleOnClick(record)}
            >
              {record.name}
            </div>
          );
        }
      },
      {
        title: "Program Names",
        dataIndex: "programNames",
        key: "programNames"
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email"
      },
      {
        title: "Edit",
        dataIndex: "edit",
        render: (text, record) => (
          <Button
            className="iqvia"
            type="ghost"
            onClick={e => this.handleEdit(record)}
          >
            Edit
          </Button>
        )
      }
    ];

    const { user, program, user_data } = this.props;
    let data = [];
    const userIds = Object.keys(user);
    data = userIds
      .filter(userId => {
        const { status } = user[userId];
        if (userId !== user_data._id && status === USER_STATUS.ACTIVE)
          return true;

        return false;
      })
      .map(userId => {
        const { name, programId = [], email, _id } = user[userId] || {};
        let programNames = [];
        if (programId.length > 0) {
          programNames = programId.map(id => {
            const { name = "" } = program[id] || {};
            return `${name}, `;
          });
        }
        return {
          name,
          programNames,
          email,
          id: _id
        };
      });

    return (
      <Fragment>
        <Table columns={columns} dataSource={data} />
      </Fragment>
    );
  }
}

export default Form.create()(AddUser);
