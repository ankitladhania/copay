const bcrypt = require("bcrypt");
const user = require("../models/users");
const Mongo = require("../../libs/mongo");
var ObjectId = require("mongodb").ObjectID;

(async () => {
  try {
    const mongo = new Mongo("charity-mongodb://charity-mongodb:27017/rpm");

    var conn = (async function() {
      try {
        const connection = await mongo.getConnection();
      } catch (err) {}
    })();

    const salt = await bcrypt.genSalt(Number(process.config.saltRounds));
    const options = [
      {
        _id: ObjectId("111000000000"),
        name: "Super Admin",
        email: "superadmin@rpm.com",
        category: "superAdmin",
        status: "ACTIVE",
        password: await bcrypt.hash("password", salt)
      },
      {
        _id: ObjectId("444000000000"),
        name: "Care Coach",
        email: "patientcoordinator@rpm.com",
        password: await bcrypt.hash("password", salt),
        status: "ACTIVE",
        programId: [],
        category: "patientCoordinator"
      }
    ];
    await user.remove({});
    let status;
    status = await user.create(options);
    console.log("status", status);
    mongo.disconnectConnection();
  } catch (err) {
    console.log("err========================>", err);
  }
})();
