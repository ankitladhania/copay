const Log = require("../../../libs/log")("programController");
const ProgramService = require("../../services/program/program.service");
const UserService = require("../../services/user/user.service");
const Response = require("../../helper/responseFormat");
const errMessage = require("../../../config/messages.json").errMessages;

class ProgramController {
  constructor() {}

  async addProgram(req, res) {
    try {
      if (req.userDetails.exists) {
        const { userId } = req.userDetails;
        const { data: formData } = req.body;
        formData.programCreatedBy = userId;
        formData.tracker = [];
        const result = await ProgramService.addProgram(formData);
        let data = {};
        const { _id } = result;
        data[_id] = result;
        let response = new Response(true, 200);
        response.setData({
          program: data
        });
        response.setMessage("Program Added Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      if (error.code && error.code == 11000) {
        console.log("error=========>>", error.code);

        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_PROGRAM);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        let response = new Response(false, 500);
        response.setError(errMessage.INTERNAL_SERVER_ERROR);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      }
    }
  }

  async editProgram(req, res) {
    try {
      if (req.userDetails.exists) {
        const { programId } = req.params;
        const { data: formData } = req.body;
        const result = await ProgramService.editProgram(programId, formData);
        let data = {};
        const { _id } = result;
        data[_id] = result;

        let response = new Response(true, 200);
        response.setData({
          program: data
        });
        response.setMessage("Program Updated Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      if (error.code && error.code == 11000) {
        console.log("error", error);

        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_PROGRAM);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        let response = new Response(false, 500);
        response.setError(errMessage.INTERNAL_SERVER_ERROR);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      }
    }
  }

  async getAllProgram(req, res) {
    try {
      if (req.userDetails.exists) {
        const result = await ProgramService.getAllProgram();
        let data = {};
        result.forEach(value => {
          const { _id } = value;
          data[_id] = value;
        });
        let response = new Response(true, 200);
        response.setData({
          program: data
        });
        return res.send(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async addTrackerInProgram(req, res) {
    try {
      if (req.userDetails.exists) {
        const { programId } = req.params;
        const { data } = req.body;
        const { file, updatedBy } = data;
        let tracker = {};
        tracker.file = file;
        tracker.updatedBy = updatedBy;
        tracker.updateAt = new Date();
        console.log("formData====================>", tracker);
        const result = await ProgramService.addTrackerInProgram(
          programId,
          tracker
        );
        let programData = {};
        const { _id } = result;
        programData[_id] = result;

        let response = new Response(true, 200);
        response.setData({
          program: programData
        });
        response.setMessage("Program tracker added Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      if (error.code && error.code == 11000) {
        console.log("error", error);

        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_PROGRAM);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        let response = new Response(false, 500);
        response.setError(errMessage.INTERNAL_SERVER_ERROR);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      }
    }
  }

  async getTrackerForProgram(req, res) {
    try {
      if (req.userDetails.exists) {
        const { programId } = req.params;

        const result = await ProgramService.getTrackerForProgram(programId);
        const { tracker } = result[0] || {};
        let user = {};
        for (const key in tracker) {
          const { updatedBy } = tracker[key] || {};
          const userData = await UserService.getBasicInfo(updatedBy);
          user[updatedBy] = userData;
        }

        console.log("result, user", result, user);
        let response = new Response(true, 200);
        response.setData({
          programData: result,
          user
        });
        response.setMessage("Program tracker added Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      if (error.code && error.code == 11000) {
        console.log("error", error);

        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_PROGRAM);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        let response = new Response(false, 500);
        response.setError(errMessage.INTERNAL_SERVER_ERROR);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      }
    }
  }

  async getProgramData(req, res) {
    try {
      if (req.userDetails.exists) {
        const { programId } = req.params;

        const result = await ProgramService.getProgramById(programId);
        console.log("result========================>", result);
        let data = {};
        result.forEach(value => {
          const { _id } = value;
          data[_id] = value;
        });
        console.log("result, user", result);
        let response = new Response(true, 200);
        response.setData({
          program: data
        });
        response.setMessage("Program tracker added Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      if (error.code && error.code == 11000) {
        console.log("error", error);

        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_PROGRAM);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        let response = new Response(false, 500);
        response.setError(errMessage.INTERNAL_SERVER_ERROR);
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      }
    }
  }
}

module.exports = new ProgramController();
