// import changePassword from "../../../Containers/page/changePassword";

export const getProgramURL = (userId = "") => {
  return "/getProgram";
};

export const getAddProgramURL = () => {
  return `/addProgram`;
};

export const getEditProgramByIdURL = (programId = "") => {
  return `/editProgram/${programId}`;
};
export const getAddTrackerURL = programId => {
  return `/addTracker/${programId}`;
};
export const getProgramTrackerURL = programId => {
  return `/program/${programId}/tracker`;
};
export const getProgramDataByIdURL = programId => {
  return `/program/${programId}`;
};
