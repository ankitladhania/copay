const express = require("express");
const router = express.Router();
const multer = require("multer");
const userController = require("../../../app/controller/user/user.controller");
const formsController = require("../../../app/controller/forms/forms.controller");

const { check, body } = require("express-validator/check");
import Authenticate from "../middleware/auth";
const storage = multer.memoryStorage();
const upload = multer({ dest: "../app/public/", storage: storage });

const PASSWORD_LENGTH = 6;
const STRING = "string";
const BOOLEAN = "boolean";

router.post(
  "/sign-in",
  [
    check("email")
      .isEmail()
      .withMessage("email is not valid"),
    check("password").isLength({ min: PASSWORD_LENGTH })
  ],
  userController.signIn
);

router.post("/sign-out", Authenticate, userController.signOut);

router.get("/getActiveUser", Authenticate, userController.getActiveUser);
router.get(
  "/getPatientCoordinatorProgram",
  Authenticate,
  userController.getPatientCoordinatorProgram
);
router.post(
  "/:id/editPatientCoordinator",
  Authenticate,
  userController.EditPatientCoordinator
);
router.get(
  "/getAllFormFilled/:programId",
  Authenticate,
  userController.getPatientCoordinatorFormFilled
);
router.post("/addProgramToUser", Authenticate, userController.addProgramIds);
router.get("/get-basic-info", Authenticate, userController.onAppStart);
router.post("/addUser", Authenticate, userController.addUser);
router.post(
  "/addForm",
  Authenticate,
  userController.addFormToPatientCoordinator
);
router.post(
  "/:id/uploadDocs",
  upload.single("files"),
  userController.uploadDocs
);
router.get("/patientCoordinator/:userId", userController.getUserPrograms);

router.post("/:formId/editForm", Authenticate, formsController.updateFormData);
router.delete("/delete/:userId", Authenticate, userController.deleteUser);
module.exports = router;
