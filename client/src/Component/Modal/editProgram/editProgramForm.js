import React, { Component, Fragment } from "react";
import { Form, Input } from "antd";
import messages from "../message";

const { Item: FormItem } = Form;
const { TextArea } = Input;
const NAME = "name";
const DESCRIPTION = "description";

const FIELDS = [NAME];

class EditProgramForm extends Component {
  componentDidMount() {
    this.props.form.validateFields();
  }
  render() {
    const {
      intl: { formatMessage },
      form,
      programs,
      programId
    } = this.props;

    const program = programs[programId] || {};
    const { getFieldDecorator, getFieldError } = form;
    let fieldsError = {};
    FIELDS.forEach(value => {
      const error = getFieldError(value);
      fieldsError = { ...fieldsError, [value]: error };
    });
    return (
      <Fragment>
        <div className="pl48 pr24">
          <Form>
            <FormItem
              label={formatMessage(messages.name)}
              validateStatus={fieldsError[NAME] ? "error" : ""}
              help={fieldsError[NAME] || ""}
            >
              {getFieldDecorator(NAME, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.enterName)
                  }
                ],
                initialValue: program.name
              })(<Input />)}
            </FormItem>
            <FormItem label={formatMessage(messages.email)}>
              {getFieldDecorator(DESCRIPTION, {
                initialValue: program.description
              })(<TextArea type="email" />)}
            </FormItem>
          </Form>
        </div>
      </Fragment>
    );
  }
}

export default EditProgramForm;
