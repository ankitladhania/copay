const mongoose = require("mongoose");
const collectionName = "insuranceFormFilled";

const InsuranceFormFilledSchema = new mongoose.Schema(
  {
    data: { type: Object },
    programId: { type: mongoose.Schema.Types.ObjectId, ref: "program" }
  },
  {
    collection: collectionName,
    timestamps: true
  }
);

module.exports = mongoose.model(
  "insuranceFormFilled",
  InsuranceFormFilledSchema
);
