import { doRequest } from "../../helper/network";
import { PatientAttribute } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_PATIENT_ATTRIBUTE = "FETCHING_PATIENT_ATTRIBUTE";
const FETCHING_PATIENT_ATTRIBUTE_COMPLETED =
  "FETCHING_PATIENT_ATTRIBUTE_COMPLETED";
const FETCHING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR =
  "FETCHING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR";

const ADDING_PATIENT_ATTRIBUTE = "ADDING_PATIENT_ATTRIBUTE";
export const ADDING_PATIENT_ATTRIBUTE_COMPLETED =
  "ADDING_PATIENT_ATTRIBUTE_COMPLETED";
const ADDING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR =
  "ADDING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR";

function setPateintAttribute(state, data) {
  const { attribute } = data;
  if (attribute) {
    let newState = attribute;
    return newState;
  } else {
    return state;
  }
}

export const fetchPatientAttributeById = formId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_PATIENT_ATTRIBUTE });
      let response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: PatientAttribute.getPatientAttributeByIdURL(formId)
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_PATIENT_ATTRIBUTE_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const updatePatientAttribute = (id, data, attributeId) => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_PATIENT_ATTRIBUTE });
      let response = await doRequest({
        data: { data, attributeId },
        url: PatientAttribute.getUpdatePatientAttributeByIdURL(id),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_PATIENT_ATTRIBUTE_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_PATIENT_ATTRIBUTE_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return status;
    } catch (error) {
      throw error;
    }
  };
};

const intialState = {};

export default (state = intialState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case FETCHING_PATIENT_ATTRIBUTE_COMPLETED:
      return setPateintAttribute(state, payload);
    case ADDING_PATIENT_ATTRIBUTE_COMPLETED:
      const { data } = payload;
      return setPateintAttribute(state, data);

    default:
      return { ...state };
  }
};
