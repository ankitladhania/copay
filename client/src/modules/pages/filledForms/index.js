import { doRequest } from "../../../helper/network";
import { FormFilled } from "../../../helper/urls";
import { REQUEST_TYPE } from "../../../constant";

const FETCHING_USER_FORM_FILLED = "FETCHING_USER_FORM_FILLED";
export const FETCHING_USER_FORM_FILLED_COMPLETED =
  "FETCHING_USER_FORM_FILLED_COMPLETED";
const FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR =
  "FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR";

function setProviderFormData(state, data = {}) {
  const { providerForms } = data;

  if (providerForms) {
    let newState = Object.assign({}, providerForms);

    //
    return newState;
  } else return state;
}

export const getPatientCoordinatorFormFilled = programId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER_FORM_FILLED });
      let response = await doRequest({
        url: FormFilled.getFormFilledForProgramURL(programId),
        method: REQUEST_TYPE.GET
      });

      // console.log(
      //   "response===========>",
      //   response,
      //   response === "Error: Network Error"
      // );
      const { status, payload } = response;

      if (status === true) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      console.log("error", error);
    }
  };
};

export const getformFilledforCharity = (providerId, programId) => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER_FORM_FILLED });
      let response = await doRequest({
        params: { query: programId },
        url: FormFilled.getFormFilledForCharityURL(providerId),
        method: REQUEST_TYPE.GET
      });
      console.log("response", response);
      const { status, payload } = response;

      if (status === true) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      console.log("error", error);
      //
    }
  };
};

const initialState = {};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCHING_USER_FORM_FILLED_COMPLETED:
      return setProviderFormData(state, payload);

    default:
      return setProviderFormData(state, payload);
  }
};
