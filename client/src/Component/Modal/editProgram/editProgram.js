import React, { Component, Fragment } from "react";
import { Modal, Button, Form } from "antd";
import { injectIntl } from "react-intl";
import CommonError from "../../CommonError";
import EditProgramForm from "./editProgramForm";
import messages from "../message";

const hasErrors = fieldsError => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
};
let errMessage = "";

class EditProgram extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requesting: false,
      disabledSubmit: true,
      is_error: false
    };
    this.FormWrapper = Form.create({ onFieldsChange: this.onFormFieldChanges })(
      EditProgramForm
    );
  }

  onFormFieldChanges = (props, allvalues) => {
    const {
      form: { getFieldsError, isFieldsTouched }
    } = props;
    const isError = hasErrors(getFieldsError());
    const { disabledSubmit } = this.state;
    if (disabledSubmit !== isError && isFieldsTouched()) {
      this.setState({ disabledSubmit: isError });
    }
  };

  handleCancel = e => {
    if (e) {
      e.preventDefault();
    }
    const { close } = this.props;
    close();
  };

  setFormRef = formRef => {
    this.formRef = formRef;
  };

  handleSubmit = e => {
    e.preventDefault();
    const { editProgram, close, programId } = this.props;

    const { formRef = {} } = this;
    const {
      props: {
        form: { validateFields }
      }
    } = formRef;

    validateFields((err, values) => {
      if (!err) {
        this.setState({ requesting: true });
        editProgram(programId, { data: values }).then(response => {
          const { status, payload } = response;
          if (status) {
            this.setState({ requesting: false, disabledSubmit: true });
            close();
          } else {
            const { error: { message = "" } = {} } = payload;
            errMessage = message;
            this.setState({ requesting: false, is_error: true });
          }
        });
      }
    });
  };

  footer = () => {
    const { handleCancel, handleSubmit } = this;
    const { requesting, disabledSubmit } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="flex align-items-center justify-content-end h72px mr24">
        <Button className="iqvia-btn cancel mr8" onClick={handleCancel}>
          {formatMessage(messages.cancel)}
        </Button>
        <Button
          className="iqvia-btn warning"
          type="primary"
          loading={requesting}
          onClick={handleSubmit}
          disabled={disabledSubmit}
        >
          {formatMessage(messages.submit)}
        </Button>
      </div>
    );
  };

  clearMsg = e => {
    //e.preventDefault();
    this.setState({
      is_link_send: false,
      is_error: false
    });
  };

  render() {
    const { show: visible, isError = {} } = this.props;

    if (visible === false) {
      return null;
    }

    const { handleCancel, footer, FormWrapper, setFormRef } = this;

    const modalProps = {
      visible: visible || isError,
      title: "Edit Program",
      okButtonProps: {},
      onCancel: handleCancel,
      wrapClassName: "iqvia_modals full-height",
      destroyOnClose: true,
      bodyStyle: { height: "100%" },
      width: "480px",
      footer: footer()
    };
    return (
      <Modal {...modalProps}>
        <Fragment>
          <FormWrapper wrappedComponentRef={setFormRef} {...this.props} />
          {this.state.is_error && (
            <CommonError
              msg={errMessage}
              className={"superAdmin-new-user-failure-message"}
              close={this.clearMsg}
            />
          )}
        </Fragment>
      </Modal>
    );
  }
}

export default injectIntl(EditProgram);
