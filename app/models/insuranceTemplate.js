const mongoose = require("mongoose");

const insuranceFormTemplateSchema = new mongoose.Schema(
  {
    formTemplate: [Object]
  },
  {
    collection: "insuranceFormTemplate",
    timestamps: true
  }
);

module.exports = mongoose.model(
  "insuranceFormTemplate",
  insuranceFormTemplateSchema
);
