import PatientAttribute from "../../Component/PatientAttribute";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { fetchPatientAttributeById } from "../../modules/patientAttribute";
import { getProgram } from "../../modules/program";
const mapStateToProps = state => {
  const { attribute, program, insurance, formFilled, auth } = state;
  return {
    attribute,
    program,
    insurance,
    formFilled,
    userId: auth.authenticated_user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchPatientAttributeById: formId =>
      dispatch(fetchPatientAttributeById(formId)),
    getProgram: () => dispatch(getProgram())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PatientAttribute)
);
