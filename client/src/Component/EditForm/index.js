import React, { Component, Fragment } from "react";
import { Form, Icon } from "antd";
import isEmpty from "lodash-es/isEmpty";

import { Element, scroller } from "react-scroll";
import EditFormElement from "./editForm";
import Menubar from "./menubar";
import "./style.less";

const NAME = "NAME";

class EditForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      attachments: {},
      active: "Personal Info"
    };
  }
  // componentDidMount() {
  //   const { fetchInsurance, fetchTemplate, getFormDatabyId } = this.props;
  //   const { formId } = this.props.match.params;

  //   fetchInsurance();
  //   fetchTemplate();
  //   getFormDatabyId(formId);
  // }

  // componentDidUpdate = (prevProps, prevState) => {
  //   if (this.props.filledForm !== prevProps.filledForm) {
  //     const { formId } = this.props.match.params;
  //     const { filledForm, attachments } = this.props;

  //     const { data = {} } = filledForm[formId] || {};
  //     const { ATTACHMENTS = {} } = data;
  //     this.setState({
  //       attachments: { ...attachments, ...ATTACHMENTS }
  //     });
  //   }
  // };
  scrollToElement = name => {
    scroller.scrollTo(name, {
      duration: 1000,
      delay: 0,
      smooth: true,
      offset: -180
    });
  };

  setActive = val => {
    this.setState({ active: val });
  };

  hasErrors = () => {
    const { form, hasErrors } = this.props;
    const { getFieldsValue } = form;
    const value = getFieldsValue([NAME]);
    if (value[NAME] === undefined) {
      hasErrors(true);
    }
    hasErrors(false);
  };

  render() {
    const {
      template,
      filledForm = {},
      handleSavingAttachments,
      handleDeletingAttachments
    } = this.props;
    const { formId } = this.props.match.params;

    const { data } = filledForm[formId] || {};
    const templateId = Object.keys(template);
    const { formTemplate = [] } = template[templateId] || {};
    let formResponse = [];
    if (!isEmpty(data)) {
      formResponse = formTemplate.map((template, index) => {
        const { component, name, showHeader } = template;
        if (showHeader) {
          return (
            <Fragment key={index}>
              <Element name={name} className="mt32" key={index}>
                <div className="fontsize22 dark bold mt32 mb16">{name}</div>
                <EditFormElement
                  questions={component}
                  data={data}
                  {...this.props}
                  handleSavingAttachments={handleSavingAttachments}
                  handleDeletingAttachments={handleDeletingAttachments}
                />
              </Element>
            </Fragment>
          );
        } else {
          return (
            <Fragment key={index}>
              <div className="mt16" key={index}>
                <EditFormElement
                  questions={component}
                  data={data}
                  {...this.props}
                />
              </div>
            </Fragment>
          );
        }
      });
    }

    if (formResponse.length <= 0) {
      return (
        <Fragment>
          <div className="editForm flex align-item-center justify-content-center">
            <Icon type="loading" className="loading-icon" />
          </div>
        </Fragment>
      );
    }

    return (
      <Fragment>
        <div className="editForm">
          <div className="menubar ">
            <Menubar
              scrollToElement={this.scrollToElement}
              setActive={this.setActive}
              active={this.state.active}
              template={this.props.template}
            />
          </div>
          <div className="insurance-form">
            <Form>{formResponse}</Form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Form.create()(EditForm);
