import { connect } from "react-redux";
import ViewAllTrackerModal from "../../Component/Modal/viewAllTracker";
import { close } from "../../modules/modal";
import { fetchProgramTracker } from "../../modules/program";

const mapStateToProps = state => {
  const { modal, user } = state;
  return {
    show: modal.show && modal.modalType === "VIEW_ALL_TRACKER",
    programId: modal.entityId,
    user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close()),
    fetchProgramTracker: programId => dispatch(fetchProgramTracker(programId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewAllTrackerModal);
