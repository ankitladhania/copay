const Log = require("../../../libs/log")("TemplateController");
const TemplateService = require("../../services/template/template.servide");
const Response = require("../../helper/responseFormat");

class TemplateController {
  constructor() {}

  async getTemplate(req, res) {
    try {
      const result = await TemplateService.getFormTemplate();
      let data = {};
      result.forEach(value => {
        const { _id } = value;
        data[_id] = value;
      });
      let response = new Response(true, 200);
      response.setData({
        template: data
      });

      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }
}

module.exports = new TemplateController();
