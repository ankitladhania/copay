import { doRequest } from "../../helper/network";
import { User } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_USER = "FETCHING_USER";
export const FETCHING_USER_COMPLETED = "FETCHING_USER_COMPLETED";
const FETCHING_USER_COMPLETED_WITH_ERROR = "FETCHING_USER_COMPLETED_WITH_ERROR";

function setUsersData(state, data = {}) {
  const { programPatientCoordinator } = data;

  if (programPatientCoordinator) {
    // const userIds = Object.keys(programPatientCoordinator);
    // const prevUser = { ...state };
    let newState = Object.assign({}, programPatientCoordinator);

    return newState;
  } else return state;
}

export const getPatientCoordinator = programId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER });
      let response = await doRequest({
        url: User.getPatientCoordinatorForProgramURL(programId),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_USER_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
    } catch (error) {
      throw error;
      //
    }
  };
};

const initialState = {};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCHING_USER_COMPLETED:
      return setUsersData(state, payload);

    default:
      return setUsersData(state, payload);
  }
};
