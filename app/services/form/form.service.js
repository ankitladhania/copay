const formModal = require("../../models/InsuranceFormsFilled");
const patientAttributeService = require("../../services/patientAttribute/patientAttribute.service");
class FormService {
  constructor() {}

  async getFormDataById(id) {
    try {
      const result = await formModal.find({ _id: id }).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getFormDataByIds(ids) {
    try {
      const result = await formModal.find({ _id: { $in: ids } }).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getFormCountforProgram(programId, formFilled) {
    try {
      const result = await formModal
        .find({ programId: programId, _id: { $in: formFilled } })
        .countDocuments();

      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getFormForCharoty(providerId, programId) {
    try {
      const formIds = await patientAttributeService.getFormOfACharity(
        providerId,
        programId
      );
      const requiredFormIds = formIds.map(form => {
        return form.formId;
      });
      const formData = await this.getFormDataByIds(requiredFormIds);
      return formData;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new FormService();
