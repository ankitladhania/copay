import { defineMessages } from "react-intl";

const messages = defineMessages({
  edit: {
    id: "app.patientAttribute.edit",
    description: "edit Button",
    defaultMessage: "Edit"
  },
  save: {
    id: "app.patientAttribute.save",
    description: "save Button",
    defaultMessage: "Save"
  },
  cancel: {
    id: "app.patientAttribute.cancel",
    description: "cancel Button",
    defaultMessage: "Cancel"
  },
  patientAttribute: {
    id: "app.patientAttribute.patientAttribute",
    description: "Patient Attribute text",
    defaultMessage: "Patient Attribute"
  },
  masterForm:{
    id: "app.patientAttribute.masterForm",
    description: "Master Form text",
    defaultMessage: "Master Form"
  }
  
});

export default messages;
