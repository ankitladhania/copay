// import changePassword from "../../../Containers/page/changePassword";

export const getAddFormURL = () => {
  return "/addForm";
};

export const getAllProviderURL = formId => {
  return "/allProvider";
};

export const getUpdateFormById = id => {
  return `/${id}/editForm`;
};
