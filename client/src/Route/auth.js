import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
//import Home from "../Components/Home";
import Dashboard from "../Container/dashBoard";
import AddInsurance from "../Container/AddInsurance";
import FilledFormList from "../Container/FilledForm.js";
import PatientProfile from "../Container/PatientProfile";

export default class Authenticated extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirecting: this.props.authRedirection
    };
  }
  componentDidMount() {
    this.setState((prevState, prevProps) => {
      return {
        redirecting: false
      };
    });
  }

  // componentDidUpdate(prevProps,prevState){
  //   if(prevProps.authenticated !== this.props.authenticated){
  //     if(!prevProps.authenticated){

  //     }
  //   }
  // }

  render() {
    //
    return (
      <BrowserRouter>
        <Switch>
          {this.state.redirecting && this.state.redirecting.length > 0 && (
            <Redirect to={this.state.redirecting} />
          )}

          <Route exact path="/" component={Dashboard} />
          <Route
            exact
            path="/:programId/addInsurance"
            component={AddInsurance}
          />
          <Route exact path="/form/:formId" component={PatientProfile} />

          <Route exact path="/program/:programId" component={FilledFormList} />
        </Switch>
      </BrowserRouter>
    );
  }
}
