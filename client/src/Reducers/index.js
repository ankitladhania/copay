import { combineReducers } from "redux";
import insurance from "../modules/insurance";
import template from "../modules/template";
import user from "../modules/user";
import insuranceForm from "../modules/insuranceForm";
import auth from "../modules/auth";
import formFilled from "../modules/formFilled";
import addProgramToUser from "../modules/addProgramToUser";
import successMsg from "../modules/successMsg";
import program from "../modules/program";
import modal from "../modules/modal";
import attribute from "../modules/patientAttribute";
import filledForm from "../modules/pages/filledForms";

const page = combineReducers({
filledForm
})

export default combineReducers({
  auth,
  successMsg,
  insurance,
  template,
  user,
  program,
  insuranceForm,
  formFilled,
  addProgramToUser,
  modal,
  attribute,
  page
});
