import { defineMessages } from "react-intl";

const messages = defineMessages({
  add: {
    id: "app.header.add",
    description: "Add text for button",
    defaultMessage: "Add"
  },
  addForm: {
    id: "app.header.addForm",
    description: "AddForm text for button",
    defaultMessage: "Add Cases"
  },
  addProgram: {
    id: "app.header.addProgram",
    description: "Add Program text for dropdown",
    defaultMessage: "Add Program"
  },
  addUser: {
    id: "app.header.addUser",
    description: "Add User text for dropdown",
    defaultMessage: "Add User"
  },
  logout: {
    id: "app.header.logout",
    description: "Logout text for dropdown",
    defaultMessage: "Logout"
  },
 
});

export default messages;
