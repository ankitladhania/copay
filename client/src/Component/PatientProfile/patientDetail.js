import React, { Component, Fragment } from "react";
import PlaceHolder from "../../Assets/images/ico_placeholder_userdp.png";


const ImageHost = process.env.REACT_APP_IMAGE_HOST;

class PatientDetail extends Component {
  render() {
    const { filledForm = {} } = this.props;
    const { formId } = this.props.match.params;
    const { data = {} } = filledForm[formId] || {};
    const { NAME = "", PHOTO } = data;
    let photo = PlaceHolder;
    if (PHOTO) {
      photo = `${ImageHost}${PHOTO}`;
    }
    return (
      <Fragment>
        <div className="flex column align-items-center  patient-detail">
          <img alt="" src={photo} className="patient-photo" />
          <div className="mt16">
            <span>{NAME}</span>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default PatientDetail;
