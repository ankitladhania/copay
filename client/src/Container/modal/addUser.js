import { connect } from "react-redux";
import AddUser from "../../Component/Modal/addUser/addUser";
import { close } from "../../modules/modal";
import { addPatientCoordinator } from "../../modules/user";

const mapStateToProps = state => {
  const { modal, program } = state;
  return {
    show: modal.show && modal.modalType === "ADD_USER",
    program
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close()),
    addPatientCoordinator: data => dispatch(addPatientCoordinator(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddUser);
