import { doRequest } from "../../helper/network";
import { Auth } from "../../helper/urls";
import { REQUEST_TYPE, path } from "../../constant";

const SIGNING = "SIGNING";
export const SIGNING_COMPLETED = "SIGNING_COMPLETED";
const SIGNING_COMPLETED_WITH_ERROR = "SIGNING_COMPLETED_WITH_ERROR";

export const SIGNING_UP_COMPLETED = "SIGNING_UP_COMPLETED";
const SIGNING_UP_COMPLETED_WITH_ERROR = "SIGNING_UP_COMPLETED_WITH_ERROR";

const VALIDATING_LINK = "VALIDATING_LINK";
const VALIDATING_LINK_COMPLETED = "VALIDATING_LINK_COMPLETED";
const VALIDATING_LINK_COMPLETED_WITH_ERROR =
  "VALIDATING_LINK_COMPLETED_WITH_ERROR";

const SIGNING_OUT = "SIGNING_OUT";
const SIGNING_OUT_COMPLETED = "SIGNING_OUT_COMPLETED";
// const SIGNING_OUT_COMPLETED_WITH_ERROR = "SIGNING_OUT_COMPLETED_WITH_ERROR";

const GETTING_INITIAL_DATA = "GETTING_INITIAL_DATA";
export const GETTING_INITIAL_DATA_COMPLETED = "GETTING_INITIAL_DATA_COMPLETED";
const GETTING_INITIAL_DATA_COMPLETED_WITH_ERROR =
  "GETTING_INITIAL_DATA_COMPLETED_WITH_ERROR";

const RESET_ERROR = "RESET_ERROR";

const intial_state = {};

const getRedirectLink = user => {
  let redirectTo = path.DASHBOARD.DASHBOARD;

  return redirectTo;
};

export const clearError = () => {
  return { type: RESET_ERROR };
};

export const signIn = (data = {}) => {
  return async dispatch => {
    try {
      dispatch({ type: SIGNING });
      const response = await doRequest({
        method: REQUEST_TYPE.POST,
        data: data,
        url: Auth.getSignInURL()
      });
      if (response.status === false) {
        dispatch({
          type: SIGNING_COMPLETED_WITH_ERROR,
          payload: { error: response.payload.error }
        });
      } else if (response.status === true) {
        const { _id, user } = response.payload.data;
        let authRedirection = getRedirectLink(user[_id]);
        if (authRedirection.length === 0) authRedirection = "/";
        dispatch({
          type: SIGNING_COMPLETED,
          payload: {
            user: response.payload.data.user,
            authenticatedUser: _id,
            authRedirection
          }
        });
      }
      return response;
    } catch (err) {
      throw err;
    }
  };
};

export const signOut = (data = {}) => {
  return async dispatch => {
    try {
      dispatch({ type: SIGNING_OUT });
      let response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Auth.getSignOutURL()
      });

      const { status } = response;
      console.log("status", status);
      if (status === true) {
        dispatch({ type: SIGNING_OUT_COMPLETED });
        window.location.href = "/";
      } //think and right for other cases
      return status;
    } catch (err) {
      throw err;
    }
  };
};

export const validateLink = (data = {}) => {
  return async dispatch => {
    try {
      dispatch({ type: VALIDATING_LINK });
      let response = await doRequest({
        method: REQUEST_TYPE.POST,
        data: data,
        url: Auth.getValidateLinkURL()
      });
      if (response.status === false) {
        dispatch({
          type: VALIDATING_LINK_COMPLETED_WITH_ERROR,
          payload: { error: response.payload.error }
        });
      } else {
        dispatch({
          type: VALIDATING_LINK_COMPLETED,
          payload: {
            email: response.payload.data.email,
            category: response.payload.data.category,
            invitationLink: data.link
          }
        });
      }
    } catch (err) {
      throw err;
    }
  };
};

export const onAppStart = () => {
  return async dispatch => {
    try {
      dispatch({ type: GETTING_INITIAL_DATA });
      const response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Auth.getOnAppStartURl()
      });
      console.log("response====>", response);
      if (response.status === false) {
        dispatch({
          type: GETTING_INITIAL_DATA_COMPLETED_WITH_ERROR,
          payload: {}
        });
      }

      if (response.status === true) {
        const { data } = response.payload;
        dispatch({
          type: GETTING_INITIAL_DATA_COMPLETED,
          payload: {
            authenticatedUser: data._id,
            user: data.user
          }
        });
      }
    } catch (err) {
      console.log("err", err);
    }
  };
};

export default (state = intial_state, action = {}) => {
  const { type, payload } = action;
  //
  switch (type) {
    case GETTING_INITIAL_DATA_COMPLETED: {
      return {
        authenticated: true,
        authenticated_user: payload.authenticatedUser
      };
    }
    case GETTING_INITIAL_DATA_COMPLETED_WITH_ERROR:
      return {
        authenticated: false
      };

    case SIGNING_COMPLETED_WITH_ERROR:
      return {
        authenticated: false,
        error: payload.error
      };

    case SIGNING_COMPLETED:
      return {
        authenticated: true,
        isProfileCompleted: payload.isProfileCompleted,
        isCalendarSynced: payload.isCalendarSynced,
        authenticated_user: payload.authenticatedUser,
        authRedirection: payload.authRedirection
      };

    case SIGNING_OUT_COMPLETED:
      return {
        authenticated: false,
        authRedirection: "/"
      };

    case VALIDATING_LINK_COMPLETED_WITH_ERROR:
      return {
        ...state,
        validLink: false,
        error: payload.error
      };

    case VALIDATING_LINK_COMPLETED:
      return {
        ...state,
        validLink: true,
        email: payload.email,
        category: payload.category,
        invitationLink: payload.invitationLink
      };

    case SIGNING_UP_COMPLETED_WITH_ERROR:
      return {
        authenticated: false,
        error: payload.error
      };

    case SIGNING_UP_COMPLETED:
      return {
        ...state,
        authenticated: true,
        authenticated_user: payload.authenticatedUser,
        authRedirection: payload.authRedirection
      };

    case RESET_ERROR:
      return {
        ...state,
        error: null
      };

    default: {
      //attach redirect logic here when user is not authenticated and try to do task as an authorized user
      return state;
    }
  }
};
