import React, { Component, Fragment } from "react";
import {
  Input,
  Radio,
  Checkbox,
  DatePicker,
  Select,
  Form,
  Upload,
  Icon,
  Button
} from "antd";
import forEachRight from "lodash-es/forEachRight";
import { QUESTION_TYPE } from "../../constant";
import { getUploadDocsURL } from "../../helper/urls/user";
import "./style.less";

const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
const { Item: FormItem } = Form;
const CheckboxGroup = Checkbox.Group;
// const ATTACHMENT = "ATTACHMENT";
const {
  INPUT,
  TEXT,
  CHECKBOX,
  RADIO,
  DROPDOWN,
  ATTACHMENT,
  ATTACHMENT_PHOTO,
  DOC_TYPE,
  DATE,
  CONDITIONAL,
  NATIONALITY,
  DEPENDENT,
  EMAIL
} = QUESTION_TYPE;

const NAME = "NAME";
const DONE = "done";
const OTHER = "Others";
const NATIONALITY_OTHER = "NATIONALITY_OTHER";
const DEPENDENT_MALE = "NO_OF_DEPENDENT_MALE";
const DEPENDENT_FEMALE = "NO_OF_DEPENDENT_FEMALE";
const ERROR = "error";
const ImageHost = process.env.REACT_APP_IMAGE_HOST;

class FormCreation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileList: {},
      docs: [],
      attachments: [],
      selectedDocs: [],
      removeUpload: false,
      allAttachments: {},
      alreadyAppliedToCharity: false,
      nationality: "",
      dependents: "",
      uploadingDocs: false
    };
  }

  deleteAttachment = element => {
    const { handleDeletingAttachments } = this.props;
    handleDeletingAttachments(element);
    let { attachments, selectedDocs } = this.state;

    let index = attachments.indexOf(element);
    attachments.splice(index, 1);

    let index_in_selected_Docs = selectedDocs.indexOf(element);
    selectedDocs.splice(index_in_selected_Docs, 1);

    this.setState(
      {
        attachments: attachments,
        selectedDocs: selectedDocs
      },
      () => {}
    );
  };

  handleAttachmentChange = (info, element) => {
    const { fileList, allAttachments } = this.state;
    const myfileList = {};
    myfileList[element] = info.fileList;
    this.setState({
      fileList: { ...fileList, ...myfileList },
      uploadingDocs: true
    });
    const { file: { status, response = {}, name = "" } = {} } = info;

    const { payload: { data: { files = [] } = {} } = {} } = response;
    if (status === DONE) {
      const data = {};
      data[element] = {};
      data[element].url = files[0];
      data[element].name = name;
      this.setState({
        removeUpload: true,
        allAttachments: { ...allAttachments, ...data },
        uploadingDocs:false
      });
      const { handleSavingAttachments } = this.props;
      handleSavingAttachments(data);
    } else if (status === ERROR) {
      this.props.handleUploadError();
    }
  };

  handleDelete = element => {
    const { allAttachments } = this.state;
    delete allAttachments[element];
    this.setState({ allAttachments: allAttachments });
    const { handleDeletingAttachments } = this.props;
    handleDeletingAttachments(element);
  };

  addMoreAttachment = () => {
    let { attachments } = this.state;
    attachments.push("");
    //status : 0 for new, 1 for old
    this.setState(
      {
        attachments: attachments
      },
      () => {}
    );
  };

  changeAttachmentType = (e, index) => {
    const { attachments } = this.state;
    let attachment = attachments;
    attachment[index] = e;

    this.setState({
      attachments: attachment,
      index: index,
      selectedDocs: [...attachment]
    });
  };

  getAttachment = (index, QuestionId) => {
    const { attachments } = this.state;

    const attachment = attachments[index];
    if (attachment === ATTACHMENT) {
      return null;
    } else {
      return this.getUploadAttachment(
        QuestionId + "." + attachment,
        attachment
      );
    }
  };

  handleAlreadyAppliedCharity = e => {
    const { value } = e.target;
    this.setState({ alreadyAppliedToCharity: value });
  };

  onSelectChange = key => {
    this.setState({ nationality: key });
  };

  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  onDependentChange = e => {
    const { setFieldsValue } = this.props.form;
    if (e.target.value !== "") {
      this.setState({ dependents: e.target.value });
    } else {
      setFieldsValue({ [DEPENDENT_MALE]: undefined });
      setFieldsValue({ [DEPENDENT_FEMALE]: undefined });
    }
  };

  dependentsValidator = FieldId => (rule, value, callback) => {
    let otherField =
      FieldId === DEPENDENT_MALE ? DEPENDENT_FEMALE : DEPENDENT_MALE;

    const { setFieldsValue } = this.props.form;
    const { dependents } = this.state;
    if (value && value.length > 0 && value.match("^[0-9]+$") !== null) {
      const totalDependents = parseInt(dependents);
      const fieldValue = parseInt(value);

      if (fieldValue < 0) {
        callback("Value cannot be less than 0(zero)");
      } else if (fieldValue > totalDependents) {
        callback("Value cannot be greater that Total dependents");
      } else {
        const remaining = totalDependents - fieldValue;

        setFieldsValue({ [otherField]: `${remaining}` });
        callback();
      }
    } else if (!value) {
      callback();
    } else {
      callback("Enter valid number");
    }
  };

  getFieldInputs = question => {
    const { Question = "", Type, FieldId, Option: Options = [] } = question;
    const { userId } = this.props;
    const {
      getFieldDecorator,
      getFieldError,
      isFieldTouched
    } = this.props.form;

    const { nationality, dependents } = this.state;

    const nameError = isFieldTouched(NAME) && getFieldError(NAME);

    switch (Type) {
      case INPUT:
        return (
          <FormItem
            label={Question !== "" && `${Question}`}
            validateStatus={nameError ? "error" : ""}
            help={nameError || ""}
          >
            {getFieldDecorator(FieldId, {
              rules:
                FieldId === NAME
                  ? [
                      {
                        required: true,
                        message: "Enter a Name"
                      }
                    ]
                  : []
            })(<Input className="mt8" />)}
          </FormItem>
        );

      case TEXT:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(<TextArea className="mt8" />)}
          </FormItem>
        );

      case EMAIL:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {
              rules: [
                {
                  type: "email",
                  message: "Enter a valid email"
                }
              ]
            })(<Input className="mt8" />)}
          </FormItem>
        );

      case DATE:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(<DatePicker className="mt8" />)}
          </FormItem>
        );
      case CONDITIONAL:
        const { Condition = {} } = question;
        const { ifValue, Fields } = Condition;
        return (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {})(
                <RadioGroup
                  className="radio-group-tab flex align-items-center mt8"
                  buttonStyle="solid"
                  onChange={this.handleAlreadyAppliedCharity}
                >
                  {Options &&
                    Options.map(singleOption => {
                      const { label, value } = singleOption;
                      return (
                        <RadioButton key={value} value={value}>
                          {label}
                        </RadioButton>
                      );
                    })}
                </RadioGroup>
              )}
            </FormItem>
            {this.state.alreadyAppliedToCharity === ifValue &&
              Fields.map(field => {
                return this.getFieldInputs(field);
              })}
          </Fragment>
        );

      case RADIO:
        const radioOption = (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {
                rules: [
                  {
                    required: false, //mandatory_fields.includes(FieldId),
                    message: "This is a required field"
                  }
                ]
              })(
                <RadioGroup
                  className="radio-group-tab flex align-items-center mt8"
                  buttonStyle="solid"
                >
                  {Options &&
                    Options.map(singleOption => {
                      const { label, value } = singleOption;
                      return (
                        <RadioButton key={value} value={value}>
                          {label}
                        </RadioButton>
                      );
                    })}
                </RadioGroup>
              )}
            </FormItem>
          </Fragment>
        );
        return radioOption;

      case CHECKBOX:
        return (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {
                rules: []
              })(<CheckboxGroup options={Options} className="mt8" />)}
            </FormItem>
          </Fragment>
        );

      case DROPDOWN:
        const options = Options.map(single_option => {
          const { label, value } = single_option;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <Select className="mt8">{options}</Select>
            )}
          </FormItem>
        );

      case NATIONALITY:
        const nationalityoptions = Options.map(single_option => {
          const { label, value } = single_option;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });
        return (
          <Fragment>
            <FormItem label={Question !== "" && `${Question}`}>
              {getFieldDecorator(FieldId, {})(
                <Select className="mt8" onChange={this.onSelectChange}>
                  {nationalityoptions}
                </Select>
              )}
            </FormItem>
            {nationality === OTHER && (
              <FormItem>
                {getFieldDecorator(NATIONALITY_OTHER, {})(
                  <Input placeholder="Please Enter the Nationality" />
                )}
              </FormItem>
            )}
          </Fragment>
        );

      case ATTACHMENT_PHOTO:
        const uploadButton = (
          <div>
            <Icon type={this.state.loading ? "loading" : "plus"} />
            <div className="ant-upload-text">Upload</div>
          </div>
        );
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <Fragment>
                <div className="photo-wrapper">
                  <Upload
                    action={getUploadDocsURL(userId)}
                    accept="image/jpeg, image/jpg ,image/png, image/svg"
                    showUploadList={false}
                    onChange={this.props.handlePhotoChange}
                    name="files"
                    listType="picture-card"
                    multiple={false}
                  >
                    {this.props.imgUrl ? (
                      <img
                        alt=""
                        src={`${ImageHost}${this.props.imgUrl}`}
                        className="userDp"
                      />
                    ) : (
                      uploadButton
                    )}
                  </Upload>
                  <div className="close-icon">
                    {this.props.imgUrl && (
                      <Icon
                        type="close-circle"
                        theme="filled"
                        onClick={this.props.handlePhotoDelete}
                      />
                    )}
                  </div>
                </div>
              </Fragment>
            )}
          </FormItem>
        );
      case ATTACHMENT:
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <Upload
                action={getUploadDocsURL(userId)}
                accept="image/jpeg"
                onChange={this.handleChange}
                name="files"
              >
                <Button className="mt8">
                  <Icon type="upload" /> Click to Upload
                </Button>
              </Upload>
            )}
          </FormItem>
        );

      case DOC_TYPE:
        let attachedDocs = [];
        const {
          selectedDocs,
          attachments = [],
          removeUpload,
          allAttachments
        } = this.state;
        const docOptions = Options.filter(singleoption => {
          const { value } = singleoption;
          if (!selectedDocs.includes(value)) return true;

          return false;
        }).map(singleoption => {
          const { label, value } = singleoption;
          return (
            <Option key={value} value={value}>
              {label}
            </Option>
          );
        });

        forEachRight(attachments, (element, index) => {
          const dispalybuton =
            removeUpload &&
            (allAttachments[element] && allAttachments[element].length > 0);
          attachedDocs.unshift(
            <div className="flex" key={index}>
              <div
                key={index}
                className="mb20 flex align-items-center justify-content-space-between flex-grow-1"
              >
                <Select
                  className={"iqvia-style-select attachment fontsize14"}
                  value={element}
                  onChange={e => {
                    this.changeAttachmentType(e, index);
                  }}
                >
                  {docOptions}
                </Select>
                <div>
                  <div
                    className={`image-name flex align-items-center ${
                      allAttachments[element] ? undefined : "donot-display"
                    }`}
                  >
                    <div>
                      {allAttachments[element] &&
                        this.getfileName(allAttachments[element].name)}
                    </div>
                    <div className="ml8">
                      <Icon
                        type="close"
                        onClick={e => {
                          this.handleDelete(element);
                        }}
                        className="ml5"
                      />
                    </div>
                  </div>
                  <Upload
                    action={getUploadDocsURL(userId)}
                    onChange={info => {
                      this.handleAttachmentChange(info, element);
                    }}
                    disabled={element === ""}
                    name="files"
                    className={`clickable ${allAttachments[element] &&
                      "donot-display"}`}
                  >
                    {!dispalybuton && (
                      <Button
                        className="uploading-docs-btn"
                        disabled={element === ""}
                        type="default"
                      >
                        {this.state.uploadingDocs ? (
                          <Icon type="loading" />
                        ) : (
                          <div>
                            <Icon type="upload" /> Click to Upload{" "}
                          </div>
                        )}
                      </Button>
                    )}
                  </Upload>
                </div>
              </div>
              <div
                className="ml20 deleteIcon clickable flex align-items-center justify-content-center"
                onClick={e => {
                  this.deleteAttachment(element);
                }}
              >
                <Icon type="delete" />
              </div>
            </div>
          );
        });
        return (
          <FormItem label={Question !== "" && `${Question}`}>
            {getFieldDecorator(FieldId, {})(
              <div>
                <div>{attachedDocs}</div>
                <Button
                  className="border-none mt8"
                  onClick={e => {
                    e.preventDefault();
                    this.addMoreAttachment();
                  }}
                  icon="plus"
                >
                  Add
                </Button>
              </div>
            )}
          </FormItem>
        );

      case DEPENDENT:
        const {
          FieldId: dependentsFieldId,
          Question: dependentsQuestion,
          Dependents
        } = question;
        return (
          <Fragment>
            <FormItem label={dependentsQuestion !== "" && dependentsQuestion}>
              {getFieldDecorator(dependentsFieldId, {
                rules: []
              })(<Input type="number" onChange={this.onDependentChange} />)}
            </FormItem>
            {Dependents.map(dependent => {
              const {
                FieldId: dependentFieldId,
                Question: dependentQuestion
              } = dependent;
              return (
                <FormItem label={dependentQuestion} key={dependentQuestion}>
                  {getFieldDecorator(dependentFieldId, {
                    rules: [
                      {
                        validator: this.dependentsValidator(dependentFieldId)
                      }
                    ]
                  })(
                    <Input
                      type="number"
                      disabled={!dependents || dependents === ""}
                    />
                  )}
                </FormItem>
              );
            })}
          </Fragment>
        );

      default:
        break;
    }
  };
  render() {
    // console.log("ImageHost", ImageHost);
    const { questions = [] } = this.props;

    const getField = questions.map((question, index) => (
      <Fragment key={index}>{this.getFieldInputs(question)}</Fragment>
    ));

    return (
      <Fragment>
        <div className="flex column add-form-element">
          <div>
            <div>{getField}</div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default FormCreation;
