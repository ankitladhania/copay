import DashBoard from "../../Component/Dashboard";
import { connect } from "react-redux";
import { makeGetUserById } from "../../modules/user/selector";
import { fetchInsurance } from "../../modules/insurance";
import {
  addPatientCoordinator,
  getActiveUser,
  addProgramIdsToUser,
  getPatientCoordinatorProgram
} from "../../modules/user";
import { fetchTemplate } from "../../modules/template";
import { getPatientCoordinatorFormFilled } from "../../modules/formFilled";
import { addProgram, getProgram } from "../../modules/program";
import { signOut } from "../../modules/auth";
import { open } from "../../modules/modal";

const mapStateToProps = state => {
  const { auth, user, program, formFilled, insurance } = state;
  const getUser = makeGetUserById();
  return {
    user_data: getUser(user, auth.authenticated_user),
    program,
    user,
    filledForm: formFilled,
    insurance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInsurance: () => dispatch(fetchInsurance()),
    getPatientCoordinatorProgram: () =>
      dispatch(getPatientCoordinatorProgram()),
    fetchTemplate: () => dispatch(fetchTemplate()),
    addPatientCoordinator: data => dispatch(addPatientCoordinator(data)),
    addProgram: data => dispatch(addProgram(data)),
    addProgramIdsToUser: data => dispatch(addProgramIdsToUser(data)),
    getPatientCoordinator: () => dispatch(getActiveUser()),
    getProgram: () => dispatch(getProgram()),
    getPatientCoordinatorFormFilled: programId =>
      dispatch(getPatientCoordinatorFormFilled(programId)),
    openAddProgram: () => dispatch(open("ADD_PROGRAM")),
    openAddUser: () => dispatch(open("ADD_USER")),
    openEditUser: id => dispatch(open("EDIT_USER", id)),
    openEditProgram: id => dispatch(open("EDIT_PROGRAM", id)),
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashBoard);
