export const getCharityOptions = charitys => {
  let charityChildred = [];
  charitys.forEach(charity => {
    const { _id: id, name } = charity;
    let obj = {};
    obj.id = id;
    obj.name = name;
    charityChildred.push(obj);
  });

  //
  //
  return charityChildred;
};

export const getPatientOptions = patients => {
  let patientChildred = [];
  patients.forEach(patient => {
    const { _id: id, data: { NAME: name } = {} } = patient;
    let obj = {};
    obj.id = id;
    obj.name = name;
    patientChildred.push(obj);
  });

  //
  //
  return patientChildred;
};
