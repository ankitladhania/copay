import React, { Component, Fragment } from "react";
import { Tabs, Button, Icon } from "antd";
import { injectIntl } from "react-intl";
import AppHeader from "../../Container/Header";
import PatientDetail from "./patientDetail";
import messages from "./message";
import PatientAttribute from "../../Container/PatientAttribute";
import CommonSuccessMsg from "../../Container/CommonSuccessMsg";
import CommonError from "../CommonError";
import MasterForm from "./masterForm";
import { MODE, USER_CATEGORY } from "../../constant";
import "./style.less";

const NoInternet = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="98.447"
    height="93.717"
    viewBox="0 0 98.447 93.717"
  >
    <g
      id="noun_No_cloud_821504"
      data-name="noun_No cloud_821504"
      transform="translate(-6.78 -8.577)"
    >
      <path
        id="Path_2018"
        data-name="Path 2018"
        d="M95.76,77.867a25.633,25.633,0,0,0-12.094-36.76A25.633,25.633,0,0,0,58.138,17.571a23.641,23.641,0,0,0-15.928,5.6l5.325,6.175a15.508,15.508,0,0,1,10.6-3.624A17.477,17.477,0,0,1,75.615,43.2v.676l-.117,3.2,3.088.874a17.477,17.477,0,0,1,10.242,25.75Z"
        transform="translate(5.851 1.484)"
        fill="#b9b9b9"
      />
      <path
        id="Path_2019"
        data-name="Path 2019"
        d="M32.39,87.22H69.3V79.064H32.39A17.477,17.477,0,0,1,14.913,61.587,19.807,19.807,0,0,1,26.378,43.76l-3.251-7.48A27.859,27.859,0,0,0,6.78,61.587,25.633,25.633,0,0,0,32.39,87.22Z"
        transform="translate(0 4.575)"
        fill="#b9b9b9"
      />
      <rect
        id="Rectangle_904"
        data-name="Rectangle 904"
        width="8.156"
        height="124.38"
        transform="translate(9.152 14.344) rotate(-45)"
        fill="#b9b9b9"
      />
    </g>
  </svg>
);

const { TabPane } = Tabs;

const ATTRIBUTE = "Attribute";
const MASTERFORM = "MasterForm";
const ATTACHMENTS = "ATTACHMENTS";
const PHOTO = "PHOTO";

const CONSENT_FORM = "consentForm";

const { READ, EDIT } = MODE;

class PatientProfile extends Component {
  constructor(props) {
    super(props);
    const {
      location: { state: { selectedTab = ATTRIBUTE } = {} } = {}
    } = this.props;
    this.state = {
      mode: READ,
      currentTab: selectedTab,
      attachments: {},
      ConcentForm: {},
      imgurl: "",
      errorMsg: "",
      uploadError: "",
      networkError: "",
      requesting: false
    };
  }
  componentDidMount() {
    const { getFormDatabyId, fetchTemplate, fetchInsurance } = this.props;
    const { formId } = this.props.match.params;
    fetchTemplate().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      }
    });
    fetchInsurance().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      }
    });
    getFormDatabyId(formId).then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      }
    });
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.filledForm !== prevProps.filledForm) {
      const { formId } = this.props.match.params;
      const { filledForm, attachments } = this.props;

      const { data = {} } = filledForm[formId] || {};
      const { ATTACHMENTS = {}, PHOTO } = data;
      this.setState({
        attachments: { ...attachments, ...ATTACHMENTS },
        imgurl: PHOTO
      });
    }
  };

  handleTryAgain = () => {
    const { getFormDatabyId, fetchTemplate, fetchInsurance } = this.props;
    const { formId } = this.props.match.params;
    fetchTemplate().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        } else if (response.status === true) {
          this.setState({ networkError: false });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
    fetchInsurance().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        } else if (response.status === true) {
          this.setState({ networkError: false });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
    getFormDatabyId(formId).then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        } else if (response.status === true) {
          this.setState({ networkError: false });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
  };

  handleTabChange = values => {
    this.setState({ currentTab: values, mode: READ });
  };
  handleEdit = () => {
    this.setState({ mode: EDIT });
  };

  handleClose = () => {
    this.setState({ mode: READ });
  };

  setAttributeRef = ref => {
    this.AttributeformRef = ref;
  };

  setMasterFormRef = ref => {
    this.MasterformRef = ref;
  };

  handleSavingAttachments = attachment => {
    const { attachments } = this.state;
    this.setState({ attachments: { ...attachments, ...attachment } }, () => {});
  };

  handleDeletingAttachments = attachment => {
    const { attachments } = this.state;
    delete attachments[attachment];
    this.setState({ attachments: { ...attachments } }, () => {});
  };

  handleSavingConcentForm = concentForm => {
    this.setState({ ConcentForm: { ...concentForm } }, () => {});
  };

  handleDeletingConcentForm = () => {
    this.setState({ ConcentForm: {} }, () => {});
  };

  handleImageChange = file => {
    this.setState({ imgurl: file });
  };

  handleImageDelete = () => {
    this.setState({ imgurl: undefined });
  };

  handleOnSubmit = () => {
    const { currentTab } = this.state;
    const {
      updatePatientAttribute,
      attribute,
      updateMasterFormData
    } = this.props;
    const { formId } = this.props.match.params;
    const { _id: attributeId = "" } = attribute[formId] || {};
    let form = "";
    if (currentTab === ATTRIBUTE) {
      form = this.AttributeformRef.props.form;
    } else if (currentTab === MASTERFORM) {
      form = this.MasterformRef.props.form;
    }
    form.validateFields((err, values) => {
      if (!err) {
        if (currentTab === ATTRIBUTE) {
          this.setState({ requesting: true });
          const { ConcentForm } = this.state;
          const { consentSigned } = values;
          if (consentSigned === "Yes") {
            values[CONSENT_FORM] = ConcentForm;
          } else {
            values[CONSENT_FORM] = undefined;
          }
          updatePatientAttribute(formId, values, attributeId).then(status => {
            if (status) {
              this.setState({ requesting: false });
              this.handleClose();
            }
          });
        } else {
          const { attachments, imgurl } = this.state;
          values[ATTACHMENTS] = attachments;
          values[PHOTO] = imgurl;
          this.setState({ requesting: true });
          updateMasterFormData(formId, { data: values }).then(status => {
            if (status) {
              this.setState({ requesting: false });

              this.handleClose();
            }
          });
        }
      }
    });
  };

  resetMsg = () => {
    this.setState({ errorMsg: "" });
  };

  handleUploadError = () => {
    this.setState({
      uploadError: true,
      errorMsg: "Something went wrong. Please try again."
    });
  };

  render() {
    const { mode, currentTab } = this.state;
    const {
      intl: { formatMessage },
      user_data
    } = this.props;
    console.log("user_data", user_data);
    const {
      handleDeletingAttachments,
      handleSavingAttachments,
      handleImageChange,
      handleImageDelete,
      hasErrors
    } = this;
    const { category } = user_data;
    const NoInternetIcon = props => <Icon component={NoInternet} {...props} />;
    const { networkError } = this.state;
    if (networkError) {
      return (
        <Fragment>
          <AppHeader />
          <div
            className="flex align-items-center justify-content-center "
            style={{ height: "100vh" }}
          >
            <div className="flex column align-items-center justify-content-center ">
              <div className="no-internet-img-container">
                <NoInternetIcon className="no-internet-img " />
              </div>
              <div className="mt10 fontsize18 medium">No Internet</div>
              <Button
                className="iqvia-btn mt16"
                type="primary"
                onClick={this.handleTryAgain}
              >
                Try Again
              </Button>
            </div>
          </div>
        </Fragment>
      );
    }

    const { requesting, errorMsg, uploadError } = this.state;
    const edit = (
      <Button className="iqvia mb8" type="primary" onClick={this.handleEdit}>
        {formatMessage(messages.edit)}
      </Button>
    );
    const save = (
      <Fragment>
        <Button
          className="iqvia mb8 mr16"
          type="ghost"
          onClick={this.handleClose}
        >
          {formatMessage(messages.cancel)}
        </Button>
        <Button
          className="iqvia mb8"
          type="primary"
          loading={requesting}
          onClick={this.handleOnSubmit}
        >
          {formatMessage(messages.save)}
        </Button>
      </Fragment>
    );

    return (
      <Fragment>
        <AppHeader />
        <div className="Detail">
          <PatientDetail {...this.props} />
        </div>
        <div className="patient-profile flex align-item-center ">
          <div className="flex-grow-1 master-form">
            <CommonSuccessMsg className={"patientProfile-success-msg "} />
            {(uploadError || errorMsg.length > 0) && (
              <CommonError
                msg={errorMsg}
                close={this.resetMsg}
                className="insurance-form-error"
              />
            )}
            <Tabs
              defaultActiveKey={currentTab}
              tabBarExtraContent={
                category === USER_CATEGORY.PATIENT_COORDINATOR
                  ? mode === READ
                    ? edit
                    : save
                  : false
              }
              tabBarStyle={{
                fontFamily: "AvenirNext-Medium",
                color: "#7f888d"
              }}
              className="tabs"
              onChange={this.handleTabChange}
            >
              <TabPane
                tab={formatMessage(messages.patientAttribute)}
                key={ATTRIBUTE}
              >
                <PatientAttribute
                  mode={mode}
                  setAttributeRef={this.setAttributeRef}
                  handleSavingConcentForm={this.handleSavingConcentForm}
                  handleDeletingConcentForm={this.handleDeletingConcentForm}
                  handleUploadError={this.handleUploadError}
                />
              </TabPane>
              <TabPane
                tab={formatMessage(messages.masterForm)}
                key={MASTERFORM}
              >
                <MasterForm
                  mode={mode}
                  {...this.props}
                  setMasterFormRef={this.setMasterFormRef}
                  handleDeletingAttachments={handleDeletingAttachments}
                  handleSavingAttachments={handleSavingAttachments}
                  handleImageChange={handleImageChange}
                  handleImageDelete={handleImageDelete}
                  hasErrors={hasErrors}
                  handleUploadError={this.handleUploadError}
                />
              </TabPane>
            </Tabs>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default injectIntl(PatientProfile);
