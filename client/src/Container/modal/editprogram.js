import { connect } from "react-redux";
import EditProgram from "../../Component/Modal/editProgram/editProgram";
import { close } from "../../modules/modal";
import { editProgram } from "../../modules/program";

const mapStateToProps = state => {
  const { modal, program } = state;
  return {
    show: modal.show && modal.modalType === "EDIT_PROGRAM",
    programId: modal.entityId,
    programs: program
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close()),
    editProgram: (id, data) => dispatch(editProgram(id, data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProgram);
