import { defineMessages } from "react-intl";

export default defineMessages({
  viewTracker: {
    id: "app.modal.viewTracker",
    description: "",
    defaultMessage: "Trackers"
  },
  cancel: {
    id: "app.modal.cancel",
    description: "",
    defaultMessage: "Go Back"
  },
});
