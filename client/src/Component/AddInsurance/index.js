import React, { Component, Fragment } from "react";
import { Icon, Button } from "antd";
import AppHeader from "../../Container/Header";
import InsuranceForm from "../InsuranceForm";
import "./style.less";

const NoInternet = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="98.447"
    height="93.717"
    viewBox="0 0 98.447 93.717"
  >
    <g
      id="noun_No_cloud_821504"
      data-name="noun_No cloud_821504"
      transform="translate(-6.78 -8.577)"
    >
      <path
        id="Path_2018"
        data-name="Path 2018"
        d="M95.76,77.867a25.633,25.633,0,0,0-12.094-36.76A25.633,25.633,0,0,0,58.138,17.571a23.641,23.641,0,0,0-15.928,5.6l5.325,6.175a15.508,15.508,0,0,1,10.6-3.624A17.477,17.477,0,0,1,75.615,43.2v.676l-.117,3.2,3.088.874a17.477,17.477,0,0,1,10.242,25.75Z"
        transform="translate(5.851 1.484)"
        fill="#b9b9b9"
      />
      <path
        id="Path_2019"
        data-name="Path 2019"
        d="M32.39,87.22H69.3V79.064H32.39A17.477,17.477,0,0,1,14.913,61.587,19.807,19.807,0,0,1,26.378,43.76l-3.251-7.48A27.859,27.859,0,0,0,6.78,61.587,25.633,25.633,0,0,0,32.39,87.22Z"
        transform="translate(0 4.575)"
        fill="#b9b9b9"
      />
      <rect
        id="Rectangle_904"
        data-name="Rectangle 904"
        width="8.156"
        height="124.38"
        transform="translate(9.152 14.344) rotate(-45)"
        fill="#b9b9b9"
      />
    </g>
  </svg>
);

class AddInsurance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      providerId: "",
      networkError: false
    };
  }

  componentDidMount() {
    const { fetchInsurance, fetchTemplate } = this.props;
    fetchInsurance().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      }
    });
    fetchTemplate().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      }
    });
  }

  handleTryAgain = () => {
    const { fetchInsurance, getPatientCoordinator } = this.props;
    fetchInsurance().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
    getPatientCoordinator().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
  };

  render() {
    const NoInternetIcon = props => <Icon component={NoInternet} {...props} />;
    const { networkError } = this.state;
    if (networkError) {
      return (
        <Fragment>
          <AppHeader />
          <div
            className="flex align-items-center justify-content-center "
            style={{ height: "100vh" }}
          >
            <div className="flex column align-items-center justify-content-center ">
              <div className="no-internet-img-container">
                <NoInternetIcon className="no-internet-img " />
              </div>
              <div className="mt10 fontsize18 medium">No Internet</div>
              <Button
                className="iqvia-btn mt16"
                type="primary"
                onClick={this.handleTryAgain}
              >
                Try Again
              </Button>
            </div>
          </div>
        </Fragment>
      );
    }

    return (
      <Fragment>
        <AppHeader />
        <div className="container">
          <div className="addinsurance ">
            <InsuranceForm {...this.props} />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default AddInsurance;
