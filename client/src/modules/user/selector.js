import { createSelector } from "reselect";

const getUser = (userData, id) => {
  if (!id) {
    return {};
  }
  return userData[id];
};

export const makeGetUserById = () =>
  createSelector(
    [getUser],
    users => {
      return users;
    }
  );
