import { doRequest } from "../../helper/network";
import { Insurance } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const ADDING_FORMDATA = "ADDING_FORMDATA";
export const ADDING_FORMDATA_COMPLETED = "ADDING_FORMDATA_COMPLETED";
const ADDING_FORMDATA_COMPLETED_WITH_ERROR =
  "ADDING_FORMDATA_COMPLETED_WITH_ERROR";

function setFormData(state, data) {
  const { formData } = data;
  if (formData) {
    const prevformData = { ...state };
    let newState = Object.assign({}, prevformData, formData);
    return { ...newState };
  } else {
    return state;
  }
}

export const UploadDocs = (id, file) => {
  return async dispatch => {
    try {
      const docs = { docs: file };
      dispatch({ type: "REUPLOAD_IDPROOF_REQUEST" });
      const url = ``;
      const response = await doRequest({
        method: "post",
        data: docs,
        url: url
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: "REUPLOAD_IDPROOF_REQUEST_COMPLETED",
          payload: { data: payload.data, userId: id }
        });
      } else if (response.status === false) {
        dispatch({
          type: "REUPLOAD_IDPROOF_REQUEST_COMPLETED_WITH_ERROR",
          payload: payload.error
        });
      }
      return response;
    } catch (error) {}
  };
};

export const addFormData = (data, programId) => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_FORMDATA });
      let response = await doRequest({
        data: { data, programId },
        url: Insurance.getAddFormURL(),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_FORMDATA_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_FORMDATA_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
      //
    }
  };
};

export const updateFormData = (id, data) => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_FORMDATA });
      let response = await doRequest({
        data: data,
        url: Insurance.getUpdateFormById(id),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_FORMDATA_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_FORMDATA_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return status;
    } catch (error) {
      throw error;
    }
  };
};

const intialState = {};

export default (state = intialState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case ADDING_FORMDATA_COMPLETED:
      const { data } = payload;
      return setFormData(state, data);

    default:
      return setFormData(state, payload);
  }
};
