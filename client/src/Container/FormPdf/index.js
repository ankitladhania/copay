import FormPdf from "../../Component/FormPdf";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getFormDatabyId } from "../../modules/formFilled";
import {fetchTemplate} from "../../modules/template"

const mapStateToProps = state => {
  const { template, insuranceForm, insurance } = state;
  return {
    formFilled: insuranceForm,
    template,
    insurance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFormDatabyId: formId => dispatch(getFormDatabyId(formId)),
    fetchTemplate: () => dispatch(fetchTemplate())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FormPdf)
);
