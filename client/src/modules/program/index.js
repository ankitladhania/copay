import { doRequest } from "../../helper/network";
import { Program, User } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_PROGRAM = "FETCHING_PROGRAM";
export const FETCHING_PROGRAM_COMPLETED = "FETCHING_PROGRAM_COMPLETED";
const FETCHING_PROGRAM_COMPLETED_WITH_ERROR =
  "FETCHING_PROGRAM_COMPLETED_WITH_ERROR";

const FETCHING_PROGRAM_DATA = "FETCHING_PROGRAM_DATA";
export const FETCHING_PROGRAM_DATA_COMPLETED =
  "FETCHING_PROGRAM_DATA_COMPLETED";
const FETCHING_PROGRAM_DATA_COMPLETED_WITH_ERROR =
  "FETCHING_PROGRAM_DATA_COMPLETED_WITH_ERROR";

const ADDING_PROGRAM = "ADDING_PROGRAM";
export const ADDING_PROGRAM_COMPLETED = "ADDING_PROGRAM_COMPLETED";
const ADDING_PROGRAM_COMPLETED_WITH_ERROR =
  "ADDING_PROGRAM_COMPLETED_WITH_ERROR";

const ADDING_TRACKER = "ADDING_TRACKER";
export const ADDING_TRACKER_COMPLETED = "ADDING_TRACKER_COMPLETED";
const ADDING_TRACKER_COMPLETED_WITH_ERROR =
  "ADDING_TRACKER_COMPLETED_WITH_ERROR";

const EDITING_PROGRAM = "EDITING_PROGRAM";
export const EDITING_PROGRAM_COMPLETED = "EDITING_PROGRAM_COMPLETED";
const EDITING_PROGRAM_COMPLETED_WITH_ERROR =
  "EDITING_PROGRAM_COMPLETED_WITH_ERROR";

const FETCHING_TRACKER = "FETCHING_TRACKER";
export const FETCHING_TRACKER_COMPLETED = "FETCHING_TRACKER_COMPLETED";
const FETCHING_TRACKER_COMPLETED_WITH_ERROR =
  "FETCHING_TRACKER_COMPLETED_WITH_ERROR";

function setProgramsData(state, data = {}) {
  const { program } = data;

  if (program) {
    // ("=+++++++++++++++++++program data------------", Object.keys(program))
    const programIds = Object.keys(program);
    const prevprogram = { ...state };
    let newState = Object.assign({}, prevprogram);

    programIds.forEach(id => {
      newState = Object.assign(
        newState,
        setIndividualprogram(prevprogram, program, id)
      );
    });
    //
    return newState;
  } else return state;
}

function setIndividualprogram(state, programs, id) {
  const program = Object.assign({}, state[id], programs[id]);
  return { [id]: { ...program } }; //addded quick fix need to remove
}

export const getProgram = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_PROGRAM });
      let response = await doRequest({
        url: Program.getProgramURL(),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_PROGRAM_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_PROGRAM_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const getProgramForUser = userId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_PROGRAM });
      let response = await doRequest({
        url: User.getUserProgramURL(),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_PROGRAM_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_PROGRAM_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const getProgramDataById = programId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_PROGRAM_DATA });
      let response = await doRequest({
        url: Program.getProgramDataByIdURL(programId),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_PROGRAM_DATA_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_PROGRAM_DATA_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const addProgram = data => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_PROGRAM });
      let response = await doRequest({
        data: data,
        url: Program.getAddProgramURL(),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_PROGRAM_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_PROGRAM_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const addTracker = (programId, data) => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_TRACKER });
      let response = await doRequest({
        data: { data },
        url: Program.getAddTrackerURL(programId),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_TRACKER_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_TRACKER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const editProgram = (programId, data) => {
  return async dispatch => {
    try {
      dispatch({ type: EDITING_PROGRAM });
      let response = await doRequest({
        data: data,
        url: Program.getEditProgramByIdURL(programId),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: EDITING_PROGRAM_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: EDITING_PROGRAM_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const fetchProgramTracker = programId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_TRACKER });
      let response = await doRequest({
        url: Program.getProgramTrackerURL(programId),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_TRACKER_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_TRACKER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};
const initialState = {};

export default (state = initialState, action) => {
  const { type, payload = {} } = action;
  const { data } = payload;

  switch (type) {
    case ADDING_PROGRAM_COMPLETED:
      return setProgramsData(state, data);

    case EDITING_PROGRAM_COMPLETED:
      return setProgramsData(state, data);

    case FETCHING_PROGRAM_DATA_COMPLETED:
      return setProgramsData(state, data);

    default:
      return setProgramsData(state, payload);
  }
};
