import React, { Component, Fragment, lazy, Suspense } from "react";
import isEmpty from "lodash-es/isEmpty";
import { Icon } from "antd";
import Menubar from "./menubar";
import { Element, scroller } from "react-scroll";

import "./style.less";

const CharitySection = lazy(() => import("./charitySection"));
const CreateResponse = lazy(() => import("./createResponse"));

class FormPdf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prepareDoc: false,
      active: "Personal Info"
    };
  }

  componentDidMount() {
    // const { getFormDatabyId, fetchTemplate } = this.props;
    // const { formId } = this.props.match.params;
    // getFormDatabyId(formId);
    // fetchTemplate();
    if (!isEmpty(this.props.template) && !isEmpty(this.props.formFilled)) {
      this.setState({
        prepareDoc: true
      });
    }
    window.scrollTo(0, 0);
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      !isEmpty(this.props.template) &&
      !isEmpty(this.props.formFilled) &&
      (this.props.template !== prevProps.template ||
        this.props.formFilled !== prevProps.formFilled)
    ) {
      this.setState({
        prepareDoc: true
      });
    }
  }

  scrollToElement = name => {
    scroller.scrollTo(name, {
      duration: 1000,
      delay: 0,
      smooth: true,
      offset: -180
    });
  };

  setActive = val => {
    this.setState({ active: val });
  };

  handleOnClick = e => {
    e.preventDefault();
    this.props.history.push("/");
  };

  handleEdit = e => {
    e.preventDefault();
    const { formFilled: formData } = this.props;
    const formId = Object.keys(formData);
    this.props.history.push(`/edit/${formId}`);
  };

  render() {
    const { template } = this.props;
    const templateId = Object.keys(template);
    const { formTemplate = [] } = template[templateId] || {};

    const formResponse = formTemplate.map((template, index) => {
      const { component, name, showHeader } = template;
      if (name === "Term" || name === "Endorsement") {
        const { formFilled: formData } = this.props;
        const { formId } = this.props.match.params;
        const { data = {} } = formData[formId] || {};

        if (data["TERM"] && data["TERM"].length > 0) {
          return (
            <Fragment key={index}>
              <div className="flex align-items-center mb16">
                <div className="fontsize16 dark ">{`${name}s`}:</div>
                <div className="fontsize14 label-color ml30 ">Accepted</div>
              </div>
            </Fragment>
          );
        } else if (data["ENDORSEMENT"] && data["ENDORSEMENT"].length > 0) {
          return (
            <Fragment key={index}>
              <div className="flex align-items-center mb16">
                <div className="fontsize16 dark ">{name}:</div>
                <div className="fontsize14 label-color ml30 ">Accepted</div>
              </div>
            </Fragment>
          );
        } else {
          return null;
        }
      } else if (showHeader) {
        return (
          <Fragment key={index}>
            <Element name={name}>
              <div className="fontsize22 section dark bold mt32">{name}</div>
              <Suspense
                fallback={
                  <div>
                    <Icon type="loading" />
                  </div>
                }
              >
                <CreateResponse questions={component} {...this.props} />
              </Suspense>
            </Element>
          </Fragment>
        );
      } else {
        return (
          <Fragment key={index}>
            <Element name={name}>
              <div className="fontsize22 section dark bold mt32">{name}</div>
              <Suspense
                fallback={
                  <div>
                    <Icon type="loading" />
                  </div>
                }
              >
                <CreateResponse questions={component} {...this.props} />
              </Suspense>
            </Element>
          </Fragment>
        );
      }
    });
    if (formResponse.length <= 0) {
      return (
        <Fragment>
          <div className="response">
            <div className="form-response">
              <Icon type="loading" />
            </div>
          </div>
        </Fragment>
      );
    }
    return (
      <Fragment>
        <div className="response">
          <Suspense
            fallback={
              <div className="mt40 mb16 form-response flex column">
                <Icon type="loading" />
              </div>
            }
          >
            <div className="menubar mt10">
              <Menubar
                scrollToElement={this.scrollToElement}
                setActive={this.setActive}
                active={this.state.active}
                template={this.props.template}
              />
            </div>

            <div className="mt32 mb16 form-response flex column">
              {formResponse}
              <div className="mt32">
                <Element name="Charity Section">
                  <div className="fontsize22 section dark bold mt32 mb32">
                    Charity Section{" "}
                  </div>
                  <CharitySection
                    {...this.props}
                    prepareDoc={this.state.prepareDoc}
                  />
                </Element>
              </div>
            </div>
          </Suspense>
        </div>
      </Fragment>
    );
  }
}

export default FormPdf;
