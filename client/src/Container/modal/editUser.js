import { connect } from "react-redux";
import EditUser from "../../Component/Modal/editUser/editUser";
import { close } from "../../modules/modal";
import {
  addPatientCoordinator,
  editPatientCoordinator,
  deleteUser
} from "../../modules/user";
import { makeGetUserById } from "../../modules/user/selector";

const mapStateToProps = state => {
  const { modal, program, user } = state;
  const getUser = makeGetUserById();

  return {
    show: modal.show && modal.modalType === "EDIT_USER",
    program,
    userId: modal.entityId,
    users: getUser(user, modal.entityId)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    close: () => dispatch(close()),
    addPatientCoordinator: data => dispatch(addPatientCoordinator(data)),
    editPatientCoordinator: (id, data) =>
      dispatch(editPatientCoordinator(id, data)),
    deleteUser: userId => dispatch(deleteUser(userId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUser);
