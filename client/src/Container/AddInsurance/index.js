import AddInsurance from "../../Component/AddInsurance";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { fetchInsurance } from "../../modules/insurance";
import { fetchTemplate } from "../../modules/template";
import { addFormData, UploadDocs } from "../../modules/insuranceForm";
const mapStateToProps = state => {
  const { insurance, template, auth } = state;
  return {
    userId: auth.authenticated_user,
    insurance,
    template
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInsurance: () => dispatch(fetchInsurance()),
    fetchTemplate: () => dispatch(fetchTemplate()),
    addFormData: (data, programId) => dispatch(addFormData(data, programId)),
    UploadDocs: (id, doc) => dispatch(UploadDocs(id, doc))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddInsurance)
);
