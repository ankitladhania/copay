import React, { Component } from "react";
import { Modal, Button } from "antd";
import { injectIntl } from "react-intl";
import messages from "../message";
import alertIcon from "../../../Assets/images/round-warning-24-px.svg";
import "../style.less";

class DeleteUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requesting: false
    };
  }

  footer = () => {
    const { handleCancel, handleDelete } = this.props;
    const { requesting } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="flex align-items-center justify-content-end h72px mr24">
        <Button
          className="iqvia-btn delete"
          type="primary"
          loading={requesting}
          onClick={handleDelete}
        >
          {formatMessage(messages.yes)}
        </Button>
        <Button className="iqvia-btn cancel mr8" onClick={handleCancel}>
          {formatMessage(messages.no)}
        </Button>
      </div>
    );
  };

  render() {
    const {
      visible,
      isError = {},
      handleCancel,
      intl: { formatMessage }
    } = this.props;

    if (visible === false) {
      return null;
    }

    const { footer } = this;

    const modalProps = {
      visible: visible || isError,
      title: "Confirm Delete",
      okButtonProps: {},
      onCancel: handleCancel,
      destroyOnClose: true,
      bodyStyle: { height: "100%" },
      width: "480px",
      footer: footer()
    };
    return (
      <Modal {...modalProps} className="overflow-auto">
        <div className="fontsize16 dark medium">
          {formatMessage(messages.messageUser)}
        </div>
        <div className="flex align-items-start mt16">
          <div className="mr8">
            <img alt="alert" src={alertIcon} />
          </div>
          <div className="fontsize16 dark">
            {formatMessage(messages.contentUser)}
          </div>
        </div>
      </Modal>
    );
  }
}

export default injectIntl(DeleteUser);
