import React, { Component, Fragment } from "react";
import Routes from "./Container/Routes";
import AddProgram from "./Container/modal/addProgram";
import AddUser from "./Container/modal/addUser";
import EditUser from "./Container/modal/editUser";
import EditProgram from "./Container/modal/editprogram";
import ViewAllTracker from "./Container/modal/viewAllTracker";
class AppWrapper extends Component {
  render() {
    return (
      <Fragment>
        <Routes />
        <AddProgram />
        <AddUser />
        <EditUser />
        <EditProgram />
        <ViewAllTracker />
      </Fragment>
    );
  }
}

export default AppWrapper;
