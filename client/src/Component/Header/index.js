import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Button, Dropdown, Menu } from "antd";
import { Link } from "react-router-dom";
import logo from "../../Assets/images/iqvia_logo_white.png";
import UserDpPlaceholder from "../../Assets/images/ico_placeholder_userdp.png";
import { USER_CATEGORY } from "../../constant";
import messages from "./message";
import "./style.less";
import MenuItem from "antd/lib/menu/MenuItem";

class AppHeader extends Component {
  handleAddProgram = () => {
    const { openAddProgram } = this.props;
    openAddProgram();
  };
  handleAddUser = () => {
    const { openAddUser } = this.props;
    openAddUser();
  };
  handleSignOut = () => {
    console.log("this.props====================>", this.props);
    const { signOut } = this.props;
    signOut();
  };
  render() {
    const {
      category,
      intl: { formatMessage },
      networkError,
      authenticated
    } = this.props;
    const menu = (
      <Menu>
        <Menu.Item key="add_program" onClick={this.handleAddProgram}>
          {formatMessage(messages.addProgram)}
        </Menu.Item>
        <Menu.Item key="add_user" onClick={this.handleAddUser}>
          {formatMessage(messages.addUser)}
        </Menu.Item>
      </Menu>
    );

    const logOutMenu = (
      <Menu>
        <MenuItem key="logout" onClick={this.handleSignOut}>
          {formatMessage(messages.logout)}
        </MenuItem>
      </Menu>
    );

    console.log("category=========================>", this.props, category);
    return (
      <div
        className={
          "fixed_header header flex align-items-center justify-content-space-between pl20 pr20"
        }
      >
        <div className={"flex align-items-center"}>
          <Link to={"/"}>
            <div className="logo">
              <img alt="" src={logo} />
            </div>
          </Link>
        </div>
        <div className="flex align-items-center justify-content-space-between">
          {!networkError && category === USER_CATEGORY.PATIENT_COORDINATOR ? (
            <Button
              icon="plus"
              className="addFormButton"
              onClick={this.props.handleClick}
            >
              {formatMessage(messages.addForm)}
            </Button>
          ) : (
            !networkError &&
            category === USER_CATEGORY.SUPER_ADMIN && (
              <Dropdown overlay={menu} trigger={["click"]}>
                <Button icon="plus" className="addButton">
                  <span> {formatMessage(messages.add)}</span>
                </Button>
              </Dropdown>
            )
          )}
          {authenticated && (
            <div>
              <Dropdown overlay={logOutMenu} trigger={["click"]}>
                <div className="userDpholder">
                  <img
                    src={UserDpPlaceholder}
                    alt="userDpholder"
                    className="userDpholder"
                  />
                </div>
              </Dropdown>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default injectIntl(AppHeader);
