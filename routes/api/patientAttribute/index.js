const express = require("express");
const router = express.Router();
const patientAttributeController = require("../../../app/controller/patientAttribute/patientAttribute.controller");

router.get("/patientAttribute", patientAttributeController.getPateintAttribute);
router.get("/patientAttribute/:formId", patientAttributeController.getPateintAttributeByFormId);
router.post("/patientAttribute/:formId/update", patientAttributeController.addPateintAttribute);


module.exports = router;