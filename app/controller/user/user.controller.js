const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator/check");
const Log = require("../../../libs/log")("userController");
const constants = require("../../../config/constant");
const errMessage = require("../../../config/messages.json").errMessages;
const UserService = require("../../services/user/user.service");
const ProgramService = require("../../services/program/program.service");
const FormService = require("../../services/form/form.service");
const PatientAttribute = require("../../services/patientAttribute/patientAttribute.service");
const Response = require("../../helper/responseFormat");
const saveFileIntoUserBucket = require("./userControllerHelper");
const minioService = require("../../services/minio/minio.service");

const SUPER_ADMIN = "superAdmin";
const PATIENT_COORDINATOR = "patientCoordinator";
class UserController {
  constructor() {}

  /**
   * @api {POST} /sign-in Sign-in
   * @apiName signIn
   * @apiGroup Users
   * @apiVersion 1.0.0
   *
   * @apiParam {String} email Email of the user which was invited.
   * @apiParam {String} password Password against the given email.
   *
   * @apiSuccess (200) {json} Status Success status
   *
   * @apiParamExample {json} Request-Example:
   * {
   *  "email": "iamsuper@admin.com",
   *  "password": "password"
   * }
   *
   *
   * @apiSuccessExample {json} Success-Response:
   * {
   *   "status": true,
   *   "statusCode": 200,
   *   "payload": {
   *      "data": {
   *            isCalendarSynced: true,
   *            isProfileCompleted: false
   *      },
   *      "message": "Sign in successful!"
   *    }
   * }
   *
   * @apiErrorExample {json} Invalid-Email-Response:
   * {
   *     "status": false,
   *     "statusCode": 422,
   *     "payload": {
   *         "error": {
   *             "email": {
   *                 "location": "body",
   *                 "param": "email",
   *                 "value": "iamsupemin.com",
   *                 "msg": "email is not valid"
   *            }
   *        }
   *    }
   * }
   *
   * @apiErrorExample {json} Invalid-Password-Length-Response:
   * {
   *     "status": false,
   *     "statusCode": 422,
   *     "payload": {
   *         "error": {
   *             "password": {
   *                 "location": "body
   *                 "param": "password",
   *                 "value": "pas",
   *                 "msg": "Invalid value"
   *            }
   *        }
   *    }
   * }
   *
   * @apiErrorExample {json} Email-Password-Mismatch-Response:
   * {
   *    "status": false,
   *    "statusCode": 401,
   *    "payload": {
   *        "error": {
   *           "status": "UNAUTHORIZED_ACCESS",
   *           "message": "Either Username or Password is not correct."
   *        }
   *    }
   * }
   *
   * @apiErrorExample {json} Server-Error-Response:
   * {
   *    "status": false,
   *    "statusCode": 500,
   *    "payload": {
   *        "error": {
   *           "status": "INTERNAL_SERVER_ERROR",
   *           "message": "Internal Server Error!"
   *        }
   *    }
   * }
   */

  async signIn(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      let response = new Response(false, 422);
      response.setError(
        Object.assign(errors.mapped(), {
          message: "Invalid value"
        })
      );
      return res.status(422).json(response.getResponse());
    }
    try {
      const { email, password } = req.body;
      const user = await UserService.getUser({
        email: email.toLowerCase()
      });
      if (!user) {
        throw new Error(constants.CAN_NOT_AUTHORIZE);
      }
      const { status } = user;
      if (status === "DELETED") {
        throw new Error(constants.ACCOUNT_DELETED);
      }
      const passwordMatch = await bcrypt.compare(password, user.password);
      if (passwordMatch) {
        // create access token
        const expiresIn = process.config.TOKEN_EXPIRE_TIME; // expires in 30 day

        const secret = process.config.TOKEN_SECRET_KEY;
        const accessToken = await jwt.sign(
          {
            userId: user._id
          },
          secret,
          {
            expiresIn
          }
        );
        res.cookie("accessToken", accessToken, {
          expires: new Date(
            Date.now() + process.config.INVITE_EXPIRE_TIME * 86400000
          ),
          httpOnly: true
        });

        let response = new Response(true, 200);
        response.setData({
          user: {
            [user._id]: {
              _id: user._id,
              name: user.name,
              category: user.category,
              programId: user.programId
            }
          },
          _id: user._id
        });
        response.setMessage("Sign in successful!");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.INCORRECT_PASSWORD);
      }
    } catch (err) {
      Log.debug("Error while signing in: ", err);
      Log.debug(err);
      let payload;
      switch (err.message) {
        case constants.CAN_NOT_AUTHORIZE:
          payload = {
            code: 401,
            error: errMessage.CAN_NOT_AUTHORIZE
          };
          break;
        case constants.INCORRECT_PASSWORD:
          payload = {
            code: 401,
            error: errMessage.INCORRECT_PASSWORD
          };
          break;
        case constants.ACCOUNT_DELETED:
          payload = {
            code: 401,
            error: errMessage.ACCOUNT_DELETED
          };
          break;
        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }
      console.log("payload=====================>", payload);
      Log.debug(payload.error);

      let response = new Response(false, payload.code);
      response.setError(payload.error);

      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async signOut(req, res) {
    if (req.cookies.accessToken) {
      res.clearCookie("accessToken");
      let response = new Response(true, 200);
      response.setData({});
      response.setMessage("Signed out successfully!");
      return res.status(response.getStatusCode()).json(response.getResponse());
    } else {
      let payload;

      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async onAppStart(req, res, next) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { userId } = req.userDetails;
        let basicInfo = await UserService.getBasicInfo(req.userDetails.userId, [
          "_id",
          "name",
          "category"
        ]);
        response = new Response(true, 200);
        response.setData({
          _id: userId,
          user: {
            [userId]: basicInfo
          }
        });
        response.setMessage("Basic info");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      Log.debug(err);
      let payload;

      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async getActiveUser(req, res) {
    try {
      const result = await UserService.getActiveUser();
      let data = {};
      result.forEach(value => {
        const { _id } = value;
        data[_id] = value;
      });
      let response = new Response(true, 200);
      response.setData({
        user: data
      });
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async uploadDocs(req, res) {
    let response;
    try {
      const files = await saveFileIntoUserBucket({
        service: minioService,
        file: req.file,
        userId: req.params.id
      });
      response = new Response(true, 200);
      response.setData({
        files: files
      });
      response.setMessage("Id Proof uploaded successfully!");
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (err) {
      console.log("err=>", err);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async getPatientCoordinatorFormFilled(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { userId } = req.userDetails;
        const { programId } = req.params;

        const userData = await UserService.getUserById(userId);
        const { category } = userData[0] || {};
        if (category === PATIENT_COORDINATOR) {
          const result = await UserService.getPatientCoordinatorFormFilledIds(
            userId
          );

          const { formFilled: formFilledId } = result[0];
          const formData = await UserService.getPatientCoordinatorFormData(
            formFilledId,
            programId
          );
          let data = {};
          formData.forEach(value => {
            const { _id } = value;
            data[_id] = value;
          });
          response = new Response(true, 200);
          response.setData({
            formFilled: data,
            providerForms: data
          });
          response.setMessage("Fetched all formfilled");
          return res
            .status(response.getStatusCode())
            .json(response.getResponse());
        } else {
          const formData = await UserService.getFormDataForAllFormInProgram(
            programId
          );
          let data = {};
          formData.forEach(value => {
            const { _id } = value;
            data[_id] = value;
          });
          response = new Response(true, 200);
          response.setData({
            formFilled: data,
            providerForms: data
          });
          response.setMessage("Fetched all formfilled");
          return res
            .status(response.getStatusCode())
            .json(response.getResponse());
        }
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async addUser(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { data } = req.body;
        const { password, email, category, programId = [] } = data;
        const userExist = await UserService.getUser({ email: email });
        if (userExist) {
          throw new Error(constants.EXISTING_USER);
        }
        data.status = "ACTIVE";
        data.formFilled = [];
        const salt = await bcrypt.genSalt(Number(process.config.saltRounds));
        const hash = await bcrypt.hash(password, salt);
        data.password = hash;
        data.email = email.toLowerCase();
        // data.programId = programId;
        const result = await UserService.addPatientCoordinator(data);

        let formatedResult = {};
        const { _id } = result;
        formatedResult[_id] = result;

        response = new Response(true, 200);
        response.setData({
          user: formatedResult
        });
        response.setMessage("patient Coordinator added");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      if (err.code && err.code == 11000) {
        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_USER);
        return res.status(400).json(response.getResponse());
      }
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        case constants.EXISTING_USER:
          payload = {
            code: 400,
            error: errMessage.EXISTING_USER
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async EditPatientCoordinator(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { id } = req.params;
        const { data } = req.body;
        data.category = "PatientCoordinator";
        data.formFilled = [];
        const user = await UserService.getUserById(id);
        const { password, email: userEmail } = user[0] || {};
        const { password: newPassword, email } = data;
        let result;
        if (userEmail !== email) {
          const userExist = await UserService.getUser({ email: email });
          if (userExist) {
            throw new Error(constants.EXISTING_USER);
          }
        }
        if (password === newPassword) {
          result = await UserService.editPatientCoordinator(id, data);
        } else {
          const salt = await bcrypt.genSalt(Number(process.config.saltRounds));
          const hash = await bcrypt.hash(newPassword, salt);
          data.password = hash;

          result = await UserService.editPatientCoordinator(id, data);
        }

        let formatedResult = {};
        const { _id } = result;
        formatedResult[_id] = result;

        response = new Response(true, 200);
        response.setData({
          user: formatedResult
        });
        response.setMessage("Updated Care Coach");
        return res.send(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      if (err.code && err.code == 11000) {
        let response = new Response(false, 400);
        response.setError(errMessage.EXISTING_USER);
        return res.status(400).json(response.getResponse());
      }
      let payload;
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        case constants.EXISTING_USER:
          payload = {
            code: 400,
            error: errMessage.EXISTING_USER
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async getPatientCoordinatorProgram(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { userId } = req.userDetails;
        const result = await UserService.getUser({ _id: userId });
        const { programId, formFilled } = result;
        const programs = await ProgramService.getProgramByIds(programId);
        const forms = {};
        for (let id in programId) {
          const currentId = programId[id];
          const formCount = await FormService.getFormCountforProgram(
            currentId,
            formFilled
          );
          forms[currentId] = formCount;
        }
        let data = {};
        programs.forEach(async value => {
          const { _id } = value;
          value.formCount = forms[_id];
          data[_id] = value;
        });

        response = new Response(true, 200);
        response.setData({
          program: data
        });
        response.setMessage("PatientCoordinator program recieved");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async getUserPrograms(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { userId } = req.params;
        const result = await UserService.getUser({ _id: userId });
        const { programId, formFilled } = result;
        const programs = await ProgramService.getProgramByIds(programId);
        const forms = {};
        for (let id in programId) {
          const currentId = programId[id];
          const formCount = await FormService.getFormCountforProgram(
            currentId,
            formFilled
          );
          forms[currentId] = formCount;
        }
        let data = {};
        programs.forEach(async value => {
          const { _id } = value;
          value.formCount = forms[_id];
          data[_id] = value;
        });

        response = new Response(true, 200);
        response.setData({
          program: data
        });
        response.setMessage("PatientCoordinator program recieved");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async addProgramIds(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { data } = req.body;
        const { programId, userIds } = data;

        const result = await UserService.addProgramId(programId, userIds);

        response = new Response(true, 200);
        response.setData({
          result
        });
        response.setMessage("Care Coach added");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async addFormToPatientCoordinator(req, res) {
    try {
      if (req.userDetails.exists) {
        const { userId: id } = req.userDetails;
        let { data, programId } = req.body;

        const result = await UserService.addForm(id, data, programId);
        const { user, formId, formFilled } = result;
        await PatientAttribute.addFormPateintAtrribute(formId, programId);
        let formatedResult = {};
        const { _id } = user;
        formatedResult[_id] = user;
        let formData = {};
        formData[formId] = formFilled;
        let response = new Response(true, 200);
        response.setData({
          user: formatedResult,
          formData: formData,
          formFilled: formFilled
        });
        response.setMessage("Form Filled Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(500).json(response.getResponse());
    }
  }

  async getFormFilledForUser(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { programId, userId } = req.params;

        const result = await UserService.getPatientCoordinatorFormFilledIds(
          userId
        );

        const { formFilled: formFilledId } = result[0];
        const formData = await UserService.getPatientCoordinatorFormData(
          formFilledId,
          programId
        );
        let data = {};
        formData.forEach(value => {
          const { _id } = value;
          data[_id] = value;
        });
        response = new Response(true, 200);
        response.setData({
          formFilled: data,
          providerForms: data
        });
        response.setMessage("Fetched all formfilled");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }

  async deleteUser(req, res) {
    let response;
    try {
      if (req.userDetails.exists) {
        const { userId } = req.params;

        const result = await UserService.deleteUser(userId);

        let formatedResult = {};
        const { _id } = result;
        formatedResult[_id] = result;
        response = new Response(true, 200);
        response.setData({
          user: formatedResult
        });
        response.setMessage("User Deleted SuccessFully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (err) {
      let payload;
      console.log("err", err);
      switch (err.message) {
        case constants.COOKIES_NOT_SET:
          payload = {
            code: 401,
            error: errMessage.COOKIES_NOT_SET
          };
          break;

        default:
          payload = {
            code: 500,
            error: errMessage.INTERNAL_SERVER_ERROR
          };
          break;
      }

      response = new Response(false, payload.code);
      response.setError(payload.error);
      return res.status(payload.code).json(response.getResponse());
    }
  }
}

module.exports = new UserController();
