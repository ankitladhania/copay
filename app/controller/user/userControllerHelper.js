const md5 = require("js-md5");


 const saveFileIntoUserBucket = async ({ service, file, userId }) => {
    try {
      await service.createBucket();
      let hash = md5.create();
      const originalFileName = file.originalname;
      hash.update(userId);
      hash.hex();
      const bucket = String(hash).substring(0, 4);
      hash.update(userId + new Date().getTime());
      hash.hex();
      hash = String(hash);
      const fileBuffer = file.buffer;
      const filename = hash.substring(4);
      let fileUrl = bucket + "/" + filename + originalFileName;
      await service.saveBufferObject(fileBuffer, fileUrl);
      let files = [fileUrl];
      return files;
    } catch (err) {
      console.log('err', err)
      throw err;
    }
  };

  module.exports = saveFileIntoUserBucket