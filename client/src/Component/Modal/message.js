import { defineMessages } from "react-intl";

const messages = defineMessages({
  email: {
    id: "app.modal.email",
    description: "email text",
    defaultMessage: "Email"
  },
  password: {
    id: "app.modal.password",
    description: "password text",
    defaultMessage: "Password"
  },
  description: {
    id: "app.modal.description",
    description: "description text",
    defaultMessage: "Description"
  },
  submit: {
    id: "app.modal.submit",
    description: "submit button text",
    defaultMessage: "Submit"
  },
  cancel: {
    id: "app.modal.cancel",
    description: "cancel button text",
    defaultMessage: "Cancel"
  },
  delete: {
    id: "app.modal.delete",
    description: "delete button text",
    defaultMessage: "Delete"
  },
  name: {
    id: "app.modal.name",
    description: "name text",
    defaultMessage: "Name"
  },
  type: {
    id: "app.modal.type",
    description: "type text",
    defaultMessage: "User Category"
  },
  programId: {
    id: "app.modal.progrmaiD",
    description: "programId text",
    defaultMessage: "ProgramId"
  },
  validEmail: {
    id: "app.modal.valid-email",
    description: "valid email decorator text",
    defaultMessage: "Please provide a valid email"
  },
  passwordError: {
    id: "app.modal.password-error",
    description: "password not filled error text",
    defaultMessage: "Please enter your password"
  },
  enterEmail: {
    id: "app.modal.enter-email",
    description: "email decorator text",
    defaultMessage: "Please enter your email"
  },
  enterName: {
    id: "app.modal.enter-name",
    description: "name decorator text",
    defaultMessage: "Please enter your name"
  },
  contentUser: {
    id: "cancel.modal.contentUser",
    description: "",
    defaultMessage:
      "Deleting a user is an irreversible action, all the data will lost."
  },
  messageUser: {
    id: "cancel.modal.messageUser",
    description: "",
    defaultMessage: "Are you sure you want to delete the user?"
  },
  yes: {
    id: "app.modal.yes",
    description: "yes button text",
    defaultMessage: "Yes"
  },
  no: {
    id: "app.modal.no",
    description: "no button text",
    defaultMessage: "No"
  }
});

export default messages;
