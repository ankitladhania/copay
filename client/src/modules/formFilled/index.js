import { doRequest } from "../../helper/network";
import { FormFilled } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_USER_FORM_FILLED = "FETCHING_USER_FORM_FILLED";
export const FETCHING_USER_FORM_FILLED_COMPLETED =
  "FETCHING_USER_FORM_FILLED_COMPLETED";
const FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR =
  "FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR";

function setUsersFormData(state, data = {}) {
  const { formFilled } = data;

  if (formFilled) {
    let newState = Object.assign({}, formFilled);

    //
    return newState;
  } else return state;
}

export const getPatientCoordinatorFormFilled = programId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER_FORM_FILLED });
      let response = await doRequest({
        url: FormFilled.getPatientCoordinatorForProgramURL(programId),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;

      if (status === true) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
    } catch (error) {
      throw error;
      //
    }
  };
};

export const getFormDatabyId = formId => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER_FORM_FILLED });
      let response = await doRequest({
        url: FormFilled.getFormByIdURL(formId),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;

      if (status === true) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_FORM_FILLED_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
    } catch (error) {
      throw error;
      //
    }
  };
};

const initialState = {};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCHING_USER_FORM_FILLED_COMPLETED:
      return setUsersFormData(state, payload);

    default:
      return setUsersFormData(state, payload);
  }
};
