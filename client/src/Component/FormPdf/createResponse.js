import React, { Component, Fragment } from "react";
import { Icon } from "antd";
import { QUESTION_TYPE } from "../../constant";
import moment from "moment";
const {
  INPUT,
  TEXT,
  CHECKBOX,
  RADIO,
  DROPDOWN,
  ATTACHMENT,
  ATTACHMENT_PHOTO,
  DOC_TYPE,
  DATE,
  CONDITIONAL,
  EMAIL,
  NATIONALITY,
  DEPENDENT
} = QUESTION_TYPE;

const ImageHost = process.env.REACT_APP_IMAGE_HOST;
const OTHER = "Others";
const NATIONALITY_OTHER = "NATIONALITY_OTHER";

class CreateResponse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileList: [],
      docs: []
    };
  }

  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  getFieldInputs(question) {
    const { formFilled: formData } = this.props;
    const { formId } = this.props.match.params;
    const { data: formFilled = {} } = formData[formId] || {};
    const { QuestionId: qid, Question, Type, FieldId } = question;

    console.log("formFilled[FieldId]", FieldId, formFilled[FieldId]);
    switch (Type) {
      case INPUT:
        return (
          <div className="flex align-items-center mb16" key={qid}>
            <div className="fontsize16 dark min-width-300">{Question}</div>
            <div className="fontsize16 dark ">
              {`:   ${formFilled[FieldId] || ""}`}
            </div>
          </div>
        );

      case DATE:
        return (
          <div className="flex align-items-center mb16" key={qid}>
            <div className="fontsize16 dark min-width-300">{Question}</div>
            <div className="fontsize16 dark ">
              {`:   ${
                formFilled[FieldId]
                  ? moment(formFilled[FieldId]).format("DD-MMM-YYYY")
                  : ""
              }`}
            </div>
          </div>
        );

      case EMAIL:
        return (
          <div className="flex align-items-center mb16" key={qid}>
            <div className="fontsize16 dark min-width-300">{Question}</div>
            <div className="fontsize16 dark ">
              {`:   ${formFilled[FieldId] || ""}`}
            </div>
          </div>
        );
      case TEXT:
        return (
          <div className="flex mb16" key={qid}>
            <div className="fontsize16 dark min-width-300">{Question}</div>
            <div className="fontsize16 dark caseDiscription">
              {`: ${formFilled[FieldId] || ""}`}
            </div>
          </div>
        );

      case RADIO:
        const radioOption = (
          <Fragment key={qid}>
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">{Question}</div>
              <div className="fontsize16 dark ">
                {`:   ${formFilled[FieldId] || ""}`}
              </div>
            </div>
          </Fragment>
        );
        return radioOption;

      case CONDITIONAL:
        const { Condition = {} } = question;
        const { ifValue, Fields } = Condition;

        const condition = (
          <Fragment key={qid}>
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">{Question}</div>
              <div className="fontsize16 dark ">
                {`:   ${formFilled[FieldId] || ""}`}
              </div>
            </div>
            {formFilled[FieldId] === ifValue &&
              Fields.map(field => {
                return this.getFieldInputs(field);
              })}
          </Fragment>
        );
        return condition;

      case NATIONALITY:
        const nationality = (
          <Fragment key={qid}>
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">{Question}</div>
              <div className="fontsize16 dark ">
                {`:   ${
                  formFilled[FieldId]
                    ? formFilled[FieldId] === OTHER
                      ? `${OTHER}(${formFilled[NATIONALITY_OTHER]})`
                      : formFilled[FieldId]
                    : ""
                }`}
              </div>
            </div>
          </Fragment>
        );
        return nationality;

      case DEPENDENT:
        const { Dependents = {} } = question;

        const dependents = (
          <Fragment key={qid}>
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">{Question}</div>
              <div className="fontsize16 dark ">
                {`:   ${formFilled[FieldId] || ""}`}
              </div>
            </div>
            {Dependents.map(dependent => {
              return this.getFieldInputs(dependent);
            })}
          </Fragment>
        );
        return dependents;

      case CHECKBOX:
        let checkbox = [];
        if (formFilled[FieldId]) {
          const checkboxResponse = formFilled[FieldId] || [];

          for (const response in checkboxResponse) {
            checkbox.push(<div>{`${formFilled[FieldId][response]},`} </div>);
          }
        } else {
          checkbox.push(<div key={qid}>{``} </div>);
        }
        return (
          <Fragment key={qid}>
            {" "}
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">
                {Question && `${Question}`}
              </div>
              <div className="fontsize16 dark">{`: ${checkbox}`}</div>
            </div>
          </Fragment>
        );

      case DROPDOWN:
        return (
          <Fragment key={qid}>
            <div className="flex align-items-center mb16">
              <div className="fontsize16 dark min-width-300">{Question}</div>
              <div className="fontsize16 dark ">
                {`:   ${formFilled[FieldId] || ""}`}
              </div>
            </div>
          </Fragment>
        );

      case ATTACHMENT_PHOTO:
        if (formFilled[FieldId]) {
          return (
            <Fragment key={qid}>
              <div className="flex align-items-center mb16">
                <div key={qid} className="fontsize16 dark min-width-300">
                  {Question}
                </div>
                <span>{`:  `}</span>
                <img
                  alt=""
                  src={`${ImageHost}${formFilled[FieldId]}`}
                  className="patient-photo "
                />
              </div>
            </Fragment>
          );
        } else {
          return (
            <Fragment key={qid}>
              <div className="flex align-items-center mb16">
                <div key={qid} className="fontsize16 dark min-width-300">
                  {Question}
                </div>
                <div className="fontsize16 dark ">:</div>
              </div>
            </Fragment>
          );
        }

      case ATTACHMENT:
        if (formFilled[FieldId]) {
          return (
            <Fragment key={qid}>
              <div className="flex align-items-center mb16">
                <div key={qid} className="fontsize16 dark min-width-300">
                  {Question}
                </div>
                :
                <img alt="" src={`${ImageHost}${formFilled[FieldId]}`} />
              </div>
            </Fragment>
          );
        } else {
          return (
            <Fragment key={qid}>
              <div className="flex align-items-center mb16">
                <div key={qid} className="fontsize16 dark min-width-300">
                  {Question}
                </div>
                <div className="fontsize16 dark ">:</div>
              </div>
            </Fragment>
          );
        }

      case DOC_TYPE:
        let attachment = [];
        if (formFilled[FieldId]) {
          const attachments = formFilled[FieldId] || [];

          for (const response in attachments) {
            const { name, url } = attachments[response];
            attachment.push(
              <div className="flex align-item-center mt8" key={response}>
                <div className="fontsize16 dark attachment-filename min-width-350">
                  {response}
                </div>
                <a href={`${ImageHost}${url}`} download>
                  {this.getfileName(name.trim())}
                  <Icon type="download" className="ml8" />
                </a>
                <img alt="" src={url} />
              </div>
            );
          }
        } else {
          attachment.push(
            <div key={"null"} className="fontsize16 dark ">
              {`No Files Attached`}{" "}
            </div>
          );
        }
        return (
          <Fragment key={qid}>
            <div className="mb16">
              <div>{attachment}</div>
            </div>
          </Fragment>
        );

      default:
        break;
    }
  }
  render() {
    const { questions = [] } = this.props;
    const getField = questions.map((question, index) => (
      <Fragment key={index}>{this.getFieldInputs(question)}</Fragment>
    ));
    return (
      <Fragment>
        <div className="flex column mt10 ">
          <div>
            <div>{getField}</div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default CreateResponse;
