import { defineMessages } from "react-intl";

const messages = defineMessages({
  noFormFIlled: {
    id: "app.patientAttribute.noFormFilled",
    description: "No Form Filled text",
    defaultMessage: "No Form Filled"
  },
 
});

export default messages;
