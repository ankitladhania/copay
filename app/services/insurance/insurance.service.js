const insuranceModal = require("../../models/insuranceProvider");

class InsuranceService {
  constructor() {}

  async getAllInsuranceProvider() {
    try {
      const result = await insuranceModal.find({}).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new InsuranceService();
