const templateModal = require("../../models/insuranceTemplate");

class TemplateService {
  constructor() {}

  async getFormTemplate() {
    try {
      const result = await templateModal.find({}).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new TemplateService();
