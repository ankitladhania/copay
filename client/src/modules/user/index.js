import { doRequest } from "../../helper/network";
import { User } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const ADDING_PATIENT_COORDINATOR = "ADDING_PATIENT_COORDINATOR";
export const ADDING_PATIENT_COORDINATOR_COMPLETED =
  "ADDING_PATIENT_COORDINATOR_COMPLETED";
const ADDING_PATIENT_COORDINATOR_COMPLETED_WITH_ERROR =
  "ADDING_PATIENT_COORDINATOR_COMPLETED_WITH_ERROR";
const ADDING_PROGRAMID_TO_USER = "ADDING_PROGRAMID_TO_USER";
export const ADDING_PROGRAMID_TO_USER_COMPLETED =
  "ADDING_PROGRAMID_TO_USER_COMPLETED";
const ADDING_PROGRAMID_TO_USER_COMPLETED_WITH_ERROR =
  "ADDING_PROGRAMID_TO_USER_COMPLETED_WITH_ERROR";

const FETCHING_USER = "FETCHING_USER";
export const FETCHING_USER_COMPLETED = "FETCHING_USER_COMPLETED";
const FETCHING_USER_COMPLETED_WITH_ERROR = "FETCHING_USER_COMPLETED_WITH_ERROR";

const DELETING_USER = "DELETING_USER";
export const DELETING_USER_COMPLETED = "DELETING_USER_COMPLETED";
const DELETING_USER_COMPLETED_WITH_ERROR = "DELETING_USER_COMPLETED_WITH_ERROR";

function setUsersData(state, data = {}) {
  const { user } = data;
  if (user) {
    const userIds = Object.keys(user);
    const prevUser = { ...state };
    let newState = Object.assign({}, prevUser);

    userIds.forEach(id => {
      newState = Object.assign(newState, setIndividualUser(prevUser, user, id));
    });
    return newState;
  } else return state;
}

function setIndividualUser(state, users, id) {
  const user = Object.assign({}, state[id], users[id]);
  return { [id]: { ...user } }; //addded quick fix need to remove
}

export const getActiveUser = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER });
      let response = await doRequest({
        url: User.getActiveUserURL(),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_USER_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const getPatientCoordinatorProgram = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_USER });
      let response = await doRequest({
        url: User.getPatientCoordinatorProgramURL(),
        method: REQUEST_TYPE.GET
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_USER_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_USER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const addPatientCoordinator = data => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_PATIENT_COORDINATOR });
      let response = await doRequest({
        data: data,
        url: User.getAddPatientCoordinatorURL(),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_PATIENT_COORDINATOR_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_PATIENT_COORDINATOR_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const editPatientCoordinator = (id, data) => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_PATIENT_COORDINATOR });
      let response = await doRequest({
        data: data,
        url: User.getEditPatientCoordinatorByIdURL(id),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_PATIENT_COORDINATOR_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_PATIENT_COORDINATOR_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const addProgramIdsToUser = data => {
  return async dispatch => {
    try {
      dispatch({ type: ADDING_PROGRAMID_TO_USER });
      let response = await doRequest({
        data: data,
        url: User.getAddProgramToUserURL(),
        method: REQUEST_TYPE.POST
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: ADDING_PROGRAMID_TO_USER_COMPLETED,
          payload: payload
        });
      } else if (response.status === false) {
        dispatch({
          type: ADDING_PROGRAMID_TO_USER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

export const deleteUser = userId => {
  return async dispatch => {
    try {
      dispatch({ type: DELETING_USER });
      let response = await doRequest({
        url: User.deleteUserURL(userId),
        method: REQUEST_TYPE.DELETE
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: DELETING_USER_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: DELETING_USER_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

const initialState = {};

export default (state = initialState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case FETCHING_USER_COMPLETED:
      return setUsersData(state, payload);
    case ADDING_PATIENT_COORDINATOR_COMPLETED:
      const { data = {} } = payload;
      return setUsersData(state, data);
    default:
      return setUsersData(state, payload);
  }
};
