const Log = require("../../../libs/log")("searchController");
const SearchService = require("../../services/search/search.service");
const Response = require("../../helper/responseFormat");

class SearchController {
  constructor() {}

  async doSearch(req, res) {
    try {
      const { query } = req.query;
      const { programId } = req.params;
      const provider = await SearchService.getAllInsuranceProvider(query);
      const formData = await SearchService.getformData(query, programId);
      let response = new Response(true, 200);
      response.setData({
        insuranceProvider: provider,
        formData: formData
      });
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }
}

module.exports = new SearchController();
