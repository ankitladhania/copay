import { ADDING_PATIENT_COORDINATOR_COMPLETED } from "../user";
import {
  ADDING_PROGRAM_COMPLETED,
  EDITING_PROGRAM_COMPLETED,
  ADDING_TRACKER_COMPLETED
} from "../program";
import { ADDING_FORMDATA_COMPLETED } from "../insuranceForm";
import { ADDING_PATIENT_ATTRIBUTE_COMPLETED } from "../patientAttribute";

const CLEAR_MSG = "CLEAR_MSG";
const SHOW_MSG = "SHOW_MSG";

const initialState = {};

export const clearMsg = () => {
  return dispatch => {
    dispatch({ type: CLEAR_MSG });
  };
};

export const showMsg = msg => {
  return dispatch => {
    dispatch({ type: SHOW_MSG, message: msg });
  };
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case ADDING_PATIENT_ATTRIBUTE_COMPLETED:
    case ADDING_PROGRAM_COMPLETED:
    case ADDING_PATIENT_COORDINATOR_COMPLETED:
    case ADDING_FORMDATA_COMPLETED:
    case EDITING_PROGRAM_COMPLETED:
    case ADDING_TRACKER_COMPLETED:
    case SHOW_MSG:
      return {
        msg: payload.message
      };
    case CLEAR_MSG:
      return {};
    default:
      return state;
  }
};
