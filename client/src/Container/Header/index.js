import AppHeader from "../../Component/Header";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { signOut } from "../../modules/auth";

const mapStateToProps = state => {
  const { auth } = state;
  return {
    authenticated: auth.authenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppHeader)
);
