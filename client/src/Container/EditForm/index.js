import EditForm from "../../Component/EditForm";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { makeGetUserById } from "../../modules/user/selector";
import { fetchInsurance } from "../../modules/insurance";
import { updateFormData } from "../../modules/insuranceForm";
import { fetchTemplate } from "../../modules/template";
import {
  getPatientCoordinatorFormFilled,
  getFormDatabyId
} from "../../modules/formFilled";

const mapStateToProps = state => {
  const {
    auth,
    user,
    program,
    addProgramToUser,
    formFilled,
    insurance,
    template
  } = state;
  const getUser = makeGetUserById();
  return {
    user_data: getUser(user, auth.authenticated_user),
    userId: auth.authenticated_user,
    program,
    users: addProgramToUser,
    filledForm: formFilled,
    insurance,
    template
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInsurance: () => dispatch(fetchInsurance()),
    fetchTemplate: () => dispatch(fetchTemplate()),
    getPatientCoordinatorFormFilled: () => dispatch(getPatientCoordinatorFormFilled()),
    updateFormData: (id, data) => dispatch(updateFormData(id, data)),
    getFormDatabyId: formId => dispatch(getFormDatabyId(formId))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditForm)
);
