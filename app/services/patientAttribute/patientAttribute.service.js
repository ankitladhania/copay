const patientAttributeModal = require("../../models/patientAttribute");

class PateintAtrributeService {
  constructor() {}

  async getFormPateintAtrribute() {
    try {
      const result = await patientAttributeModal.find({}).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getFormPateintAtrributeByFormId(id) {
    try {
      const result = await patientAttributeModal.find({ formId: id }).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getFormOfACharity(providerId, programId) {
    try {
      const query = `data.charity.${providerId}.submission`;
      const result = await patientAttributeModal
        .find(
          { [query]: { $ne: null }, "data.programId": programId },
          { formId: 1 }
        )
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async addFormPateintAtrribute(formId, programId) {
    try {
      const result = await patientAttributeModal.create({
        formId: formId,
        programId: programId
      });
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async updateFormPateintAtrribute(data, attributeId) {
    try {

      const result = await patientAttributeModal
        .findOneAndUpdate(
          { _id: attributeId },
          {
            $set: { data: data }
          },
          {
            new: true
          }
        )
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
}

module.exports = new PateintAtrributeService();
