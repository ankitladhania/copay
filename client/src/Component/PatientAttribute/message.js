import { defineMessages } from "react-intl";

const messages = defineMessages({
  dateOfSubmission: {
    id: "app.patientAttribute.DateofSubmission",
    description: "Date of Submission text",
    defaultMessage: "Date of Submission"
  },
  dateofRejection: {
    id: "app.patientAttribute.DateofRejection",
    description: "Date of Rejection text",
    defaultMessage: "Date of Rejection"
  },
  reasonOfRejection: {
    id: "app.patientAttribute.ReasonOfRejection",
    description: "Reason Of Rejection text",
    defaultMessage: "Reason Of Rejection"
  },
  patientProgramId: {
    id: "app.patientAttribute.PatientProgramId ",
    description: "Patient ProgramId  text",
    defaultMessage: "Patient ProgramId "
  },
  referedBy: {
    id: "app.patientAttribute.ReferedBy",
    description: "Refered By text",
    defaultMessage: "Refered By"
  },
  product: {
    id: "app.patientAttribute.Product",
    description: "Product text",
    defaultMessage: "Product"
  },
  patientType: {
    id: "app.patientAttribute.PatientType ",
    description: "Patient Type  text",
    defaultMessage: "Patient Type"
  },
  emirate: {
    id: "app.patientAttribute.Emirate",
    description: "Emirate text",
    defaultMessage: "Emirate"
  },
  dateofreferalonformId: {
    id: "app.patientAttribute.DateofreferalonformId",
    description: "Date of referal on formId text",
    defaultMessage: "Date of referal on formId"
  },
  dateofreferaltoIQVIA: {
    id: "app.patientAttribute.DateofreferaltoIQVIA ",
    description: "Date of referal to IQVIA  text",
    defaultMessage: "Date of referal to IQVIA"
  },
  consentFormSigned: {
    id: "app.patientAttribute.ConsentFormSigned",
    description: "Consent Form Signed text",
    defaultMessage: "Consent Form Signed"
  },
  reason: {
    id: "app.patientAttribute.Reason",
    description: "Reason text",
    defaultMessage: "Reason"
  },
  concentForm: {
    id: "app.patientAttribute.concentForm",
    description: "concentForm text",
    defaultMessage: "Concent Form"
  },
  dateof1stcontactwithpatient: {
    id: "app.patientAttribute.Dateof1stcontactwithpatient ",
    description: "Date of 1st contact with patient  text",
    defaultMessage: "Date of 1st contact with patient"
  },
  dateofpatientdossierstarted: {
    id: "app.patientAttribute.Dateofpatientdossierstarted",
    description: "Date of patient dossier started text",
    defaultMessage: "Date of patient dossier started"
  },
  genralComments: {
    id: "app.patientAttribute.GenralComments",
    description: "Genral Comments text",
    defaultMessage: "Genral Comments"
  },
  yes: {
    id: "app.patientAttribute.yes",
    description: "Yes option",
    defaultMessage: "Yes"
  },
  no: {
    id: "app.patientAttribute.no",
    description: "No in option",
    defaultMessage: "No"
  },
  charityProvider: {
    id: "app.patientAttribute.charityProvider",
    description: "Charity provider section heading",
    defaultMessage: "Charity Provider"
  },
  add: {
    id: "app.patientAttribute.add",
    description: "add button text",
    defaultMessage: "Add"
  },
  noformSubmitted: {
    id: "app.patientAttribute.noformSubmitted",
    description: "Form Not Submitted to Any Charity text",
    defaultMessage: "Form Not Submitted to Any Charity"
  }
});

export default messages;
