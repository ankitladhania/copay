const Log = require("../../../libs/log")("insuranceController");
const InsuranceService = require("../../services/insurance/insurance.service");
const Response = require("../../helper/responseFormat");
const errMessage = require("../../../config/messages.json").errMessages;

class InsuranceController {
  constructor() {}

  async getAllInsuranceProvider(req, res) {
    try {
      const result = await InsuranceService.getAllInsuranceProvider();
      let data = {};
      result.forEach(value => {
        const { _id } = value;
        data[_id] = value;
      });
      let response = new Response(true, 200);
      response.setData({
        insuranceProvider: data
      });
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }
}

module.exports = new InsuranceController();
