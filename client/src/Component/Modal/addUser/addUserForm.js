import React, { Component, Fragment } from "react";
import { Form, Select, Radio, Input } from "antd";
import messages from "../message";
import { USER_CATEGORY } from "../../../constant";

const { Item: FormItem } = Form;
const NAME = "name";
const EMAIL = "email";
const CATEGORY = "category";
const PASSWORD = "password";
const PROGRAMID = "programId";

const FIELDS = [NAME, EMAIL, PASSWORD];

const { Group: RadioGroup, Button: RadioButton } = Radio;

const {
  PATIENT_COORDINATOR: patientCoordinator,
  SUPER_ADMIN: superAdmin
} = USER_CATEGORY;

class AddUserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userCategory: patientCoordinator
    };
  }
  componentDidMount() {
    this.props.form.validateFields();
  }

  onChangeCategoryRadioGroup = e => {
    e.preventDefault();
    const value = e.target.value;
    this.setState({ userCategory: value });
  };
  render() {
    const {
      intl: { formatMessage },
      form,
      options
    } = this.props;

    const { getFieldDecorator, isFieldTouched, getFieldError } = form;
    let fieldsError = {};
    FIELDS.forEach(value => {
      const error = isFieldTouched(value) && getFieldError(value);
      fieldsError = { ...fieldsError, [value]: error };
    });

    const { onChangeCategoryRadioGroup } = this;
    const { userCategory } = this.state;
    return (
      <Fragment>
        <div className="pl48 pr24">
          <Form>
            <FormItem
              label={formatMessage(messages.name)}
              validateStatus={fieldsError[NAME] ? "error" : ""}
              help={fieldsError[NAME] || ""}
            >
              {getFieldDecorator(NAME, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.enterName)
                  }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem
              label={formatMessage(messages.type)}
              validateStatus={fieldsError[CATEGORY] ? "error" : ""}
              help={fieldsError[CATEGORY] || ""}
              style={{ marginBottom: "16px" }}
            >
              {getFieldDecorator(CATEGORY, {
                initialValue: patientCoordinator
              })(
                <RadioGroup
                  className="flex mb24"
                  buttonStyle="solid"
                  onChange={onChangeCategoryRadioGroup}
                >
                  <RadioButton
                    key={"careCoach"}
                    className="full-width"
                    value={superAdmin}
                  >
                    Super Admin
                  </RadioButton>
                  <RadioButton
                    key={"doctor"}
                    className="full-width"
                    value={patientCoordinator}
                  >
                    Patient Coordinator
                  </RadioButton>
                </RadioGroup>
              )}
            </FormItem>
            <FormItem
              label={formatMessage(messages.email)}
              validateStatus={fieldsError[EMAIL] ? "error" : ""}
              help={fieldsError[EMAIL] || ""}
            >
              {getFieldDecorator(EMAIL, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.enterEmail)
                  },
                  {
                    type: "email",
                    message: formatMessage(messages.validEmail)
                  }
                ]
              })(<Input type="email" />)}
            </FormItem>
            <FormItem
              label={formatMessage(messages.password)}
              validateStatus={fieldsError[PASSWORD] ? "error" : ""}
              help={fieldsError[PASSWORD] || ""}
            >
              {getFieldDecorator(PASSWORD, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.passwordError)
                  }
                ]
              })(<Input type="password" />)}
            </FormItem>
            <FormItem label={formatMessage(messages.programId)}>
              {getFieldDecorator(PROGRAMID, {})(
                <Select mode="multiple" disabled={userCategory === superAdmin}>
                  {options}
                </Select>
              )}
            </FormItem>
          </Form>
        </div>
      </Fragment>
    );
  }
}

export default AddUserForm;
