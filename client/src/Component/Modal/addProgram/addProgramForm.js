import React, { Component, Fragment } from "react";
import { Form, Input } from "antd";
import messages from "../message";

const { Item: FormItem } = Form;
const { TextArea } = Input;
const NAME = "name";
const DESCRIPTION = "description";

const FIELDS = [NAME];

class AddProgramForm extends Component {
  componentDidMount() {
    this.props.form.validateFields();
  }
  render() {
    const {
      intl: { formatMessage },
      form
    } = this.props;

    const { getFieldDecorator, isFieldTouched, getFieldError } = form;
    let fieldsError = {};
    FIELDS.forEach(value => {
      const error = isFieldTouched(value) && getFieldError(value);
      fieldsError = { ...fieldsError, [value]: error };
    });
    return (
      <Fragment>
        <div className="pl48 pr24">
          <Form onSubmit={this.handleSubmit}>
            <FormItem
              label={formatMessage(messages.name)}
              validateStatus={fieldsError[NAME] ? "error" : ""}
              help={fieldsError[NAME] || ""}
            >
              {getFieldDecorator(NAME, {
                rules: [
                  {
                    required: true,
                    message: "This is a required field"
                  }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label={formatMessage(messages.description)}>
              {getFieldDecorator(DESCRIPTION, {
                rules: [
                  {
                    required: false,
                    message: "This is a required field"
                  }
                ]
              })(<TextArea />)}
            </FormItem>
          </Form>
        </div>
      </Fragment>
    );
  }
}

export default AddProgramForm;
