export const HOST = "/api";
//user category

export const USER_CATEGORY = {
  DOCTOR: "doctor",
  PATIENT: "patient",
  PROGRAM_ADMIN: "programAdmin",
  SUPER_ADMIN: "superAdmin",
  PATIENT_COORDINATOR: "patientCoordinator"
};

//request type
export const REQUEST_TYPE = {
  POST: "post",
  GET: "get",
  PUT: "put",
  DELETE: "delete"
};

export const path = {
  CONSENT_FORM: "/consent-from",
  ID_PROOF: "/id-proof",
  EDIT_PROFILE: "/edit-profile",
  CALENDAR_SYNC: "/calendar-sync",
  MY_PROFILE: "/my-profile",
  CHANGE_PASSWORD: "/change-password",
  PROFILE_SETUP: "/profile-setup",
  SIGN_IN: "/sign-in",
  FORGOT_PASSWORD: "/forgot-password",
  IDENTIFY: "/identify/:link",
  SIGN_UP: "/sign-up/:link",
  LANDING_PAGE: "/",
  DASHBOARD: {
    DASHBOARD: "/",
    HOME: "/dashboard",
    ONLY_CALENDAR: "/calendar",
    CALENDAR: "/calendar/:show",
    PROGRAMS: "/programs",
    ONLY_SURVEYS: "/surveys",
    SURVEYS: "/surveys/:show",
    ONLY_MEDICALS: "/medicals",
    MEDICALS: "/medicals/:show"
  },
  SEARCH: "/search",
  PROGRAM_DETAILS: "/program/:id",
  ENTITY: {
    ROOT: "/:entity",
    PROFILE: "/:entity/:id",
    EDIT_PROFILE: "/:entity/:id/edit"
  },
  CREATESURVEYDETAIL: "/create-survey/template/:templateId",
  SURVEYDETAIL: "/survey/:surveyId",
  PATIENT_RESPONSE: "/survey/:surveyId/participant/:participantId/response",
  REMOTE_CONSULTING: "/remoteConsulting/:roomId"
};

export const GLOBAL_MODALS = {
  EVENT_MODAL: "EVENT_MODAL",
  PATIENT_MODAL: "PATIENT_MODAL",
  DOCTOR_MODAL: "DOCTOR_MODAL",
  DISCHARGE_PATIENT: "DISCHARGE_PATIENT",
  HISTORICAL_CLINICAL_READING: "HISTORICAL_CLINICAL_READING",
  CANCEL_APPOINTMENT: "CANCEL_APPOINTMENT",
  CANCEL_REMINDER: "CANCEL_REMINDER",
  MEDICATION: "MEDICATION",
  CLINICALREADING: "CLINICALREADING",
  ADVERSE_EVENTS: "ADVERSE_EVENTS",
  HISTORICAL_VITALS_READING: "HISTORICAL_VITALS_READING",
  HISTORICAL_MEDICATION_DATA: "HISTORICAL_MEDICATION_DATA",
  DOCUMENTS_MODAL: "DOCUMENTS_MODAL",
  CHANGE_PASSWORD: "CHANGE_PASSWORD",
  PRESCRIPTION: "PRESCRIPTION",
  END_SURVEY: "END_SURVEY"
};

//style
export const GRID_GUTTER = { xs: 0, sm: 16, md: 24, lg: 24, xl: 24, xxl: 24 };

export const MODE = {
  READ: "READ",
  EDIT: "EDIT"
};

export const SUPER_ADMIN_TABS = {
  PROGRAM: "Program",
  USER: "User"
};

export const QUESTION_TYPE = {
  INPUT: "INPUT",
  TEXT: "TEXT",
  RADIO: "RADIO",
  CHECKBOX: "CHECKBOX",
  DROPDOWN: "DROPDOWN",
  ATTACHMENT_PHOTO: "ATTACHMENT_PHOTO",
  ATTACHMENT: "ATTACHMENT",
  DOC_TYPE: "DOC_TYPE",
  DATE: "DATE",
  CONDITIONAL: "CONDITIONAL",
  NATIONALITY: "NATIONALITY",
  DEPENDENT: "DEPENDENT",
  EMAIL: "EMAIL"
};

export const PATIENT_ATTRIBUTE_FIELDS = {
  PROGRMAID: "programId",
  REFEREDBY: "referedBy",
  PRODUCT: "Product",
  PATIENTTYPE: "PatientType",
  EMIRATE: "Emirate",
  FORM_REFERAL_DATE: "formReferalDate",
  IQVIA_REFERAL_DATE: "formReferalIqvia",
  CONSENT_SIGNED: "consentSigned",
  CONSENT_FORM: "consentForm",
  CONSENT_REASON: "concentReason",
  PATIENT_CONTACT_DATE: "patientContactDate",
  DOSSIER_PREPARATION_DATE: "dossierPreparationDate",
  GENERAL_COMMENT: "generalComment"
};

export const USER_STATUS = {
  ACTIVE: "ACTIVE",
  DELETED: "DELETED"
};
