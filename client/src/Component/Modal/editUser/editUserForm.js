import React, { Component, Fragment } from "react";
import { Form, Select, Radio, Input } from "antd";
import messages from "../message";
import { USER_CATEGORY } from "../../../constant";

const { Item: FormItem } = Form;
const NAME = "name";
const EMAIL = "email";
const CATEGORY = "category";
const PASSWORD = "password";
const PROGRAMID = "programId";

const FIELDS = [NAME, EMAIL];

const { Group: RadioGroup, Button: RadioButton } = Radio;

const {
  PATIENT_COORDINATOR: patientCoordinator,
  SUPER_ADMIN: superAdmin
} = USER_CATEGORY;

class EditUserForm extends Component {
  componentDidMount() {
    this.props.form.validateFields();
  }
  render() {
    const {
      intl: { formatMessage },
      form,
      users,
      options
    } = this.props;

    const { getFieldDecorator, getFieldError } = form;
    let fieldsError = {};
    FIELDS.forEach(value => {
      const error = getFieldError(value);
      fieldsError = { ...fieldsError, [value]: error };
    });
    console.log("users------------------->", users);
    return (
      <Fragment>
        <div className="pl48 pr24">
          <Form>
            <FormItem
              label={formatMessage(messages.name)}
              validateStatus={fieldsError[NAME] ? "error" : ""}
              help={fieldsError[NAME] || ""}
            >
              {getFieldDecorator(NAME, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.enterName)
                  }
                ],
                initialValue: users.name
              })(<Input />)}
            </FormItem>
            <FormItem
              label={formatMessage(messages.type)}
              validateStatus={fieldsError[CATEGORY] ? "error" : ""}
              help={fieldsError[CATEGORY] || ""}
              style={{ marginBottom: "16px" }}
            >
              {getFieldDecorator(CATEGORY, {
                initialValue: users.category
              })(
                <RadioGroup className="flex mb24" buttonStyle="solid" disabled>
                  <RadioButton
                    key={"careCoach"}
                    className="full-width"
                    value={superAdmin}
                  >
                    Super Admin
                  </RadioButton>
                  <RadioButton
                    key={"doctor"}
                    className="full-width"
                    value={patientCoordinator}
                  >
                    Patient Coordinator
                  </RadioButton>
                </RadioGroup>
              )}
            </FormItem>
            <FormItem
              label={formatMessage(messages.email)}
              validateStatus={fieldsError[EMAIL] ? "error" : ""}
              help={fieldsError[EMAIL] || ""}
            >
              {getFieldDecorator(EMAIL, {
                rules: [
                  {
                    required: true,
                    message: formatMessage(messages.enterEmail)
                  },
                  {
                    type: "email",
                    message: formatMessage(messages.validEmail)
                  }
                ],
                initialValue: users.email
              })(<Input type="email" />)}
            </FormItem>
            <FormItem
              label={formatMessage(messages.password)}
              validateStatus={fieldsError[PASSWORD] ? "error" : ""}
              help={fieldsError[PASSWORD] || ""}
            >
              {getFieldDecorator(PASSWORD, {})(<Input type="password" />)}
            </FormItem>
            <FormItem label={formatMessage(messages.programId)}>
              {getFieldDecorator(PROGRAMID, {
                initialValue: users.programId
              })(
                <Select
                  mode="multiple"
                  disabled={users.category === USER_CATEGORY.SUPER_ADMIN}
                >
                  {options}
                </Select>
              )}
            </FormItem>
          </Form>
        </div>
      </Fragment>
    );
  }
}

export default EditUserForm;
