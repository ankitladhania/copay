const bcrypt = require("bcrypt");
const insuranceProvider = require("../models/insuranceProvider");
const Mongo = require("../../libs/mongo");
var ObjectId = require("mongodb").ObjectID;

(async () => {
  try {
    const mongo = new Mongo("charity-mongodb://charity-mongodb:27017/rpm");

    var conn = (async function() {
      try {
        const connection = await mongo.getConnection();
      } catch (err) {}
    })();

    const salt = await bcrypt.genSalt(Number(process.config.saltRounds));
    const options = [
      {
        _id: ObjectId("111000000000"),
        name: "Al Ihsan Medical Complex",
        required_fields: {
          "Personal Info": ["PHOTO", "NAME", "MARITAL_STATUS", "QUALIFICATION"],
          "Health Details": ["DISEASE_TYPE", "TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE", "TREATING_DOCTOR_NAME"],

          "Residence Proof": [
            "RESIDENCY_NUMBER",
            "SPONSER_NAME",
            "OTHER_CHARITY_APPLIED"
          ],
          "Case Description": ["CASE_DESCRIPTION"],

          Attachments: [
            "Recent Prescription",
            "Recent Bank Statement",
            "Passport copy"
          ]
        },
        logo: "Al Ihsan Medical Complex.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("222000000000"),
        name: "AL Jalilah Foundation",
        required_fields: {
          "Personal Info": ["NAME", "QUALIFICATION"],
          "Health Details": ["DISEASE_TYPE"],
          HealthInsurance: ["TREATING_DOCTOR_NAME"],

          "Residence Proof": ["RESIDENCY_NUMBER"],
          "Case Description": ["CASE_DESCRIPTION"],

          Attachments: [
            "Recent Prescription",
            "Passport copy",
            "Salary Certificate"
          ]
        },
        logo: "AL Jalilah Foundation.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("333000000000"),
        name: "Bait Al Khair",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Bait Al Khair.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("444000000000"),
        name: "Dar Al Ber + ( Al Maktoum Charity)",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Dar Al Ber + ( Al Maktoum Charity).png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("555000000000"),
        name: "Dubai Charity",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Dubai Charity.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("666000000000"),
        name: "FOCP",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "FOCP.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("777000000000"),
        name: "IACAD",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "IACAD.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("888000000000"),
        name: "MBRCH",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "MBRCH.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("999000000000"),
        name: "Ministry of Health (Patients friends) Salem Bin Lahej",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Ministry of Health (Patients friends) Salem Bin Lahej.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("111100000000"),
        name: "Rahma Charity Foundation- AUD",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Rahma Charity Foundation- AUD.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("222200000000"),
        name: "Red Crescent Dubai",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent Dubai.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("333300000000"),
        name: "Red Crescent Fujairah",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent Fujairah.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("444400000000"),
        name: "Red Crescent( Head Office)",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent( Head Office).png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("555500000000"),
        name: "Red Crescent RAK",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent RAK.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("666600000000"),
        name: "Red Crescent Shj",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent Shj.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("777700000000"),
        name: "Red Crescent UQ",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Red Crescent UQ.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("888800000000"),
        name: "Sharjah Charity",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Sharjah Charity.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("999900000000"),
        name: "Sharjah Media Co",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Sharjah Media Co.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("111110000000"),
        name: "Tarahum Charity Foundation",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Tarahum Charity Foundation.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("222220000000"),
        name: "U.A.Q Charity Association",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "U.A.Q Charity Association.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("333330000000"),
        name: "Zakat Fund",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Zakat Fund.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      },
      {
        _id: ObjectId("444440000000"),
        name: "Zayed bin Sultan Charity & Humantarian-AUH",
        required_fields: {
          "Personal Info": ["NAME", "MARITAL_STATUS"],
          "Health Details": ["TREATMENT_TYPE"],
          "Health Insurance": ["HEALTH_INSURANCE"],

          "Residence Proof": ["SPONSER_NAME", "OTHER_CHARITY_APPLIED"],
          Attachments: [
            "Recent Bank Statement",
            "Passport copy",
            "Local Emirates ID"
          ]
        },

        logo: "Zayed bin Sultan Charity & Humantarian-AUH.png",
        mandatory_fields: ["ENDORSEMENT", "TERM"]
      }
    ];
    await insuranceProvider.remove({});
    let status;
    for (let i = 0; i < options.length; i++) {
      status = await insuranceProvider.create(options[i]);
      console.log("status", status);
    }

    mongo.disconnectConnection();
  } catch (err) {
    console.log("err", err);
  }
})();
