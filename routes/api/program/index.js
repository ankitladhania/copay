const express = require("express");
const router = express.Router();
const multer = require("multer");
const programController = require("../../../app/controller/program/program.controller");

const { check, body } = require("express-validator/check");
import Authenticate from "../middleware/auth";
const storage = multer.memoryStorage();
const upload = multer({ dest: "../app/public/", storage: storage });

router.get("/getProgram", Authenticate, programController.getAllProgram);
router.post("/addProgram", Authenticate, programController.addProgram);
router.get(
  "/program/:programId",
  Authenticate,
  programController.getProgramData
);
router.get(
  "/program/:programId/tracker",
  Authenticate,
  programController.getTrackerForProgram
);
router.post(
  "/editProgram/:programId",
  Authenticate,
  programController.editProgram
);
router.post(
  "/addTracker/:programId",
  Authenticate,
  programController.addTrackerInProgram
);
module.exports = router;
