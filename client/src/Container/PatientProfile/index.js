import PatientProfile from "../../Component/PatientProfile";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {signOut} from "../../modules/auth";
import { getFormDatabyId } from "../../modules/formFilled";
import { fetchTemplate } from "../../modules/template";
import { getPatientCoordinatorProgram } from "../../modules/user";
import { updatePatientAttribute } from "../../modules/patientAttribute";
import { makeGetUserById } from "../../modules/user/selector";
import { updateFormData } from "../../modules/insuranceForm";
import { fetchInsurance } from "../../modules/insurance";

const mapStateToProps = state => {
  const {
    template,
    insuranceForm,
    insurance,
    attribute,
    auth,
    user,
    program,
    addProgramToUser
  } = state;

  const getUser = makeGetUserById();
  return {
    template,
    insurance,
    attribute,
    user_data: getUser(user, auth.authenticated_user),
    userId: auth.authenticated_user,
    program,
    users: addProgramToUser,
    filledForm: insuranceForm
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInsurance: () => dispatch(fetchInsurance()),
    getFormDatabyId: formId => dispatch(getFormDatabyId(formId)),
    fetchTemplate: () => dispatch(fetchTemplate()),
    fetchPatientCoordinatorProgram: () => dispatch(getPatientCoordinatorProgram()),
    updatePatientAttribute: (formId, data, attributeId) =>
      dispatch(updatePatientAttribute(formId, data, attributeId)),
    updateMasterFormData: (id, data) => dispatch(updateFormData(id, data)),
    signOut:() => dispatch(signOut())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PatientProfile)
);
