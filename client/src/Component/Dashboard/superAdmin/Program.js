import React, { Component, Fragment } from "react";
import { Form, Table, Button } from "antd";
import moment from "moment";

class AddProgram extends Component {
  componentDidMount() {
    const { getProgram } = this.props;
    getProgram();
  }

  handleEdit = key => {
    const { openEditProgram } = this.props;
    openEditProgram(key._id);
  };

  handleOnClick = key => {
    const { _id: id } = key;
    this.props.history.push(`/program/${id}`);
  };

  render() {
    const columns = [
      {
        title: "Name",
        dataIndex: "name",
        render: (text, record) => {
          return (
            <div
              className="clickable"
              key={record.id}
              onClick={e => this.handleOnClick(record)}
            >
              {record.name}
            </div>
          );
        }
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description"
      },
      {
        title: "Created On",
        dataIndex: "createdOn",
        render: (text, record) => {
          const { createdAt } = record;
          const date = moment(createdAt).format("LL");
          const time = moment(createdAt).format("LT");
          return (
            <div className="clickable" key={record.id}>
              {date},{time}
            </div>
          );
        }
      },
      {
        title: "Edit",
        dataIndex: "edit",
        render: (text, record) => (
          <Button
            className="iqvia"
            type="ghost"
            onClick={e => this.handleEdit(record)}
          >
            Edit
          </Button>
        )
      }
    ];
    const { program = {} } = this.props;
    let data = [];
    const programIds = Object.keys(program);
    programIds.forEach(programId => {
      data.push(program[programId] || {});
    });

    return (
      <Fragment>
        <Table columns={columns} dataSource={data} />
      </Fragment>
    );
  }
}

export default Form.create()(AddProgram);
