import React, { Component, Fragment } from "react";
import PatientCoordinatorHome from "./careCoach";
import SuperAdminHome from "./superAdmin";
import { USER_CATEGORY } from "../../constant";
import "./style.less";

class Dashboard extends Component {
  render() {
    const { user_data = {} } = this.props;
    const { category } = user_data;
    return (
      <Fragment>
        {category === USER_CATEGORY.PATIENT_COORDINATOR ? (
          <PatientCoordinatorHome {...this.props} />
        ) : (
          <SuperAdminHome {...this.props} />
        )}
      </Fragment>
    );
  }
}

export default Dashboard;
