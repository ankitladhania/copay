import React, { Component, Fragment } from "react";
import { Icon } from "antd";
import { injectIntl } from "react-intl";
import moment from "moment";
import isEmpty from "lodash-es/isEmpty";
import messages from "./message";

const CONCENT = "concent";
const ImageHost = process.env.REACT_APP_IMAGE_HOST;
class ReadMode extends Component {
  componentDidMount() {
    const { program } = this.props;
    if (isEmpty(program)) {
      this.props.getProgram();
    }
  }

  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  getCharityElement = () => {
    const {
      formId,
      insurance,
      attribute,
      intl: { formatMessage }
    } = this.props;

    const { data: { charity = {} } = {} } = attribute[formId] || {};
    const insuranceIds = Object.keys(insurance);
    const charityElement = insuranceIds
      .filter(id => {
        if (charity[id]) {
          if (charity[id].submission !== null) {
            return true;
          }
        }
        return false;
      })
      .map(id => {
        const { name } = insurance[id];
        return (
          <div className="flex column align-item-center" key={id}>
            {charity[id] && charity[id].submission !== null && (
              <Fragment>
                <div className="fontsize16 dark medium mt16">{name}</div>
                <div className="flex align-item-center mt10">
                  <div className="flex align-item-center wp50">
                    <div className="dark fontsize16 attribute-value min-width-140 ">
                      {formatMessage(messages.dateOfSubmission)}
                    </div>
                    <div className="fontsize16">
                      {charity[id] &&
                        charity[id].submission !== null &&
                        `: ${moment(charity[id].submission).format(
                          "DD/MM/YYYY"
                        )}`}
                    </div>
                  </div>
                  <div className="flex align-item-center">
                    <div className="dark fontsize16 attribute-value min-width-140 ">
                      {formatMessage(messages.dateofRejection)}
                    </div>
                    <div className="fontsize16">
                      {charity[id] &&
                        charity[id].rejection !== null &&
                        `: ${moment(charity[id].rejection).format(
                          "DD/MM/YYYY"
                        )}`}
                    </div>
                  </div>
                </div>
                {charity[id] && charity[id].reason && (
                  <div className="flex align-item-center mt8">
                    <div className="dark fontsize16 attribute-value min-width-140">
                      {formatMessage(messages.reasonOfRejection)}
                    </div>
                    <div className="fontsize16">
                      {charity[id] && `: ${charity[id].reason}`}
                    </div>
                  </div>
                )}
              </Fragment>
            )}
          </div>
        );
      });
    return charityElement;
  };
  render() {
    const {
      formId,
      attribute,
      formFilled,
      program,
      intl: { formatMessage }
    } = this.props;
    const { data = {} } = attribute[formId] || {};
    const {
      referedBy,
      Product,
      PatientType,
      Emirate,
      formReferalDate,
      formReferalIqvia,
      consentSigned,
      consentForm,
      concentReason,
      patientContactDate,
      dossierPreparationDate,
      generalComment
    } = data;
    const { programId } = formFilled[formId] || {};
    const { name } = program[programId] || {};
    const charity = this.getCharityElement();
    return (
      <Fragment>
        <div className="patient-attribute flex column">
          <div className="flex align-item-center mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.patientProgramId)}
            </div>
            <div className="fontsize16 ml16">{`: ${name ? name : ""}`}</div>
          </div>
          <div className="flex align-item-center mb16">
            <div className="dark fontsize16 attribute-value min-width-230 ">
              {formatMessage(messages.referedBy)}
            </div>
            <div className="fontsize16 ml16">{`: ${
              referedBy ? referedBy : ""
            }`}</div>
          </div>
          <div className="flex align-item-center mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.product)}
            </div>
            <div className="fontsize16 ml16">
              {`: ${Product ? Product : ""}`}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.patientType)}
            </div>
            <div className="fontsize16 ml16">
              {`: ${PatientType ? PatientType : ""}`}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.emirate)}
            </div>
            <div className="fontsize16 ml16">
              {`: ${Emirate ? Emirate : ""}`}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.dateofreferalonformId)}
            </div>
            <div className="fontsize16 ml16">
              {formReferalDate
                ? `: ${moment(formReferalDate).format("DD/MM/YYYY")}`
                : ":"}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.dateofreferaltoIQVIA)}
            </div>
            <div className="fontsize16 ml16">
              {formReferalIqvia
                ? `: ${moment(formReferalIqvia).format("DD/MM/YYYY")}`
                : ":"}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.consentFormSigned)}
            </div>
            <div className="fontsize16 ml16">
              {consentSigned ? `:  ${consentSigned}` : ":"}
            </div>
          </div>
          {consentSigned === "No" && (
            <div className="flex align-item-center  mb16">
              <div className="dark fontsize16 attribute-value min-width-230">
                {formatMessage(messages.reason)}
              </div>
              <div className="fontsize16 ml16">
                {concentReason ? `: ${concentReason}` : ":"}
              </div>
            </div>
          )}
          {consentSigned === "Yes" && consentForm[CONCENT] && (
            <div className="flex align-item-center  mb16">
              <div className="dark fontsize16 attribute-value min-width-230">
                {formatMessage(messages.concentForm)}
              </div>
              <div className="fontsize16 ml16">
                {consentForm ? (
                  <Fragment>
                    <a
                      href={`${ImageHost}${consentForm[CONCENT].url}`}
                      download
                    >
                      :{`${this.getfileName(consentForm[CONCENT].name.trim())}`}
                      <Icon type="download" className="ml8" />
                    </a>
                  </Fragment>
                ) : (
                  ":"
                )}
              </div>
            </div>
          )}
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.dateof1stcontactwithpatient)}
            </div>
            <div className="fontsize16 ml16">
              {patientContactDate
                ? `: ${moment(patientContactDate).format("DD/MM/YYYY")}`
                : ":"}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.dateofpatientdossierstarted)}
            </div>
            <div className="fontsize16 ml16">
              {dossierPreparationDate
                ? `: ${moment(dossierPreparationDate).format("DD/MM/YYYY")}`
                : ":"}
            </div>
          </div>
          <div className="flex align-item-center  mb16">
            <div className="dark fontsize16 attribute-value min-width-230">
              {formatMessage(messages.genralComments)}
            </div>
            <div className="fontsize16 ml16">
              {generalComment ? `: ${generalComment}` : ":"}
            </div>
          </div>
          <div className="fontsize18 dark bold mt16">
            {formatMessage(messages.charityProvider)}
          </div>
          {charity.length > 0 ? (
            charity
          ) : (
            <div className="fontsize14 mt16">
              {formatMessage(messages.noformSubmitted)}
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

export default injectIntl(ReadMode);
