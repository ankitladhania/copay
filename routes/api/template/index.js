const express = require("express");
const router = express.Router();
const templateController = require("../../../app/controller/Template/template.controller");

router.get("/template", templateController.getTemplate);


module.exports = router;