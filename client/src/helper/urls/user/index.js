// import changePassword from "../../../Containers/page/changePassword";

export const getActiveUserURL = () => {
  return "/getActiveUser";
};

export const getPatientCoordinatorProgramURL = () => {
  return "/getPatientCoordinatorProgram";
};

export const getAddPatientCoordinatorURL = () => {
  return "/addUser";
};

export const getEditPatientCoordinatorByIdURL = id => {
  return `/${id}/editPatientCoordinator`;
};

export const getUploadDocsURL = (userId = "") => {
  return `/api/${userId}/uploadDocs`;
};

export const getPatientCoordinatorForProgramURL = programId => {
  return `/${programId}/getPatientCoordinator`;
};

export const getAddProgramToUserURL = () => {
  return "/addProgramToUser";
};

export const getUserProgramURL = userId => {
  return `/patientCoordinator/${userId}`;
};

export const deleteUserURL = userId => {
  return `/delete/${userId}`;
};
