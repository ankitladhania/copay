import FilledFormList from "../../Component/FilledFormList";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { makeGetUserById } from "../../modules/user/selector";
import { fetchInsurance } from "../../modules/insurance";
import { getPatientCoordinatorProgram } from "../../modules/user";
import { fetchTemplate } from "../../modules/template";
import { open } from "../../modules/modal";
import {
  getPatientCoordinatorFormFilled,
  getformFilledforCharity
} from "../../modules/pages/filledForms";
import { addTracker, getProgramDataById } from "../../modules/program";
const mapStateToProps = state => {
  const { auth, user, program, formFilled, insurance, page } = state;
  const { filledForm: providerForms } = page;
  const getUser = makeGetUserById();
  return {
    user_data: getUser(user, auth.authenticated_user),
    program,
    user,
    filledForm: providerForms,
    formFilled,
    insurance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInsurance: () => dispatch(fetchInsurance()),
    getPatientCoordinatorProgram: () =>
      dispatch(getPatientCoordinatorProgram()),
    fetchTemplate: () => dispatch(fetchTemplate()),
    getPatientCoordinatorFormFilled: programId =>
      dispatch(getPatientCoordinatorFormFilled(programId)),
    getformFilledforCharity: (providerId, progrmaId) =>
      dispatch(getformFilledforCharity(providerId, progrmaId)),
    addTracker: (progrmaId, data) => dispatch(addTracker(progrmaId, data)),
    openViewAllTracker: programId =>
      dispatch(open("VIEW_ALL_TRACKER", programId)),
    getProgramDataById: progrmaId => dispatch(getProgramDataById(progrmaId))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FilledFormList)
);
