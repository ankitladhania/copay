const mongoose = require("mongoose");

const insuranceProviderSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true
    },
    required_fields: {type: Object},
    mandatory_fields: [
      {
        type: String
      }
    ],
    logo: {
      type: String
      // required: true
    }
  },
  {
    collection: "insuranceProvider",
    timestamps: true
  }
);

module.exports = mongoose.model("insuranceProvider", insuranceProviderSchema);
