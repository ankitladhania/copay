const Log = require("../../../libs/log")("attributeController");
const AttributeService = require("../../services/patientAttribute/patientAttribute.service");
const Response = require("../../helper/responseFormat");
const errMessage = require("../../../config/messages.json").errMessages;

class PatientAttributeController {
  constructor() {}

  async getPateintAttribute(req, res) {
    try {
      const result = await AttributeService.getFormPateintAtrribute();
      let data = {};
      result.forEach(value => {
        const { formId } = value;
        data[formId] = value;
      });
      let response = new Response(true, 200);
      response.setData({
        attribute: data
      });
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async getPateintAttributeByFormId(req, res) {
    try {
      const { formId } = req.params;
      const result = await AttributeService.getFormPateintAtrributeByFormId(
        formId
      );
      let data = {};
      result.forEach(value => {
        const { formId } = value;
        data[formId] = value;
      });
      let response = new Response(true, 200);
      response.setData({
        attribute: data
      });
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async addPateintAttribute(req, res) {
    try {
      const { formId } = req.params;
      const { data, attributeId } = req.body;
      const result = await AttributeService.updateFormPateintAtrribute(
        data,
        attributeId
      );
      let formData = {};
      formData[formId] = result;
      let response = new Response(true, 200);
      response.setData({
        attribute: formData
      });
      response.setMessage("Patient Attribute Update SuccessFully");
      return res.status(response.getStatusCode()).json(response.getResponse());
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }
}

module.exports = new PatientAttributeController();
