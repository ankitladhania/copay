const express = require("express");
const router = express.Router();
const formController = require("../../../app/controller/forms/forms.controller");

import Authenticate from "../middleware/auth";  

router.get("/:formId", Authenticate, formController.getFormData);
router.get("/getcharityForms/:providerId", Authenticate, formController.getcharityForms)
module.exports = router;
