import React, { Component, Fragment } from "react";
import { Modal, Button, Form, Select } from "antd";
import { injectIntl } from "react-intl";
import messages from "../message";
import AddUserForm from "./addUserForm";
import CommonError from "../../CommonError";
import "../style.less";
const { Option } = Select;

const hasErrors = fieldsError => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
};

let errMessage = "";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProgram: [],
      requesting: false,
      disabledSubmit: true,
      is_error: false
    };
    this.FormWrapper = Form.create({ onFieldsChange: this.onFormFieldChanges })(
      AddUserForm
    );
  }

  getProgramOption = () => {
    const { program: programs } = this.props;
    const programIds = Object.keys(programs);
    return programIds;
  };

  handleProgramSelectChange = selectedProgram => {
    this.setState({ selectedProgram });
  };

  handleCancel = e => {
    if (e) {
      e.preventDefault();
    }
    const { close } = this.props;
    close();
  };

  setFormRef = formRef => {
    this.formRef = formRef;
  };

  handleSubmit = e => {
    e.preventDefault();
    const { addPatientCoordinator, close } = this.props;

    const { formRef = {} } = this;
    const {
      props: {
        form: { validateFields }
      }
    } = formRef;

    validateFields((err, values) => {
      if (!err) {
        this.setState({ requesting: true });
        console.log("values", values);
        addPatientCoordinator({ data: values }).then(response => {
          const { status, payload } = response;
          if (status) {
            this.setState({ requesting: false });
            close();
          } else {
            console.log("payload", payload);
            const { error: { message = "" } = {} } = payload;
            errMessage = message;
            this.setState({ requesting: false, is_error: true });
          }
        });
      }
    });
  };

  clearMsg = e => {
    //e.preventDefault();
    this.setState({
      is_link_send: false,
      is_error: false
    });
  };

  footer = () => {
    const { handleCancel, handleSubmit } = this;
    const { requesting, disabledSubmit } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="flex align-items-center justify-content-end h72px mr24">
        <Button className="iqvia-btn cancel mr8" onClick={handleCancel}>
          {formatMessage(messages.cancel)}
        </Button>
        <Button
          className="iqvia-btn warning"
          type="primary"
          loading={requesting}
          onClick={handleSubmit}
          disabled={disabledSubmit}
        >
          {formatMessage(messages.submit)}
        </Button>
      </div>
    );
  };

  makeOptions = values => {
    const { program } = this.props;
    const options = values.map(programId => {
      const { _id, name } = program[programId];
      return (
        <Option key={`${_id}`} value={_id}>
          {name}
        </Option>
      );
    });
    return options;
  };
  onFormFieldChanges = (props, allvalues) => {
    const {
      form: { getFieldsError, isFieldsTouched }
    } = props;
    const isError = hasErrors(getFieldsError());
    const { disabledSubmit } = this.state;
    if (disabledSubmit !== isError && isFieldsTouched()) {
      this.setState({ disabledSubmit: isError });
    }
  };

  render() {
    const { show: visible, isError = {} } = this.props;

    if (visible === false) {
      return null;
    }

    const programOption = this.getProgramOption();
    const options = this.makeOptions(programOption);

    const { handleCancel, footer, FormWrapper, setFormRef } = this;

    const modalProps = {
      visible: visible || isError,
      title: "Add User",
      okButtonProps: {},
      onCancel: handleCancel,
      wrapClassName: "iqvia_modals full-height",
      destroyOnClose: true,
      bodyStyle: { height: "100%" },
      width: "480px",
      footer: footer()
    };
    return (
      <Modal {...modalProps} className="overflow-auto">
        <Fragment>
          <FormWrapper
            wrappedComponentRef={setFormRef}
            {...this.props}
            options={options}
          />

          <div className="end-padding">
            {this.state.is_error && (
              <CommonError
                msg={errMessage}
                className={"superAdmin-new-user-failure-message"}
                close={this.clearMsg}
              />
            )}
          </div>
        </Fragment>
      </Modal>
    );
  }
}

export default injectIntl(AddUser);
