const Log = require("../../../libs/log")("FormController");
const Response = require("../../helper/responseFormat");
const UserService = require("../../services/user/user.service");
const FormService = require("../../services/form/form.service");
const errMessage = require("../../../config/messages.json").errMessages;

class FormsController {
  constructor() {}

  async updateFormData(req, res) {
    try {
      if (req.userDetails.exists) {
        const { formId } = req.params;
        let { data } = req.body;

        const result = await UserService.UpdateForm(formId, data);
        const formatedData = {};
        formatedData[formId] = result;
        let response = new Response(true, 200);
        response.setData({
          formData: formatedData
        });
        response.setMessage("Form Updated Successfully");
        return res
          .status(response.getStatusCode())
          .json(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async getFormData(req, res) {
    try {
      if (req.userDetails.exists) {
        const { formId } = req.params;

        const result = await FormService.getFormDataById(formId);
        let formatedResult = {};
        const { _id } = result[0];
        formatedResult[_id] = result[0];
        let response = new Response(true, 200);
        response.setData({
          formFilled: formatedResult,
          formData: formatedResult
        });
        return res.send(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }

  async getcharityForms(req, res) {
    try {
      if (req.userDetails.exists) {
        const { providerId } = req.params;
        const { query: programId } = req.query;
        const result = await FormService.getFormForCharoty(
          providerId,
          programId
        );
        let data = {};
        result.forEach(value => {
          const { _id: formId } = value;
          data[formId] = value;
        });
        let response = new Response(true, 200);
        response.setData({
          providerForms: data
        });
        return res.send(response.getResponse());
      } else {
        throw new Error(constants.COOKIES_NOT_SET);
      }
    } catch (error) {
      console.log("error", error);
      let response = new Response(false, 500);
      response.setError(errMessage.INTERNAL_SERVER_ERROR);
      return res.status(response.getStatusCode()).json(response.getResponse());
    }
  }
}

module.exports = new FormsController();
