import React, { Component, Fragment } from "react";
import { Input, AutoComplete, Card, Icon, Button, Upload } from "antd";
import { injectIntl } from "react-intl";
import throttle from "lodash-es/throttle";
import moment from "moment";
import { doRequest } from "../../helper/network";
import { getCharityOptions, getPatientOptions } from "./getSearchOptions";
import { getUploadDocsURL } from "../../helper/urls/user";
import AppHeader from "../../Container/Header";
import userDp from "../../Assets/images/ico_placeholder_userdp.png";
import CommonSuccessMsg from "../../Container/CommonSuccessMsg";
import messages from "./message";
import "./style.less";

const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;

const ImageHost = process.env.REACT_APP_IMAGE_HOST;

const NoInternet = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="98.447"
    height="93.717"
    viewBox="0 0 98.447 93.717"
  >
    <g
      id="noun_No_cloud_821504"
      data-name="noun_No cloud_821504"
      transform="translate(-6.78 -8.577)"
    >
      <path
        id="Path_2018"
        data-name="Path 2018"
        d="M95.76,77.867a25.633,25.633,0,0,0-12.094-36.76A25.633,25.633,0,0,0,58.138,17.571a23.641,23.641,0,0,0-15.928,5.6l5.325,6.175a15.508,15.508,0,0,1,10.6-3.624A17.477,17.477,0,0,1,75.615,43.2v.676l-.117,3.2,3.088.874a17.477,17.477,0,0,1,10.242,25.75Z"
        transform="translate(5.851 1.484)"
        fill="#b9b9b9"
      />
      <path
        id="Path_2019"
        data-name="Path 2019"
        d="M32.39,87.22H69.3V79.064H32.39A17.477,17.477,0,0,1,14.913,61.587,19.807,19.807,0,0,1,26.378,43.76l-3.251-7.48A27.859,27.859,0,0,0,6.78,61.587,25.633,25.633,0,0,0,32.39,87.22Z"
        transform="translate(0 4.575)"
        fill="#b9b9b9"
      />
      <rect
        id="Rectangle_904"
        data-name="Rectangle 904"
        width="8.156"
        height="124.38"
        transform="translate(9.152 14.344) rotate(-45)"
        fill="#b9b9b9"
      />
    </g>
  </svg>
);

class FilledFormList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Charity: [],
      Patient: [],
      dataSource: [],
      query: "",
      isLoading: false,
      networkError: false,
      programId: "",
      requesting: false,
      dataLoading: true
    };
    this.getoptions = throttle(this.getoptions.bind(this), 3000);
  }
  componentDidMount = () => {
    const {
      getPatientCoordinatorFormFilled,
      fetchInsurance,
      getProgramDataById
    } = this.props;
    const { programId } = this.props.match.params;
    this.setState({ programId: programId });
    getProgramDataById(programId);
    getPatientCoordinatorFormFilled(programId).then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        }
        this.setState({ dataLoading: false });
      }
    });
    fetchInsurance();
  };

  handleTryAgain = () => {
    const {
      getPatientCoordinatorFormFilled,
      fetchInsurance,
      getProgramDataById
    } = this.props;
    const { programId } = this.props.match.params;
    getProgramDataById(programId);
    getPatientCoordinatorFormFilled(programId).then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        } else if (response.status === true) {
          this.setState({ networkError: false });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
    fetchInsurance().then(response => {
      if (response) {
        if (response.statusCode === "000") {
          this.setState({ networkError: true });
        } else if (response.status === true) {
          this.setState({ networkError: false });
        }
      } else {
        this.setState({ networkError: false });
      }
    });
  };

  handleOnclick = formId => {
    this.props.history.push(`/form/${formId}`);
  };

  handleClick = e => {
    e.preventDefault();
    const { programId } = this.props.match.params;
    this.props.history.push(`/${programId}/addInsurance`);
  };

  handleCharityClick = providerId => {
    const { getformFilledforCharity } = this.props;
    const { programId } = this.props.match.params;
    getformFilledforCharity(providerId, programId);
  };

  showoptions = e => {
    const { getoptions } = this;
    if (getoptions !== null) {
      this.setState({ query: e });
      this.setState({ isLoading: true });
      getoptions(e);
    }
  };

  async getoptions(e) {
    const value = e.trim();
    const { programId } = this.props.match.params;

    if (!(value === "" || value === "$")) {
      const data = await doRequest({
        params: { query: value },
        url: `/search/${programId}`,
        method: "get"
      });

      let charity = [];
      let patient = [];
      if (data.payload.data !== undefined) {
        const { payload } = data;
        const {
          data: { formData = [], insuranceProvider = [] } = {}
        } = payload;
        charity = getCharityOptions(insuranceProvider);
        patient = getPatientOptions(formData);
      }
      this.setState(
        prevState => ({
          Charity: charity,
          Patient: patient
        }),
        () => {
          this.setState({
            dataSource: [
              {
                title: "Charity",
                children: this.state.Charity
              },
              {
                title: "Patient",
                children: this.state.Patient
              }
            ],
            isLoading: false
          });
        }
      );
    } else {
      this.setState({ isLoading: false });
    }
  }

  getFormList = formIds => {
    const { formFilled } = this.props;

    const formList = formIds.map(formId => {
      const { data = {} } = formFilled[formId] || {};
      const imgUrl = data.PHOTO ? `${ImageHost}${data.PHOTO}` : userDp;
      return (
        <Card
          className="patient clickable"
          onClick={e => this.handleOnclick(formId)}
          key={formId}
        >
          <div className="">
            <div className="flex align-items-center">
              <img alt="" src={imgUrl} className="patientDp mr16" />
              <div className="dark patientInfo">{data.NAME}</div>
            </div>
            <div>
              <ul className="patients ">
                <li>
                  <span className="fontsize12 dark">{data.DISEASE_TYPE}</span>
                </li>
                <li>
                  <span className="fontsize12 label-color">
                    {data.RESIDENCE_AREA}
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </Card>
      );
    });
    return formList;
  };

  handleOnChange = info => {
    const { addTracker, user_data } = this.props;
    const { programId } = this.props.match.params;
    this.setState({ requesting: true });
    // console.log("info=======================>", programId, user_data, info);
    const { file: { name, status, response = {} } = {} } = info;
    const { _id: updatedBy } = user_data;
    if (status === "done") {
      const { payload: { data: { files = [] } = {} } = {} } = response;
      const file = {
        name,
        url: files[0]
      };
      addTracker(programId, { file, updatedBy }).then(response => {
        const { status } = response;
        if (status) {
          this.setState({ requesting: false });
        }
      });
    }
  };

  handleViewAll = e => {
    const { openViewAllTracker } = this.props;
    const { programId } = this.state;
    openViewAllTracker(programId);
  };

  handleCancel = e => {
    this.setState({ visible: false });
  };

  render() {
    const {
      user_data = {},
      filledForm = {},
      formFilled = {},
      intl: { formatMessage },
      program
    } = this.props;
    const { category, _id: userId } = user_data;
    const {
      dataSource,
      query,
      isLoading,
      networkError,
      programId,
      requesting,
      dataLoading
    } = this.state;

    if (dataLoading) {
      console.log("dataLoading", dataLoading);
      return (
        <Fragment>
          <AppHeader />
          <div className="flex align-items-center justify-content-center ">
            <Icon type="loading" className="loading-icon" />
          </div>
        </Fragment>
      );
    }

    const { tracker = [] } = program[programId] || {};
    const { updateAt } = tracker[tracker.length - 1] || {};

    const date = moment(updateAt).format("LL");
    const time = moment(updateAt).format("LT");

    const { handleViewAll } = this;

    const NoInternetIcon = props => <Icon component={NoInternet} {...props} />;

    if (networkError) {
      return (
        <Fragment>
          <AppHeader
            handleClick={this.handleClick}
            category={category}
            networkError={networkError}
          />
          <div
            className="flex align-items-center justify-content-center "
            style={{ height: "100vh" }}
          >
            <div className="flex column align-items-center justify-content-center ">
              <div className="no-internet-img-container">
                <NoInternetIcon className="no-internet-img " />
              </div>
              <div className="mt10 fontsize18 medium">No Internet</div>
              <Button
                className="iqvia-btn mt16"
                type="primary"
                onClick={this.handleTryAgain}
              >
                Try Again
              </Button>
            </div>
          </div>
        </Fragment>
      );
    }

    let filledFormIds = [];
    if (query === "" && Object.keys(filledForm) < Object.keys(formFilled)) {
      filledFormIds = Object.keys(formFilled);
    } else {
      filledFormIds = Object.keys(filledForm);
    }
    const formList = this.getFormList(filledFormIds);

    // console.log("dataSource", JSON.stringify(dataSource), query);

    const validGroups = dataSource.filter(group => {
      return group.children.length > 0;
    });

    let searchresultAvialable = true;
    if (query !== "" && validGroups.length <= 0) {
      searchresultAvialable = false;
    }
    // console.log('validGroups', validGroups)
    const options = searchresultAvialable
      ? validGroups.map((group, index) => {
          return (
            <OptGroup
              className="title"
              key={index}
              label={
                <div
                  className="flex justify-content-space-between align-items-center"
                  style={{
                    height: "40px",
                    marginTop: "16px",
                    paddingBottom: "16px"
                  }}
                >
                  <span className="title fontsize14">
                    {group.title}({group.children.length})
                  </span>
                </div>
              }
            >
              {group.children.map((opt, i) => {
                // console.log("opt", typeof opt.name);
                return (
                  <Option
                    key={opt.name}
                    value={opt.id}
                    className="searchoptions fontsize16"
                    onClick={e => {
                      if (group.title === "Charity") {
                        this.handleCharityClick(opt.id);
                      } else {
                        this.handleOnclick(opt.id);
                      }
                    }}
                  >
                    {opt.name}
                  </Option>
                );
              })}
            </OptGroup>
          );
        })
      : [
          <Option key="no_result">
            {isLoading ? "Loading" : "No Result Found"}
          </Option>
        ];
    return (
      <Fragment>
        <AppHeader handleClick={this.handleClick} category={category} />
        <div className="dashboard">
          <CommonSuccessMsg className={"addtraker-success-msg "} />
          <div className="flex justify-content-space-between align-items-center mt24 mb30">
            {formList.length > 0 && (
              <AutoComplete
                dropdownMatchSelectWidth={false}
                dropdownStyle={{ width: "50px", position: "fixed" }}
                size="small"
                style={{ width: "350px", border: "none" }}
                dataSource={options}
                placeholder="Search charity"
                onSearch={this.showoptions}
              >
                <Input className="search-option" />
              </AutoComplete>
            )}
            <div className="flex flex-grow-1 justify-content-end align-items-center">
              <div className="mr16 fontsize16 align-item-end dark medium">
                Last Updated On {date},{time}
              </div>
              <Button type="ghost" onClick={handleViewAll}>
                View All Tracker
              </Button>
              <div className="ml16">
                <Upload
                  action={getUploadDocsURL(userId)}
                  name="files"
                  className={`clickable `}
                  showUploadList={false}
                  onChange={this.handleOnChange}
                  disabled={requesting}
                >
                  <Button type="primary" className="upload-tracker-btn">
                    {requesting ? (
                      <Icon type="loading" />
                    ) : (
                      <div>
                        <Icon type="upload" />
                        Upload Tracker
                      </div>
                    )}
                  </Button>
                </Upload>
              </div>
            </div>
          </div>
          {formList.length > 0 ? (
            <div className="flex align-item-center flex-wrap">{formList}</div>
          ) : (
            <div className="flex column align-items-center justify-content-center">
              <div className="no-form-filled">
                <Icon
                  type="file"
                  style={{ fontSize: "80px", color: "darkgrey" }}
                />
              </div>
              <div className="fontsize16 pt10">
                {formatMessage(messages.noFormFIlled)}
              </div>
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

export default injectIntl(FilledFormList);
