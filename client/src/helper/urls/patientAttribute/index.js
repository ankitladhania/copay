// import changePassword from "../../../Containers/page/changePassword";

export const getPatientAttributeByIdURL = formId => {
  return `patientAttribute/${formId}`;
};

export const getUpdatePatientAttributeByIdURL = (id = "") => {
  return `/patientAttribute/${id}/update`;
};
