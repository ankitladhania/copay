import React, { Component } from "react";
import { Modal, Table, Icon } from "antd";
import { injectIntl } from "react-intl";
import moment from "moment";
import "./style.less";

const ImageHost = process.env.REACT_APP_IMAGE_HOST;

class ViewAllTrackerModal extends Component {
  constructor() {
    super();
    this.state = {
      tracker: []
    };
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.show !== prevProps.show && this.props.show) {
      const { fetchProgramTracker, programId } = this.props;
      fetchProgramTracker(programId).then(response => {
        console.log("response------------asfsfsa-------------->", response);
        const { status, payload = {} } = response;
        if (status) {
          const { data: { programData = [] } = {} } = payload;
          const { tracker } = programData[0] || {};
          this.setState({ tracker: tracker.reverse() });
        }
      });
    }
  }

  handleCancel = e => {
    if (e) {
      e.preventDefault();
    }
    const { close } = this.props;
    close();
  };

  formatMessage = data => this.props.intl.formatMessage(data);

  getfileName = filename => {
    let file = filename.split(".");
    let fileExt = file.pop();
    let fileName = file.join();
    let newName = fileName;
    if (fileName.length > 20) {
      newName = fileName.slice(0, 20);
      newName = `${newName}...`;
    }
    return `${newName}.${fileExt}`;
  };

  render() {
    const { show: visible, isError, user } = this.props;

    if (visible === false) {
      return null;
    }
    const { tracker } = this.state;
    const { handleCancel } = this;
    const columns = [
      {
        title: "File",
        dataIndex: "name",
        render: (text, record) => {
          const {
            file: { name, url }
          } = record;
          return (
            <a href={`${ImageHost}${url}`} download>
              {this.getfileName(name.trim())}
              <Icon type="download" className="ml8" />
            </a>
          );
        }
      },
      {
        title: "Updated By",
        dataIndex: "updatedBy",
        render: (text, record) => {
          const { updatedBy } = record;
          const { name = "" } = user[updatedBy] || {};
          return <div>{name}</div>;
        }
      },
      {
        title: "Edit",
        dataIndex: "edit",
        render: (text, record) => {
          const { updateAt } = record;
          const date = moment(updateAt).format("LL");
          const time = moment(updateAt).format("LT");
          return (
            <div className="clickable" key={`${date}-${time}`}>
              {date},{time}
            </div>
          );
        }
      }
    ];

    const modalProps = {
      visible: visible || isError,
      title: `Tracker`,
      okButtonProps: {},
      onCancel: handleCancel,
      wrapClassName: "global-modal full-height",
      destroyOnClose: true,
      bodyStyle: { height: "100%" },
      width: "620px",
      footer: null
    };
    return (
      <Modal {...modalProps}>
        <Table
          columns={columns}
          dataSource={tracker}
          className="p16 tracker-table"
        ></Table>
      </Modal>
    );
  }
}

export default injectIntl(ViewAllTrackerModal);
