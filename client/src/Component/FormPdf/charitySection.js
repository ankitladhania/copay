import React, { Component, Fragment } from "react";
import { Table, Button, Icon } from "antd";
import axios from "axios";
import Doc from "./doc";
import { PDFDownloadLink } from "@react-pdf/renderer";
import JSZip from "jszip";
import { REQUEST_TYPE } from "../../constant";

const baseUrl = process.env.REACT_APP_IMAGE_HOST;
let zip = new JSZip();
const request = (zipFile, filename, url) => {
  if (filename === "MasterForm") {
    return new Promise(async function(resolve) {
      zipFile.file(`${filename}.pdf`, url);
      resolve();
    });
  }
  return new Promise(async function(resolve) {
    axios({
      method: REQUEST_TYPE.GET,
      url: baseUrl + url,
      responseType: "blob"
    }).then(result => {
      zipFile.file(filename, result.data);
      resolve();
    });
  });
};

const CheckMarkSvg = () => (
  <svg
    className="checkmark"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 52 52"
  >
    <path
      className="checkmark__check"
      fill="none"
      d="M14.1 27.2l7.1 7.2 16.7-16.8"
    />
  </svg>
);
class CharitySection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      download: false,
      logo: "",
      name: ""
    };
  }

  getAllProvider = () => {
    const { insurance = {}, formFilled: formData = {}, template } = this.props;
    const templateId = Object.keys(template);
    const { formTemplate } = template[templateId] || {};

    const { formId } = this.props.match.params;
    const { data: formFilled = {} } = formData[formId] || {};

    const insuranceId = Object.keys(insurance);
    const formFilledFieldId = Object.keys(formFilled);
    let datasource = [];
    insuranceId.forEach(id => {
      const { required_fields, name, logo } = insurance[id];
      const data = {};
      const obj = {};

      for (let template in formTemplate) {
        const { component, name } = formTemplate[template];
        let missingField = [];
        if (name === "Attachments") {
          const sectionFields = required_fields[name];
          const { ATTACHMENTS = {} } = formFilled;
          const attachments = Object.keys(ATTACHMENTS);
          const missingAttachments = sectionFields.filter(atachment => {
            if (!(attachments.indexOf(atachment) > -1)) return true;

            return false;
          });
          missingField = missingAttachments;
        }
        component.forEach(question => {
          const { Question, FieldId } = question;
          const sectionFields = required_fields[name];
          if (FieldId === "NAME") {
            data.patientName = formFilled["NAME"];
          }
          if (
            sectionFields &&
            sectionFields.indexOf(FieldId) > -1 &&
            !(formFilledFieldId.indexOf(FieldId) > -1)
          ) {
            missingField.push(Question);
          }
        });
        if (missingField.length > 0) {
          obj[name] = missingField;
          data.section = obj;
        }
      }
      data.key = name;
      data.name = name;
      data.logo = logo;
      datasource.push(data);
    });
    // console.log("datasource", datasource);
    return datasource;
  };

  handleOnClickDownload = (logo, name) => {
    this.setState({ download: true, logo: logo, name: name });
  };

  handleResetState = () => {
    this.setState({ download: false, logo: "", name: "" });
  };

  saveData = async (blob, patientName, charityName) => {
    const { download } = this.state;
    const { formFilled: formData } = this.props;
    const { formId } = this.props.match.params;
    const { data: formFilled = {} } = formData[formId] || {};
    const attachments = formFilled["ATTACHMENTS"] || {};

    let files = Object.keys(attachments);
    files.push("MasterForm");
    if (download) {
      const zipFile = zip.folder(`${patientName}-${charityName}`);
      Promise.all(
        files.map(file => {
          if (file === "MasterForm") {
            return request(zipFile, file, blob);
          }
          const { url, name } = attachments[file] || {};
          const extension = name.split(".").pop();
          return request(zipFile, `${file}.${extension}`, url);
        })
      )
        .then(function() {
          zip
            .generateAsync({
              type: "blob"
            })
            .then(function(content) {
              var a = document.createElement("a");
              document.body.appendChild(a);
              a.style = "display: none";
              let url = window.URL.createObjectURL(content);
              a.href = url;
              a.download = `${patientName}-${charityName}.zip`;
              a.click();
              window.URL.revokeObjectURL(url);
              // window.open(URL.createObjectURL(content), "_blank");
            });
        })
        .catch(err => {
          console.log("error:", err);
        });
    }
  };

  render() {
    const providers = this.getAllProvider();
    const columns = [
      {
        title: "Name",
        key: "Name",
        dataIndex: "name",

        render: (text, record) => {
          const { name } = record;
          return (
            <div className="flex justify-content-start text-overflow">
              {name}
            </div>
          );
        }
      },
      {
        title: "Missing Field",
        key: "Field",
        dataIndex: "field",
        render: (text, record) => {
          const { section } = record;
          return (
            <div className="flex column">
              {section &&
                Object.keys(section).map(field => {
                  return (
                    <Fragment key={field}>
                      <div className="dark bold">{field}: </div>
                      <ul>
                        {section[field].map(field => {
                          return <li key={field}>{field}</li>;
                        })}
                      </ul>
                    </Fragment>
                  );
                })}
            </div>
          );
        }
      },
      {
        title: "Download",
        key: "Download",
        dataIndex: "download",
        render: (text, record) => {
          return (
            <div className="flex justify-content-start">
              {this.state.name !== "" &&
              record.name === this.state.name &&
              this.state.download ? (
                <Button>
                  <PDFDownloadLink
                    document={
                      <Doc
                        {...this.props}
                        logo={record.logo}
                        name={record.name}
                        handleResetState={this.handleResetState}
                      />
                    }
                    fileName={`${record.patientName}${record.name}.pdf`}
                  >
                    {({ blob, url, loading, error }) => {
                      if (blob === null) {
                        return <Icon type="loading" />;
                      } else if (blob !== null) {
                        this.saveData(blob, record.patientName, record.name);
                        setTimeout(() => {
                          this.handleOnClickDownload();
                        }, 1000);
                        if (this.state.name !== "") {
                          return <Icon component={CheckMarkSvg} />;
                        }
                      }
                    }}
                  </PDFDownloadLink>
                </Button>
              ) : (
                <Button
                  key={record.logo}
                  onClick={e => {
                    this.handleOnClickDownload(record.logo, record.name);
                  }}
                >
                  Download
                </Button>
              )}
            </div>
          );
        }
      }
    ];

    if (providers.length <= 0) {
      return (
        <Fragment>
          <div className="response">
            <div className="form-response">
              <Icon type="loading" />
            </div>
          </div>
        </Fragment>
      );
    }
    return (
      <Fragment>
        <div className="charity-table">
          <Table columns={columns} dataSource={providers} />
        </div>
      </Fragment>
    );
  }
}

export default CharitySection;
