const Config = require("../config/config");
const express = require("express");
const Mongo = require("../libs/mongo");
Config();
const cookieParser = require("cookie-parser");
const cors = require("cors");
const path = require("path");
const cookieSession = require("cookie-session");
const multer = require("multer");
const insuranceRouter = require("../routes/api/insurance");
const templateRouter = require("../routes/api/template");
const userRouter = require("../routes/api/user");
const programRouter = require("../routes/api/program");
const formRouter = require("../routes/api/form");
const searchRouter = require("../routes/api/search");
const patientAttributeRouter = require("../routes/api/patientAttribute");

// const logger = require("morgan");

var storage = multer.memoryStorage();
var upload = multer({ dest: "../app/public/", storage: storage });

const app = express();
const mongo = new Mongo();

var conn = (async function() {
  try {
    const connection = await mongo.getConnection();
  } catch (err) {
    console.log(err);
  }
})();
// app.use(logger);

app.use(express.json({ limit: "50mb" }));
app.use(
  express.urlencoded({
    extended: false,
    limit: "50mb"
  })
);

app.use(express.static(path.join(__dirname, "../public")));

app.use(cookieParser());
app.use(cors());
// app.use(
//   cookieSession({
//     maxAge: 30 * 24 * 60 * 60 * 1000,
//     keys: JSON.parse(process.config.cookieKey)
//   })
// );

app.use("/api", insuranceRouter);
app.use("/api", templateRouter);
app.use("/api", userRouter);
app.use("/api", programRouter);
app.use("/api", formRouter);
app.use("/api", searchRouter);
app.use("/api", patientAttributeRouter);
app.use((req, res) => {
  console.log("in after route", req.params);
  return res.sendFile(path.join(__dirname, "../public/index.html"));
});

const server = app.listen(process.env.APP_PORT || "5000", () =>
  console.log("server started")
);
// module.exports = { server };
