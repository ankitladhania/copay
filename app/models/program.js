const mongoose = require("mongoose");
const collectionName = "programs";

const programSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    programCreatedBy: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    description: { type: String },
    tracker: [Object]
    // activeFrom: { type: Date, default: Date.now },
    // expiresOn: { type: Date },
  },
  {
    collection: collectionName,
    timestamps: true
  }
);

module.exports = mongoose.model("program", programSchema);
