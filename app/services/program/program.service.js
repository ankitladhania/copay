const programModal = require("../../models/program");

class ProgramService {
  constructor() {}

  async addProgram(data) {
    try {
      const result = await programModal.create(data);
      return result;
    } catch (error) {
      console.log("error", error);
      throw error;
    }
  }
  async getAllProgram() {
    try {
      const result = await programModal.find({}).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getTrackerForProgram(id) {
    try {
      const result = await programModal
        .find({ _id: id }, { tracker: 1 })
        .lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getProgramById(Id) {
    try {
      const result = await programModal.find({ _id: Id }).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }

  async getProgramByIds(Ids) {
    try {
      const result = await programModal.find({ _id: { $in: Ids } }).lean();
      return result;
    } catch (error) {
      console.log("error", error);
    }
  }
  async editProgram(id, data) {
    try {
      const program = await programModal
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              name: data.name,
              description: data.description
            }
          },
          { new: true }
        )
        .lean();
      return program;
    } catch (err) {
      throw err;
    }
  }

  async addTrackerInProgram(id, data) {
    try {
      const program = await programModal
        .findOneAndUpdate(
          { _id: id },
          {
            $push: {
              tracker: data
            }
          },
          { new: true }
        )
        .lean();
      return program;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new ProgramService();
