import React, { Component, Fragment } from "react";
import { Menu } from "antd";
import { Link } from "react-scroll";

const SECTION = "Section";

class Menubar extends Component {
  render() {
    const { template } = this.props;
    const templateId = Object.keys(template);
    const { formTemplate = [] } = template[templateId] || {};
    const menuelement = formTemplate
      .filter(template => {
        const { type } = template;
        if (type === SECTION) return true;

        return false;
      })
      .map(template => {
        const { name } = template;
        return (
          <Menu.Item key={name}>
            <Link
              //   activeClass={"active"}
              to={name}
              spy={true}
              isDynamic={false}
              offset={-280}
              delay={0}
              onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                this.props.scrollToElement(name);
              }}
              onSetActive={() => {
                this.props.setActive(name);
              }}
            >
              <div
                onClick={e => {
                  e.preventDefault();
                  e.stopPropagation();
                  this.props.scrollToElement(name);
                }}
              >
                {name}
              </div>
            </Link>
          </Menu.Item>
        );
      });
    return (
      <Fragment>
        <Menu
          mode="horizontal"
          className={"flex justify-content-space-between"}
          selectedKeys={[this.props.active]}
        >
          {menuelement}
        </Menu>
      </Fragment>
    );
  }
}

export default Menubar;
