import { doRequest } from "../../helper/network";
import { Insurance } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_INSURANCE = "FETCHING_INSURANCE";
const FETCHING_INSURANCE_COMPLETED = "FETCHING_INSURANCE_COMPLETED";
const FETCHING_INSURANCE_COMPLETED_WITH_ERROR =
  "FETCHING_INSURANCE_COMPLETED_WITH_ERROR";

function setInsuranceProvider(state, data) {
  const { insuranceProvider } = data;
  if (insuranceProvider) {
    const insuranceProviderIds = Object.keys(insuranceProvider);
    const previnsuranceProviders = { ...state };
    let newState = Object.assign({}, previnsuranceProviders);
    insuranceProviderIds.forEach(id => {
      newState = Object.assign(
        newState,
        setindividualinsuranceProvider(
          previnsuranceProviders,
          insuranceProvider,
          id
        )
      );
    });
    return newState;
  } else {
    return state;
  }
}

function setindividualinsuranceProvider(state, insuranceProvider, id) {
  const program = Object.assign({}, state[id], insuranceProvider[id]);
  return { [id]: program };
}

export const fetchInsurance = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_INSURANCE });
      let response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Insurance.getAllProviderURL()
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_INSURANCE_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_INSURANCE_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;
    }
  };
};

const intialState = {};

export default (state = intialState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case FETCHING_INSURANCE_COMPLETED:
      return setInsuranceProvider(state, payload);

    default:
      return { ...state };
  }
};
