const mongoose = require("mongoose");
const collectionName = "pateintAttribute";

const PateintAttributeSchema = new mongoose.Schema(
  {
    data: { type: Object },
    formId: { type: mongoose.Schema.Types.ObjectId, ref: "insuranceFormFilled" }
  },
  {
    collection: collectionName,
    timestamps: true
  }
);

module.exports = mongoose.model(
  "pateintAttribute",
  PateintAttributeSchema
);
