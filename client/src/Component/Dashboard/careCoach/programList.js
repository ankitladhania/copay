import React, { Component, Fragment } from "react";
import {Card} from "antd";

import patients from "../../../Assets/images/ico-users.svg";

class ProgramList extends Component {
  handleOnclick = programId => {
    this.props.history.push(`/program/${programId}`);
  };
  render() {
    const { program = {} } = this.props;
    const programIds = Object.keys(program);

    return (
      <Fragment>
        {programIds.map(programId => {
          const { name, description, formCount } = program[programId];
          return (
            <Card
            className="programs clickable"
            key={name}
            onClick={e => {
              this.handleOnclick(programId);
            }}
          >
            <h4 className="dark">{name}</h4>
            <ul>
              <li>
                <span className="fontsize12 dark">
                  {description}
                </span>
              </li>
              <li>
                <span className="fontsize12 dark">
                  <img alt="" className="cardImg mr16" src={patients} />
                   {formCount} Forms
                </span>
              </li>
            </ul>
          </Card>
            // <div
            //   className="program_card clickable ml16"
            //   key={programId}
            //   onClick={e => this.handleOnclick(programId)}
            // >
            //   <div className="flex column ">
            //     <div>
            //       <div className="dark">{name}</div>
            //     </div>
            //     <div>
            //       <div>{description} </div>
            //     </div>
            //   </div>
            // </div>
          );
        })}
      </Fragment>
    );
  }
}

export default ProgramList;
