const { addLessLoader, fixBabelImports, override } = require("customize-cra");

const rewireReactIntl = require("react-app-rewire-react-intl");

module.exports = {
  webpack: override(
    addLessLoader({
      modifyVars: {},
      javascriptEnabled: true
    }),
    fixBabelImports("babel-plugin-import", {
      libraryName: "antd",
      libraryDirectory: "es",
      style: false
    })
  ),
   override(config, env) {

    config = rewireReactIntl(config, env, {
      messagesDir: "./build/messages/"
    });
    return config;
  }
};
