import Routes from "../../Routes";

import { connect } from "react-redux";
import { onAppStart } from "../../modules/auth";

const mapStateToProps = state => {
  const { auth } = state;
  return {
    authenticated: auth.authenticated,
    authenticatedUser: auth.authenticated_user,
    authRedirection: auth.authRedirection
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAppStart: () => {
      dispatch(onAppStart());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);
