import { doRequest } from "../../helper/network";
import { Template } from "../../helper/urls";
import { REQUEST_TYPE } from "../../constant";

const FETCHING_TEMPLATE = "FETCHING_TEMPLATE";
const FETCHING_TEMPLATE_COMPLETED = "FETCHING_TEMPLATE_COMPLETED";
const FETCHING_TEMPLATE_COMPLETED_WITH_ERROR =
  "FETCHING_TEMPLATE_COMPLETED_WITH_ERROR";

function setTemplate(state, data) {
  const { template } = data;
  if (template) {
    const prevtemplates = { ...state };
    let newState = Object.assign({}, prevtemplates, template);
    return newState;
  } else {
    return state;
  }
}

export const fetchTemplate = () => {
  return async dispatch => {
    try {
      dispatch({ type: FETCHING_TEMPLATE });
      let response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Template.getTemplateURL()
      });
      const { status, payload } = response;
      if (status === true) {
        dispatch({
          type: FETCHING_TEMPLATE_COMPLETED,
          payload: payload.data
        });
      } else if (response.status === false) {
        dispatch({
          type: FETCHING_TEMPLATE_COMPLETED_WITH_ERROR,
          payload: payload.error
        });
      }
      return response;
    } catch (error) {
      throw error;

    }
  };
};

const intialState = {};

export default (state = intialState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case FETCHING_TEMPLATE_COMPLETED:
      return setTemplate(state, payload);

    default:
      return { ...state };
  }
};
