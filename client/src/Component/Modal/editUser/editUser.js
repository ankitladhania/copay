import React, { Component, Fragment } from "react";
import { Modal, Button, Form, Select } from "antd";
import { injectIntl } from "react-intl";
import CommonError from "../../CommonError";
import EditUserForm from "./editUserForm";
import messages from "../message";
import DeleteUser from "./deleteUser";
const { Option } = Select;

const hasErrors = fieldsError => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
};
let errMessage = "";

class EditUser extends Component {
  constructor(props) {
    super(props);
    const { users } = this.props;
    this.state = {
      selectedProgram: users.programId || [],
      requesting: false,
      disabledSubmit: true,
      is_error: false,
      deleteModalVisible: false
    };
    this.FormWrapper = Form.create({ onFieldsChange: this.onFormFieldChanges })(
      EditUserForm
    );
  }

  onFormFieldChanges = (props, allvalues) => {
    const {
      form: { getFieldsError, isFieldsTouched }
    } = props;
    const isError = hasErrors(getFieldsError());
    const { disabledSubmit } = this.state;
    if (disabledSubmit !== isError && isFieldsTouched()) {
      this.setState({ disabledSubmit: isError });
    }
  };

  getProgramOption = () => {
    const { program: programs } = this.props;
    const programIds = Object.keys(programs);
    return programIds;
  };

  makeOptions = values => {
    const { program } = this.props;
    const options = values.map(programId => {
      const { _id, name } = program[programId];
      return (
        <Option key={`${_id}`} value={_id}>
          {name}
        </Option>
      );
    });
    return options;
  };

  handleProgramSelectChange = selectedProgram => {
    this.setState({ selectedProgram });
  };

  handleCancel = e => {
    if (e) {
      e.preventDefault();
    }
    const { close } = this.props;
    close();
  };

  setFormRef = formRef => {
    this.formRef = formRef;
  };

  handleSubmit = e => {
    e.preventDefault();
    const { editPatientCoordinator, close, users } = this.props;
    const { _id: id } = users;

    const { formRef = {} } = this;
    const {
      props: {
        form: { validateFields }
      }
    } = formRef;

    validateFields((err, values) => {
      if (!err) {
        const { password } = values;
        this.setState({ requesting: true });
        if (!password) {
          values.password = users.password;
        }
        editPatientCoordinator(id, { data: values }).then(response => {
          const { status, payload } = response;
          if (status) {
            this.setState({ requesting: false, disabledSubmit: true });
            close();
          } else {
            const { error: { message = "" } = {} } = payload;
            errMessage = message;
            this.setState({ requesting: false, is_error: true });
          }
        });
      }
    });
  };

  footer = () => {
    const { handleCancel, handleSubmit, handleOnClickDelete } = this;
    const { requesting, disabledSubmit } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="flex align-items-center justify-content-space-between">
        <Button
          className="iqvia-btn delete ml8"
          icon="delete"
          onClick={handleOnClickDelete}
        >
          {formatMessage(messages.delete)}
        </Button>
        <div className="flex align-items-center justify-content-end h72px mr24">
          <Button className="iqvia-btn cancel mr8" onClick={handleCancel}>
            {formatMessage(messages.cancel)}
          </Button>
          <Button
            className="iqvia-btn warning"
            type="primary"
            loading={requesting}
            onClick={handleSubmit}
            disabled={disabledSubmit}
          >
            {formatMessage(messages.submit)}
          </Button>
        </div>
      </div>
    );
  };

  clearMsg = e => {
    //e.preventDefault();
    this.setState({
      is_link_send: false,
      is_error: false
    });
  };

  handleOnClickDelete = e => {
    this.setState({ deleteModalVisible: true });
  };

  handleDelete = async e => {
    const { deleteUser, userId, close } = this.props;
    const response = await deleteUser(userId);
    const { status } = response;
    if (status) {
      this.setState({ deleteModalVisible: false });
      close();
    }
  };

  handleCancelDeleteModal = e => {
    if (e) {
      e.preventDefault();
    }
    this.setState({ deleteModalVisible: false });
  };
  render() {
    const { show: visible, isError = {} } = this.props;

    if (visible === false) {
      return null;
    }

    const programOption = this.getProgramOption();

    const options = this.makeOptions(programOption);

    const {
      handleCancel,
      footer,
      FormWrapper,
      setFormRef,
      handleCancelDeleteModal,
      handleDelete
    } = this;

    const { deleteModalVisible } = this.state;

    const modalProps = {
      visible: visible || isError,
      title: "Edit User",
      okButtonProps: {},
      onCancel: handleCancel,
      wrapClassName: "iqvia_modals full-height",
      destroyOnClose: true,
      bodyStyle: { height: "100%" },
      width: "480px",
      footer: footer()
    };
    return (
      <Fragment>
        <Modal {...modalProps}>
          <Fragment>
            <FormWrapper
              wrappedComponentRef={setFormRef}
              {...this.props}
              options={options}
            />
            {this.state.is_error && (
              <CommonError
                msg={errMessage}
                className={"superAdmin-new-user-failure-message"}
                close={this.clearMsg}
              />
            )}
          </Fragment>
        </Modal>
        <DeleteUser
          visible={deleteModalVisible}
          handleDelete={handleDelete}
          handleCancel={handleCancelDeleteModal}
        />
      </Fragment>
    );
  }
}

export default injectIntl(EditUser);
